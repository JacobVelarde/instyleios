//
//  INSInvitationsViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 13/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSInvitationsViewController: INSBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var invitationList:[INSInvitation]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        invitationList = [INSInvitation.init(title: "Brugal 1888", descriptionStr: "", date: "09/10/19"), INSInvitation.init(title: "OCESA", descriptionStr: "", date: "15/10/19"), INSInvitation.init(title: "Brugal 1888", descriptionStr: "", date: "22/11/19")]

        // Do any additional setup after loading the view.
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func registerCells() {
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "INSInvitationsTableViewCell", bundle: Bundle(for: INSInvitationsTableViewCell.self)), forCellReuseIdentifier: "invitationCell")
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
    
    @IBAction func unwindToInvitationsView(for unwindSegue: UIStoryboardSegue) {
    }

}

extension INSInvitationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invitationList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let invitation = invitationList?[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "invitationCell", for: indexPath) as! INSInvitationsTableViewCell
            cell.labelTitle.text = String(format:"%@ / %@", invitation.title ?? "", invitation.date ?? "")
            
            if indexPath.row % 2 == 0 {
                cell.backgroundColor = INSConstants.ThemeColor.textfieldBackground
            }
            cell.acceptAction = { (_) in
                
            }
            
            cell.removeAction = {(_) in
                self.performSegue(withIdentifier: "showCancelInvitationSegue", sender: nil)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
