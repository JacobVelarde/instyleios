//
//  INSNewTalentViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 03/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSNewTalentViewController: INSBaseViewController {

    @IBOutlet weak var buttonMale: INSButton!
    @IBOutlet weak var buttonFemale: INSButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textfieldName: INSFormTextField!
    @IBOutlet weak var textfieldSurnames: INSFormTextField!
    @IBOutlet weak var textfieldEmail: INSFormTextField!
    @IBOutlet weak var textfieldMonth: INSFormTextField!
    @IBOutlet weak var textfieldDay: INSFormTextField!
    @IBOutlet weak var textfieldYear: INSFormTextField!
    @IBOutlet weak var buttonSave: UIButton!
    fileprivate var currentEditing: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    private func setup() {
        buttonSave.layer.borderColor = #colorLiteral(red: 0.6862745098, green: 0.6901960784, blue: 0.6901960784, alpha: 1)
        buttonMale.touchInside(buttonMale)
        self.navigationController?.isNavigationBarHidden = false
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        buttonMale.buttonAction = { (_ sender: UIButton) in
            self.buttonFemale.selectedBtn = false
        }
        
        buttonFemale.buttonAction = { (_ sender: UIButton) in
            self.buttonMale.selectedBtn = false
        }
        
        textfieldDay.setInputViewDatePicker(target: self, selector: #selector(tapOnDone))
        textfieldMonth.setInputViewDatePicker(target: self, selector: #selector(tapOnDone))
        textfieldYear.setInputViewDatePicker(target: self, selector: #selector(tapOnDone))
    }
    
    fileprivate func updateSubmitButton() {
        if textfieldName.isValidated, textfieldSurnames.isValidated, textfieldEmail.isValidated, textfieldYear.isValidated, textfieldDay.isValidated, textfieldMonth.isValidated {
            buttonSave.layer.borderColor = #colorLiteral(red: 0.1058823529, green: 0.5215686275, blue: 0.568627451, alpha: 1)
            buttonSave.backgroundColor = #colorLiteral(red: 0.1058823529, green: 0.5215686275, blue: 0.568627451, alpha: 1)
            buttonSave.isEnabled = true
        }
        else {
            buttonSave.layer.borderColor = #colorLiteral(red: 0.6862745098, green: 0.6901960784, blue: 0.6901960784, alpha: 1)
            buttonSave.backgroundColor = #colorLiteral(red: 0.6862745098, green: 0.6901960784, blue: 0.6901960784, alpha: 1)
            buttonSave.isEnabled = false
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        let talent = INSTalent()
        talent.gender = buttonMale.selectedBtn ? INSTalent.GenderType.male : INSTalent.GenderType.female
        talent.name = textfieldName.text
        talent.surnames = textfieldSurnames.text
        talent.email = textfieldEmail.text
        talent.birthDate = "\(textfieldYear.text!)-\(textfieldMonth.text!)-\(textfieldDay.text!)"

        showActivityIndicator()
        INSFormDataManager().signupTalent(talent, password: "", completion: {(success, jsonResponse, error) in
            self.dismissActivityIndicator()
            self.showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: jsonResponse?["mensaje"] as? String)
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc fileprivate func tapOnDone() {
        if let datePicker = currentEditing?.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM dd yyyy"
            let dateValues = dateFormatter.string(from: datePicker.date).components(separatedBy: " ")
            textfieldMonth.text = dateValues[0]
            textfieldDay.text = dateValues[1]
            textfieldYear.text = dateValues[2]
        }
        currentEditing?.resignFirstResponder()
    }
}

extension INSNewTalentViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentEditing = textField
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var isValidInfo: Bool = true
        if let text = textField.text {
            textField.text = text.trimmingCharacters(in: .whitespacesAndNewlines)
            
            switch textField {
            case textfieldName,
                 textfieldSurnames:
                isValidInfo = textField.validateNameOrSurnameFormat()
            case textfieldEmail:
                isValidInfo = textField.validateEmailFormat()
            case textfieldYear:
                isValidInfo = textField.validateYearOfBirth()
            case textfieldMonth:
                isValidInfo = textField.validateMonthOfBirth()
            case textfieldDay:
                isValidInfo = textField.validateDayOfBirth()
            default:
                isValidInfo = textField.text?.count ?? 0 > INSConstants.FormFieldLength.defaultFieldMinLength
            }
        }
        
        textField.wasEdited = true
        textField.isValidated = isValidInfo
        if textField == textfieldDay || textField == textfieldMonth || textField == textfieldYear {
            //validate all date fields because they're all set at once
            textfieldDay.isValidated = true
            textfieldMonth.isValidated = true
            textfieldYear.isValidated = true
        }
        updateTextField(textField as! INSFormTextField)
        updateSubmitButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textfieldName {
            textfieldName.text = textfieldName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldSurnames.becomeFirstResponder()
        } else if textField == textfieldSurnames {
            textfieldEmail.becomeFirstResponder()
        } else if textField == textfieldEmail {
            textfieldMonth.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       if textField.wasEdited, !textField.isValidated {
           updateTextField(textField as! INSFormTextField)
       }

        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)

            if textField == textfieldEmail {
                return updatedText.count <= INSConstants.FormFieldLength.emailMaxLength
            }
        }
        return true
    }
    
    fileprivate func updateTextField(_ textfield: INSFormTextField) {
        if textfield.isValidated || textfield.isEditing {
            textfield.updateUIForCorrectInfo()
        } else {
            textfield.updateUIForWrongInfo()
        }
    }
}

extension INSNewTalentViewController {
    //MARK: Keyboard management
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboard = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboard.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}
