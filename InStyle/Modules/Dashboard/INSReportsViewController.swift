//
//  INSReportsViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 09/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSReportsViewController: INSBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var reporttList: [INSReport]?
    var lastViewSectionSelected: INSHeaderTableView?
    var expandedSectionHeaderNumber: Int = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        setup()
        // Do any additional setup after loading the view.
        reporttList = [INSReport.init(title: "Reporte 1", descriptionStr: "REPORTE DE ACTIVACIÓN", startDate: "15/07/2019", endDate: "25/07/2019"), INSReport.init(title: "Reporte 2", descriptionStr: "REPORTE DE ACTIVACIÓN", startDate: "15/07/2019", endDate: "25/07/2019"), INSReport.init(title: "Reporte 3", descriptionStr: "REPORTE DE ACTIVACIÓN", startDate: "15/07/2019", endDate: "25/07/2019")]
    }
    
    private func setup() {
        self.navigationController?.isNavigationBarHidden = true
        self.tableView.sectionHeaderHeight =  UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 25;
        self.tableView.estimatedRowHeight = 20
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "INSNotificationTableViewCell"
            , bundle: Bundle(for: INSNotificationTableViewCell.self)), forCellReuseIdentifier: "notificationCell")
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func menuAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
}

extension INSReportsViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return reporttList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let report = reporttList?[indexPath.section] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! INSNotificationTableViewCell
//            cell.labelDescription.text = String(format:"%@\n%@ AL %@", report.descriptionStr?.uppercased() ?? "", report.startDate ?? "", report.endDate ?? "")
            cell.labelDescription.attributedText = getAttibutedTextForDescription(report: report)

            cell.buttonGoDetail.isHidden = true
            cell.buttonEye.isHidden = false
            cell.buttonAction = {(_) in
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let report = reporttList?[section] {
            let view:INSHeaderTableView = INSHeaderTableView.instanceFromNib() as! INSHeaderTableView
            view.labelTitleHeader.text = report.title?.uppercased()
            view.labelTitleHeader.textColor = INSConstants.ThemeColor.turquoise
            view.tag = section
            view.buttonIcon.isHidden = false
            view.buttonAction = { (_) in
                let section = view.tag
                if (self.expandedSectionHeaderNumber == -1) {
                    self.expandedSectionHeaderNumber = section
                    self.lastViewSectionSelected = view
                    self.tableViewExpandSection(section, view.buttonIcon)
                } else {
                    if (self.expandedSectionHeaderNumber == section) {
                        self.tableViewCollapeSection(section, view.buttonIcon)
                    } else {
                        self.tableViewCollapeSection(self.expandedSectionHeaderNumber, self.lastViewSectionSelected?.buttonIcon)
                        self.lastViewSectionSelected = view
                        self.tableViewExpandSection(section, view.buttonIcon)
                    }
                }
            }
            return view
        }
        return nil
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    private func getAttibutedTextForDescription(report:INSReport) -> NSMutableAttributedString {
        let completeStr = String(format:"%@\n%@ AL  %@", report.descriptionStr?.uppercased() ?? "", report.startDate ?? "", report.endDate ?? "")
        let attributedText = NSMutableAttributedString(string: completeStr)
        attributedText.addWordSpacing(3, in: (completeStr as NSString).range(of: report.startDate ?? ""))
        attributedText.addWordSpacing(3, in: (completeStr as NSString).range(of: report.endDate ?? ""))
        return attributedText
    }
    
    func tableViewCollapeSection(_ section: Int, _ button: UIButton?) {
        button?.setImage(UIImage(named: "pointsIcon"), for: .normal)
        self.expandedSectionHeaderNumber = -1;
        var indexesPath = [IndexPath]()
        let index = IndexPath(row: 0, section: section)
        indexesPath.append(index)
        self.tableView!.beginUpdates()
        self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
        self.tableView!.endUpdates()
    }
    
    func tableViewExpandSection(_ section: Int, _ button: UIButton?) {
        button?.setImage(UIImage(named: "upArrowIcon"), for: .normal)
        var indexesPath = [IndexPath]()
        let index = IndexPath(row: 0, section: section)
        indexesPath.append(index)
        self.expandedSectionHeaderNumber = section
        self.tableView!.beginUpdates()
        self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
        self.tableView!.endUpdates()
    }
}
