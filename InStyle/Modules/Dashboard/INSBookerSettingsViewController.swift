//
//  INSBookerSettingsViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 12/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSBookerSettingsViewController: INSBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var list:[String]?
    var unfoldedList:[Any] = []
    var unfoldedListCopy:[Any] = []
    var lastViewSectionSelected: INSHeaderTableView?
    var expandedSectionHeaderNumber: Int = -1
    var dataManager = INSActivationsDataManager.activationsDataManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        list = ["Tipo de ativación", "Tipo de talentos"]
        setup()
        registerCells()
        // Do any additional setup after loading the view.
    }
    
    private func setup() {
        self.navigationController?.isNavigationBarHidden = true
        self.tableView.sectionHeaderHeight =  UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 10;
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
    }

    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "INSSettingsTableViewCell"
            , bundle: Bundle(for: INSSettingsTableViewCell.self)), forCellReuseIdentifier: "settingsCell")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
}


extension INSBookerSettingsViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return list?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            return unfoldedList.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = unfoldedList[indexPath.row]
        if item is INSActivationType {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath) as! INSSettingsTableViewCell
            cell.texfieldValue.text = (item as! INSActivationType).description
            cell.texfieldValue.isEnabled = false
            cell.buttonEdit.isHidden = false
            cell.buttonTrash.isHidden = false
            cell.buttonChecked.isHidden = true
            cell.edittAction = {(_) in
                cell.buttonEdit.isHidden = true
                cell.buttonTrash.isHidden = true
                cell.buttonChecked.isHidden = false
                cell.texfieldValue.isEnabled = true
                cell.texfieldValue.becomeFirstResponder()
            }
            
            cell.applyChangesAction = {(_) in
                cell.buttonEdit.isHidden = false
                cell.buttonTrash.isHidden = false
                cell.buttonChecked.isHidden = true
                cell.texfieldValue.isEnabled = false
            }
            
            cell.trashAction = {(_) in
                self.unfoldedList.remove(at: indexPath.row)
                self.showAlertWith(title: "ELIMINADO", imageName: "checkIcon", alertType: .imageBelowMessage)
                tableView.reloadSections(IndexSet(integer: indexPath.section), with: .automatic)
            }

            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let option = list?[section] {
            let view:INSHeaderTableView = INSHeaderTableView.instanceFromNib() as! INSHeaderTableView
            view.labelTitleHeader.text = option
            view.tag = section
            view.buttonIcon.setImage(UIImage(named: "downArrowIcon"), for: .normal)
            view.buttonIcon.isHidden = false
            view.buttonAction = { (_) in
                let section = view.tag
                if section == 0 {
                    if view.buttonIcon.image(for: .normal) == UIImage(named: "downArrowIcon") {
                        self.showActivityIndicator()
                        self.dataManager.fetchActivationsType(completion: {(success, activationList) in
                            DispatchQueue.main.async {
                                self.dismissActivityIndicator()
                                if success {
                                    self.unfoldedList = activationList ?? []
                                    self.reload(section: section, view: view)
                                    self.unfoldedListCopy = self.unfoldedList

                                }else {
                                    self.unfoldedList = []
                                    self.reload(section: section, view: view)
                                    self.unfoldedListCopy = []
                                }
                            }
                        })
                    }else {
                        self.reload(section: section, view: view)
                    }
                        
                } else {
                    self.unfoldedList = []
                    self.reload(section: section, view: view)
                    self.unfoldedListCopy = []
                }
            }
            return view
        }
        return nil
    }
    
    func reload(section:Int, view:INSHeaderTableView) {
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            self.lastViewSectionSelected = view
            self.tableViewExpandSection(section, view.buttonIcon)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                self.tableViewCollapeSection(section, view.buttonIcon)
            } else {
                self.tableViewCollapeSection(self.expandedSectionHeaderNumber, self.lastViewSectionSelected?.buttonIcon)
                self.lastViewSectionSelected = view
                self.tableViewExpandSection(section, view.buttonIcon)
            }
        }
    }
            
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableViewCollapeSection(_ section: Int, _ button: UIButton?) {
        button?.setImage(UIImage(named: "downArrowIcon"), for: .normal)
        self.expandedSectionHeaderNumber = -1;
        var indexesPath = [IndexPath]()
        for (i, _) in unfoldedListCopy.enumerated() {
            let index = IndexPath(row: i, section: section)
            indexesPath.append(index)
        }
        self.tableView!.beginUpdates()
        self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
        self.tableView!.endUpdates()
    }

    func tableViewExpandSection(_ section: Int, _ button: UIButton?) {
        button?.setImage(UIImage(named: "upArrowIcon"), for: .normal)
        var indexesPath = [IndexPath]()
        for (i, _) in unfoldedList.enumerated() {
            let index = IndexPath(row: i, section: section)
            indexesPath.append(index)
        }
        self.expandedSectionHeaderNumber = section
        self.tableView!.beginUpdates()
        self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
        self.tableView!.endUpdates()
    }
}
