//
//  INSTalentsViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 22/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSTalentsViewController: UIViewController {

    

    @IBOutlet weak var stackViewContainer: UIStackView!
    @IBOutlet weak var pickerViewContainer: UIStackView!
    @IBOutlet weak var filterPickerView: UIPickerView!
    @IBOutlet weak var addButton: UIButton!
    
    var allItems: [Any] = []
    var genderSelected:String?
    var profileSelected:String?
    var filtersSelected:[ItemFilter] = []
    var pickerViewList:[String] = []
    let dataManager = INSFilterTalentsManager()
    var filterContainer:INSGenericFilterCollectionView?
    var collectionView:INSGenericCollectionView?
    
    var filterSelected:ItemFilter?
    var cell:INSFilterCollectionViewCell?
    var optionSelected: String?
    var isCategorySelected: Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()

        showActivityIndicator()
        dataManager.getTalentPanel(completion: {(success, talentList) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.allItems = talentList ?? []
                    self.loadTalents()
                }else {
                    self.loadTalents()
                }
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    func setUp() {
        if INSDataController.sharedDataController.user.profile != .admin {
            addButton.alpha = 0
            addButton.isUserInteractionEnabled = false
        }
        filterPickerView.dataSource = self
        filterPickerView.delegate = self
        
        filterContainer = INSGenericFilterCollectionView.instanceFromNib(delegate: self) as? INSGenericFilterCollectionView
        filterContainer?.labelHeader.isHidden = true
        filterContainer?.hideGenderStack(value: true)
        filterContainer?.filterOptions = []
        stackViewContainer.addArrangedSubview(filterContainer!)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func loadTalents() {
        if INSDataController.sharedDataController.user.profile == .admin {
            collectionView = INSGenericCollectionView.instanceFromNib() as? INSGenericCollectionView
        } else {
            collectionView = INSGenericCollectionView.instanceFromNib(selectionType: .SimpleSelection, collectionType: .withStarts) as? INSGenericCollectionView
        }
        collectionView?.allItems = allItems
        collectionView?.delegate = self
        stackViewContainer.addArrangedSubview(collectionView!)
    }
        
    @IBAction func menuAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
        
    @IBAction func doneAction(_ sender: Any) {
        if isCategorySelected {
            filterContainer?.buttonTalentSpecialty.setTitle(optionSelected, for: .normal)
            if optionSelected == "TALENT_CATEGORY_HOSTESS_AA".localized {
                filterContainer?.buttonFemale.touchInside(filterContainer!.buttonFemale)
                filterContainer?.hideGenderStack(value: true)
            } else if optionSelected == "TALENT_CATEGORY_GIO_AA".localized {
                filterContainer?.buttonMale.touchInside(filterContainer!.buttonMale)
                filterContainer?.hideGenderStack(value: true)
            } else {
                filterContainer?.filterOptions = []
                filterContainer?.hideGenderStack(value: false)
            }
        } else {
            guard filterSelected != nil && optionSelected != nil else {
                return
            }
            filterSelected?.name = optionSelected
            filterContainer?.updateFilterItem(filterSelected, cell)
            filtersSelected.append(filterSelected!)
        }
        
        pickerViewContainer.isHidden = true
        optionSelected = nil
        filterSelected = nil
        cell = nil
        isCategorySelected = false
    }
    
    @IBAction func cancelAction(_ sender: Any?) {
        pickerViewContainer.isHidden = true
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTalentDetailSegue" {
            if let talent = sender as? INSTalent, let controller = segue.destination as? INSUpdateTalentDataFormTableViewController {
                controller.talentData = talent
            }
        }
        
        if segue.identifier == "itemAddedDreamTeamSegue" {
            if let user = sender as? INSUser, let controller = segue.destination as? INSItemAddedDreamTeamViewController {
                controller.item = user
            }
        }
    }
}

extension INSTalentsViewController: CollectionCellSelectedDelegate {
    func itemSelected(_ item: Any?, _ isBack:Bool?) {
        performSegue(withIdentifier: "showTalentDetailSegue", sender: item)
    }
    
    func itemDeselected(at index: Int, _ item: Any?, _ isBack:Bool?) {
        
    }
    
    func itemStarPressed(_ item: Any?, _ cell:INSItemWithStarCollectionViewCell) {
        if item is INSUser {
            let user = item as! INSUser
            if user.isInTeam == false {
                //Code to add at dream team
                let requestDispatcher = INSRequestDispatcher()
                requestDispatcher.dispatchRequest(opCode: .INS_ADD_DREAM_TEAM_CODE, requestParameters: [Keys.user_id:user.idUser!], completionHandler: { (success, jsonResponse, response, error) in
                    DispatchQueue.main.async {
                        self.dismissActivityIndicator()
                        if success {
                            cell.changeImage = true
                            user.isInTeam = true
                            self.performSegue(withIdentifier: "itemAddedDreamTeamSegue", sender: item)
                        }else {
                            self.showAlertControllerWith(message: error?.errorMessage)
                        }
                    }
                })
            }
        }
    }
}

extension INSTalentsViewController: FiltersDelegate {
    func search() {
        var filters:[String:Any] = ["tipo":profileSelected == "TALENTOS" ? "3" : "4","genero": genderSelected?.lowercased() ?? "", "nacionalidad":"0", "edad":"0", "altura_mts":"0", "peso_kg":"0", "calzado":"0", "ojos":"0", "cabello":"0", "saco":"0", "camisa":"0", "pantalon":"0", "pecho":"0", "cintura":"0", "cadera":"0", "especialidad":"0"]
        for filter in filtersSelected {
            switch filter.id {
            case "EDAD":
                filters["edad"] = filter.name
                break
            case "ALTURA":
                filters["altura_mts"] = filter.name
                break
            case "PESO":
                filters["peso_kg"] = filter.name
                break
            case "CALZADO":
                filters["calzado"] = filter.name
                break
            case "CAMISA":
                filters["camisa"] = filter.name
                break
            case "BUSTO":
                filters["pecho"] = filter.name
                break
            case "SACO":
                filters["saco"] = filter.name
                break
            case "CINTURA":
                filters["cintura"] = filter.name
                break
            case "PANTALON":
                filters["pantalon"] = filter.name
                break
            case "CADERA":
                filters["cadera"] = filter.name
                break
            case "OJOS":
                filters["ojos"] = filter.name
                break
            case "CABELLO":
                filters["cabello"] = filter.name
                break
            default:
                break
            }
        }
        
        self.showActivityIndicator()
        dataManager.fetchTalentFilter(parameters: filters, opCode: profileSelected == "TALENTOS" ? .INS_FILTER_TALENTS_CODE : .INS_FILTER_COORDINATORS_CODE, completion: {(success, talentList) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.allItems = talentList ?? []
                    self.collectionView?.allItems = self.allItems
                }else {
                    self.showAlertControllerWith(message: "TRY_AGAIN_LATER".localized)
                }
            }
        })
    }
    
    func genderSelected(_ gender: String, role: Role?) {
        genderSelected = gender
        filtersSelected = []
        filterContainer?.filterOptions = dataManager.getFilterOptions(gender: genderSelected, profile: profileSelected)
        cancelAction(nil)
    }
    
    func profileSelected(_ profile: String) {
        profileSelected = profile
        genderSelected = nil
        if profile == "TALENTOS" {
            filterContainer?.hideSpecialtyStack(value: false)
        } else if profile == "COORDINADORES" {
            filterContainer?.filterOptions = []
            filterContainer?.hideSpecialtyStack(value: true)
        }
        filterContainer?.hideGenderStack(value: false)
        cancelAction(nil)
    }
    
    func itemSelected(_ item: Any, _ cell: Any) {
        isCategorySelected = false
        self.filterSelected = item as? ItemFilter
        self.cell = cell as? INSFilterCollectionViewCell
        pickerViewContainer.isHidden = false
        pickerViewList = dataManager.getGetListFor(filter: item as! ItemFilter, gender: genderSelected)
        self.filterPickerView.reloadAllComponents()
        self.filterPickerView.selectRow(0, inComponent: 0, animated: false)
    }
    
    func talentSpecialtyPressed() {
        isCategorySelected = true
        pickerViewContainer.isHidden = false
        pickerViewList = ["TALENT_CATEGORY_PROMOTER".localized, "TALENT_CATEGORY_HOSTESS_AA".localized, "TALENT_CATEGORY_MODEL".localized, "TALENT_CATEGORY_GIO_AA".localized]
        self.filterPickerView.reloadAllComponents()
        self.filterPickerView.selectRow(0, inComponent: 0, animated: false)
    }
}
 
extension INSTalentsViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewList.count
    }
}

extension INSTalentsViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        optionSelected = pickerViewList[row]
    }
}
