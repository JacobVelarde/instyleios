//
//  INSDreamTeamViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 26/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSDreamTeamViewController: INSBaseViewController {

    
    @IBOutlet weak var stackViewContainer: UIStackView!
    var dreamTeamList:[Any] = []
    var collectionView:INSGenericCollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        self.showActivityIndicator()
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_DREAM_TEAM_GET, requestParameters: getRequestParameters(withData: false), completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if _success {
                    self.dreamTeamList = INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode:.INS_DREAM_TEAM_GET) ?? []
                    for item in self.dreamTeamList {
                        (item as? INSUser)?.isInTeam = true
                    }
                    self.loadCollectionView()
                } else {
                    self.showAlertControllerWith(message: "TRY_AGAIN_LATER".localized)
                }
            }
        })
        // Do any additional setup after loading the view.
        /*INSFilterTalentsManager().getTalentPanel(completion: {(success, talentList) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.dreamTeamList = talentList ?? []
                    for item in self.dreamTeamList {
                        (item as! INSTalent).isInTeam = true
                    }
                    self.collectionView?.allItems = self.dreamTeamList
                    self.loadCollectionView()
                }else {
                    self.showAlertControllerWith(message: "TRY_AGAIN_LATER".localized)
                }
            }
        })*/

    }
    
    private func loadCollectionView() {
        self.collectionView = INSGenericCollectionView.instanceFromNib(collectionType: .withStarts) as? INSGenericCollectionView
        self.collectionView?.allItems = self.dreamTeamList
        self.collectionView?.delegate = self
        self.stackViewContainer.addArrangedSubview(self.collectionView!)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "itemDeletedDreamTeamSegue" {
            if let user = sender as? INSUser, let controller = segue.destination as? INSItemAddedDreamTeamViewController {
                controller.item = user
            }
        }
    }
    
    
    private func getRequestParameters(withData:Bool) -> [String:String] {
        if withData {
            return [:]
        } else {
            let data:[String:String] = ["id_cliente":INSDataController.sharedDataController.user.idUser ?? "","genero":"", "edad":"", "altura_mts":"", "peso_kg":"", "calzado":"", "ojos":"", "cabello":"",  "nacionalidad":"", "h_saco":"", "h_camisa":"", "h_pantalon":"", "m_pecho":"", "m_cintura":"", "m_cadera":""]
            return data
        }
    }

}

extension INSDreamTeamViewController: CollectionCellSelectedDelegate {
    func itemSelected(_ item: Any?, _ isBack:Bool?) {
//        if item is INSTalent {
//            let talent = item as! INSTalent
//            talent.isInTeam = !talent.isInTeam
//            collectionView?.collectionView.reloadData()
//        }
    }
    
    func itemDeselected(at index: Int, _ item: Any?, _ isBack:Bool?) {
    }
    
    func itemStarPressed(_ item: Any?, _ cell: INSItemWithStarCollectionViewCell) {
        if item is INSUser {
            let user = item as! INSUser
            if user.isInTeam == true {
                //Code to delete at dream team
                let requestDispatcher = INSRequestDispatcher()
                requestDispatcher.dispatchRequest(opCode: .INS_DELETE_DREAM_TEAM_CODE, requestParameters: [Keys.user_id:user.idUser!], completionHandler: { (success, jsonResponse, response, error) in
                    DispatchQueue.main.async {
                        self.dismissActivityIndicator()
                        if success {
                            cell.changeImage = false
                            user.isInTeam = false
                            self.performSegue(withIdentifier: "itemDeletedDreamTeamSegue", sender: item)
                        }else {
                            self.showAlertControllerWith(message: error?.errorMessage)
                        }
                    }
                })
            }
        }
    }
}
