//
//  INSNotificationsViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 24/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSNotificationsViewController: UIViewController {
    @IBOutlet weak var tableViewNotifications: UITableView!
    var notificationsArray : [INSNotification]?
    var lastViewSectionSelected: INSHeaderTableView?
    var expandedSectionHeaderNumber: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        setup()
        // Do any additional setup after loading the view.
        notificationsArray = [INSNotification.init(title: "Notificación 1", descriptionStr: "Estoy en la notificación 1"), INSNotification.init(title: "Notificación 2", descriptionStr: "Estoy en la notificación 2"), INSNotification.init(title: "Notificación 3", descriptionStr: "Estoy en la notificación 3"), INSNotification.init(title: "Notificación 4", descriptionStr: "Estoy en la notificación 4")]
    }
    
    private func setup() {
        self.tableViewNotifications.sectionHeaderHeight =  UITableView.automaticDimension
        self.tableViewNotifications.estimatedSectionHeaderHeight = 25;
        self.tableViewNotifications.estimatedRowHeight = 20
        self.tableViewNotifications.rowHeight = UITableView.automaticDimension
    }
    
    fileprivate func registerCells() {
        tableViewNotifications.register(UINib(nibName: "INSNotificationTableViewCell"
            , bundle: Bundle(for: INSNotificationTableViewCell.self)), forCellReuseIdentifier: "notificationCell")
    }

    @IBAction func closeAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension INSNotificationsViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return notificationsArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let notification = notificationsArray?[indexPath.section] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! INSNotificationTableViewCell
            cell.labelDescription.text = notification.descriptionStr
            cell.buttonAction = {(_) in
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let notification = notificationsArray?[section] {
            let view:INSHeaderTableView = INSHeaderTableView.instanceFromNib() as! INSHeaderTableView
            view.labelTitleHeader.text = notification.title
            view.tag = section
            view.buttonIcon.isHidden = false
            view.buttonAction = { (_) in
                let section = view.tag
                if (self.expandedSectionHeaderNumber == -1) {
                    self.expandedSectionHeaderNumber = section
                    self.lastViewSectionSelected = view
                    self.tableViewExpandSection(section, view.buttonIcon)
                } else {
                    if (self.expandedSectionHeaderNumber == section) {
                        self.tableViewCollapeSection(section, view.buttonIcon)
                    } else {
                        self.tableViewCollapeSection(self.expandedSectionHeaderNumber, self.lastViewSectionSelected?.buttonIcon)
                        self.lastViewSectionSelected = view
                        self.tableViewExpandSection(section, view.buttonIcon)
                    }
                }
            }
            return view
        }
        return nil
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableViewCollapeSection(_ section: Int, _ button: UIButton?) {
        button?.setImage(UIImage(named: "pointsIcon"), for: .normal)
        self.expandedSectionHeaderNumber = -1;
            var indexesPath = [IndexPath]()
            let index = IndexPath(row: 0, section: section)
            indexesPath.append(index)
            self.tableViewNotifications!.beginUpdates()
            self.tableViewNotifications!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableViewNotifications!.endUpdates()
    }
    
    func tableViewExpandSection(_ section: Int, _ button: UIButton?) {
        button?.setImage(UIImage(named: "upArrowIcon"), for: .normal)
            var indexesPath = [IndexPath]()
            let index = IndexPath(row: 0, section: section)
            indexesPath.append(index)
            self.expandedSectionHeaderNumber = section
            self.tableViewNotifications!.beginUpdates()
            self.tableViewNotifications!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableViewNotifications!.endUpdates()
    }
}
