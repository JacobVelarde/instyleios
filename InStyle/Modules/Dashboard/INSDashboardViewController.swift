//
//  INSDashboardViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 19/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSDashboardViewController: UIViewController {

    @IBOutlet weak var constraintHeightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonNotifications: UIButton!
    @IBOutlet weak var labelNotificationsNumber: UILabel!

    let reuseIdentifier = "operationCell"
    let nibIdentifier = "INSOperationCollectionViewCell"
    private let itemsPerRow: CGFloat = 2
    var operations:[INSItem] = []
    var role: INSUser.ProfileType?
    let dataController = INSDataController.sharedDataController

    override func viewDidLoad() {
        super.viewDidLoad()
        registerViewCells()
        // Do any additional setup after loading the view.
        role = dataController.user.profile
        operations = getOperationFor(role: role)
        setUp()
//        dataController.fetchTalents()
//        dataController.fetchCoordinators()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        if let previousController = self.getPreviousController(), let _ = previousController as? INSUpdateTalentDataFormTableViewController {
            guard var controllers = self.navigationController?.viewControllers else { return }
            controllers.remove(at: controllers.count - 2)
            self.navigationController?.viewControllers = controllers
        }
    }

    func setUp() {
        setHeightConstraintToCollectionViewFor(role: role)
        if role == .booker || role == .coordinator {
            buttonNotifications.isHidden = true
            labelNotificationsNumber.isHidden = true
        }
    }

    func registerViewCells() {
        self.collectionView.register(UINib(nibName: nibIdentifier
            , bundle: Bundle(for: INSDashboardViewController.self)), forCellWithReuseIdentifier: reuseIdentifier)
    }

    func setHeightConstraintToCollectionViewFor(role:INSUser.ProfileType? = nil) {
        guard role != nil else {
            return
        }

        switch role {
        case .admin,
             .client:
            constraintHeightCollectionView.constant = 350
            break
        case .talent:
            constraintHeightCollectionView.constant = 230
            break
        default:
            break
        }
    }

    func getOperationFor(role:INSUser.ProfileType? = nil) -> [INSItem] {
        var roleStr = ""
        switch role {
        case .admin:
            roleStr = "admin"
            break
        case .client:
            roleStr = "customer"
            break
        case .talent:
            roleStr = "talent"
            break
        case .coordinator:
            roleStr = "coordinator"
            break
        case .booker:
            roleStr = "booker"
            break
        case .producer:
            roleStr = "producer"
            break
        default:
            break
        }
        if let path = Bundle.main.path(forResource: "INSOperationsMenu", ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                let json = try? JSONSerialization.jsonObject(with: data)
                if let dictionary = json as? [String: Any] {
                    let array = dictionary[roleStr] as? [Any]
                    if let items = array {
                        let opArray = items.map { INSItem(json: $0 as! [String : Any]) }
                        return opArray
                    }
                }
            } catch {
                // Handle error here
            }
        }
        return []
    }

    @IBAction func notificationsAction(_ sender: Any) {
//        let viewController:INSNotificationsViewController = self.getViewControllerWith(identifier: "notificationsView", storyboard: "Dashboard") as! INSNotificationsViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func unwindToDashboardMenu(for unwindSegue: UIStoryboardSegue) {
    }
}

extension INSDashboardViewController : UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return getNumberOfSections()
    }

    func getNumberOfSections() -> Int{
        let count = operations.count
        if count == 1 || count == 2 {
            return 1
        }else if count % 2 == 0 {
            return 1
        }else {
            return 2
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let sections = getNumberOfSections()
        if sections == 1 {
            return operations.count
        }else{
            if section == 0 {
                return operations.count - 1
            }else {
                return 1
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier , for: indexPath) as! INSOperationCollectionViewCell
        var index:Int = 0
        let operation:INSItem!
        if indexPath.section == 0 {
            index = indexPath.row
            operation = operations[index] as INSItem
        }else {
            index = operations.count - 1
            operation = operations[index] as INSItem
        }
        cell.labelTitle.text = operation.title
        cell.buttonIcon.setImage(UIImage(named: operation.icon ?? ""), for: .normal)
        cell.buttonIcon.tag = index
        cell.buttonIcon.addTarget(self, action: #selector(operationAction(sender:)), for: .touchUpInside)

        return cell
    }

    @objc func operationAction(sender : UIButton) {
        let operation = operations[sender.tag] as INSItem
        switch role {
        case .admin:
            switch operation.id {
            case "0001":
                self.performSegue(withIdentifier: "showUnderConstructionSegue", sender: nil)
            case "0002":
                self.performSegue(withIdentifier: "showTalentsSegue", sender: self)
            case "0003":
                self.performSegue(withIdentifier: "activationDashboardSegue", sender: self)
            case "0004":
                self.performSegue(withIdentifier: "showReportsSegue", sender: nil)
            case "0005":
                getMyProfile()
                /*
                self.showActivityIndicator()
                let requestDispatcher = INSRequestDispatcher()
                requestDispatcher.dispatchRequest(opCode: .INS_GET_MY_PROFILE_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
                    DispatchQueue.main.async {
                        self.dismissActivityIndicator()
                        if success {
                            self.performSegue(withIdentifier: "showMyProfileSegue", sender: self)
                        }else {
                            self.showAlertControllerWith(message: error?.errorMessage)
                        }
                    }
                })
                */
            default:
                break
            }
        case .client:
            switch operation.id {
                case "0001":
                    self.performSegue(withIdentifier: "showDreamTeamSegue", sender: nil)
                case "0002":
                    self.performSegue(withIdentifier: "showTalentsSegue", sender: self)
                case "0003":
                    self.performSegue(withIdentifier: "activationDashboardSegue", sender: nil)
                case "0004":
                    self.performSegue(withIdentifier: "showReportsSegue", sender: nil)
                case "0005":
                    getMyProfile()
                default:
                    break
            }
        case .talent:
            switch operation.id {
            case "0001":
                self.performSegue(withIdentifier: "showInvitationsSegue", sender: nil)
            case "0002":
                self.performSegue(withIdentifier: "activationDashboardSegue", sender: self)
            case "0003":
                self.performSegue(withIdentifier: "showReportsSegue", sender: nil)
            case "0004":
                getMyProfile()
            default:
                break
            }
        case .coordinator:
            switch operation.id {
            case "0001":
                getMyProfile()
            case "0002":
                self.performSegue(withIdentifier: "activationDashboardSegue", sender: self)
            case "0003":
                self.performSegue(withIdentifier: "showReportsSegue", sender: nil)
            default:
                break
            }
        case .booker:
            switch operation.id {
            case "0001":
                self.performSegue(withIdentifier: "showDreamTeamSegue", sender: nil)
            case "0002":
                self.performSegue(withIdentifier: "activationDashboardSegue", sender: self)
            case "0003":
                self.performSegue(withIdentifier: "showCustomersViewSegue", sender: nil)
            case "0004":
                self.performSegue(withIdentifier: "showBookerSettingsSegue", sender: nil)
            default:
                break
            }
        case .producer:
            switch operation.id {
            case "0001":
                self.performSegue(withIdentifier: "showDreamTeamSegue", sender: nil)
            case "0002":
                self.performSegue(withIdentifier: "showTalentsSegue", sender: self)
            case "0003":
                self.performSegue(withIdentifier: "activationDashboardSegue", sender: self)
            case "0004":
                self.performSegue(withIdentifier: "showReportsSegue", sender: nil)
            case "0005":
                getMyProfile()
            default:
                break
            }
        default:
            self.performSegue(withIdentifier: "showUnderConstructionSegue", sender: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}


extension INSDashboardViewController : UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return getCellSize(collectionViewLayout: collectionViewLayout)
    }

    private func getCellSize(collectionViewLayout : UICollectionViewLayout) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (self.collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: 110)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
        if section == 1 {
            guard  let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
                return UIEdgeInsets.zero
            }
            let cellWidth = getCellSize(collectionViewLayout: collectionViewLayout).width + flowLayout.minimumInteritemSpacing

            let totalCellWidth = cellWidth * cellCount + cellCount - 1
            let contentWidth = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right

            if (totalCellWidth < contentWidth) {
                let padding = (contentWidth - totalCellWidth) / 2.0
                return UIEdgeInsets(top: 10, left: padding, bottom: 0, right: padding)
            } else {
                return UIEdgeInsets.zero
            }
        }
        return UIEdgeInsets.zero
    }
}

extension INSDashboardViewController {
    private func getMyProfile() {
        self.showActivityIndicator()
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_MY_PROFILE_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.performSegue(withIdentifier: "showMyProfileSegue", sender: self)
                }else {
                    self.showAlertControllerWith(message: error?.errorMessage)
                }
            }
        })
    }
}
