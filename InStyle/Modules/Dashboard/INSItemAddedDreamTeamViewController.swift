//
//  INSItemAddedDreamTeamViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 09/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSItemAddedDreamTeamViewController: UIViewController {

    @IBOutlet weak var imageStart: UIImageView!
    @IBOutlet weak var imageItemPhoto: UIImageView!
    @IBOutlet weak var labelItemName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    var item:INSUser?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    private func setup() {
        if let isInTeam = item?.isInTeam, isInTeam {
            imageStart.image = UIImage(named: "startWarnIcon")
            labelDescription.text = "TALENT_ADDED_DREAM_TEAM".localized
        } else {
            imageStart.image = UIImage(named: "startGrayIcon")
            labelDescription.text = "TALENT_DELETED_DREAM_TEAM".localized
        }
        labelItemName.text = item?.name?.uppercased()
        imageItemPhoto.image = item?.image
    }

    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
