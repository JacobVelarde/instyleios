//
//  INSCancelInvitationViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 14/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

enum CancelingType: Int {
    case none = 0
    case busy = 1
    case sick = 2
    case disagree = 3
    case other = 4
    
}

class INSCancelInvitationViewController: INSBaseViewController {

    @IBOutlet weak var busy: UIButton!
    @IBOutlet weak var sick: UIButton!
    @IBOutlet weak var disagree: UIButton!
    @IBOutlet weak var other: UIButton!
    var cancelingType:CancelingType = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }
    
    private func setup() {
        busy.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        busy.imageView?.contentMode = .scaleAspectFit
        sick.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        disagree.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        other.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor

    }

    @IBAction func checkAction(_ sender: UIButton) {
        resetButtons()
        switch sender.tag {
        case 1:
            busy.setImage(UIImage(named: "checkIcon"), for: .normal)
            cancelingType = .busy
        case 2:
            sick.setImage(UIImage(named: "checkIcon"), for: .normal)
            cancelingType = .sick
        case 3:
            disagree.setImage(UIImage(named: "checkIcon"), for: .normal)
            cancelingType = .disagree
        case 4:
            other.setImage(UIImage(named: "checkIcon"), for: .normal)
            cancelingType = .other
        default:
            break
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        self.performSegue(withIdentifier: "dismissView", sender: ["cancelingType":cancelingType])
    }
    
    private func resetButtons() {
        cancelingType = .none
        busy.setImage(nil, for: .normal)
        sick.setImage(nil, for: .normal)
        disagree.setImage(nil, for: .normal)
        other.setImage(nil, for: .normal)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancelAction(_ sender: Any) {
        self.performSegue(withIdentifier: "dismissView", sender: nil)
    }
    
}
