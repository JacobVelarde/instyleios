//
//  INSCustomersViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 11/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSCustomersViewController: INSBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var customerList:[String]?
    var lastViewSectionSelected: INSCustomerHeaderTableView?
    var expandedSectionHeaderNumber: Int = -1
    var isEditng:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_ADMIN_GET_CUSTOMERS_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
        })
        customerList = ["HEINEKEN", "OCESA", "BMW", "EDRINGTON", "GNP SEGUROS"]
        registerCells()
        
        // Do any additional setup after loading the view.
    }
    
    private func setup() {
        self.navigationController?.isNavigationBarHidden = true
        self.tableView.sectionHeaderHeight =  UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 10;
        self.tableView.estimatedRowHeight = 10
        self.tableView.rowHeight = UITableView.automaticDimension
    }

    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "INSCustomerTalentsTableViewCell"
            , bundle: Bundle(for: INSCustomerTalentsTableViewCell.self)), forCellReuseIdentifier: "customerTalentCell")
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
}

extension INSCustomersViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return customerList?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            return 3
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let talent = customerList?[indexPath.section] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "customerTalentCell", for: indexPath) as! INSCustomerTalentsTableViewCell
            cell.labelUsername.text = "ALEX LÓPEZ"
            if !isEditng {
                cell.hideCheckButton = true
            } else {
                cell.hideCheckButton = false
            }
            
            cell.selectAction = { (_) in
                
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let customer = customerList?[section] {
            let view:INSCustomerHeaderTableView = INSCustomerHeaderTableView.instanceFromNib() as! INSCustomerHeaderTableView
            view.labelTitle.text = customer
            view.tag = section
            view.seeAction = { (_) in
                self.isEditng = false
                self.validateActionFor(view: view)
            }
            
            view.upAction = {(_) in
                let section = view.tag
                if (self.expandedSectionHeaderNumber == section) {
                    self.tableViewCollapeSection(section, view)
                }
            }
            
            view.editAction = {(_) in
                self.isEditng = true
                self.validateActionFor(view: view)
            }
            return view
        }
        return nil
    }
    
    private func validateActionFor(view:INSCustomerHeaderTableView) {
        let section = view.tag
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            self.lastViewSectionSelected = view
            self.tableViewExpandSection(section, view)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                self.tableViewCollapeSection(section, view)
            } else {
                self.tableViewCollapeSection(self.expandedSectionHeaderNumber, self.lastViewSectionSelected)
                self.lastViewSectionSelected = view
                self.tableViewExpandSection(section, view)
            }
        }
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableViewCollapeSection(_ section: Int, _ view: INSCustomerHeaderTableView?) {
        if isEditng {
            view?.hideEditingButton = true
        } else {
            view?.buttonEdit.isHidden = false
            view?.buttonEye.isHidden = false
            view?.buttonUpArrow.isHidden = true
            view?.buttonSelected.isHidden = true
        }

        self.expandedSectionHeaderNumber = -1;
        var indexesPath = [IndexPath]()
        for i in 0...2 {
            let index = IndexPath(row: i, section: section)
            indexesPath.append(index)
        }
        self.tableView!.beginUpdates()
        self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
        self.tableView!.endUpdates()
    }

    func tableViewExpandSection(_ section: Int, _ view: INSCustomerHeaderTableView?) {
        if isEditng {
            view?.hideEditingButton = false
        } else {
            view?.buttonEdit.isHidden = true
            view?.buttonEye.isHidden = true
            view?.buttonUpArrow.isHidden = false
            view?.buttonSelected.isHidden = true
        }

        var indexesPath = [IndexPath]()
        for i in 0...2 {
            let index = IndexPath(row: i, section: section)
            indexesPath.append(index)
        }
        self.expandedSectionHeaderNumber = section
        self.tableView!.beginUpdates()
        self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
        self.tableView!.endUpdates()
    }
}
