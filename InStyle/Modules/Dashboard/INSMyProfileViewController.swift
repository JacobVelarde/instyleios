//
//  INSMyProfileViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 08/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSMyProfileViewController: INSBaseViewController {
        
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var buttonProfile: UIButton!
    @IBOutlet weak var stackViewName: UIStackView!
    @IBOutlet weak var textfieldName: INSFormTextField!
    @IBOutlet weak var stackViewSurnames: UIStackView!
    @IBOutlet weak var texfieldSurnames: INSFormTextField!
    @IBOutlet weak var stackViewEmail: UIStackView!
    @IBOutlet weak var textfieldEmail: INSFormTextField!
    @IBOutlet weak var stackViewPhone: UIStackView!
    @IBOutlet weak var textfieldPhone: INSFormTextField!
    @IBOutlet weak var stackViewBirthday: UIStackView!
    @IBOutlet weak var textfieldBirthday: INSFormTextField!
    @IBOutlet weak var imageViewEditIcon: UIImageView!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var stackviewDatePicker: UIStackView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    let user = INSDataController.sharedDataController.user

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(editProfile(_:)))
        imageViewEditIcon.addGestureRecognizer(tapGesture)
        setUp()
        populateView()
    }
    
    func setUp() {
        buttonSave.isHidden = true
        imageViewEditIcon.layer.borderColor = UIColor.white.cgColor
        buttonProfile.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        buttonSave.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        textfieldBirthday.inputView = stackviewDatePicker
        datePicker.locale = Locale(identifier: "es")
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    private func populateView() {
        labelUserName.text = user.name?.uppercased()
        buttonProfile.setTitle(user.profileStr?.uppercased(), for: .normal)
        textfieldName.text = user.completeName
        textfieldEmail.text = user.email
        textfieldPhone.text = user.telephone
        texfieldSurnames.text = user.surnames
        if user.birthDate == "OCULTAR_CAMPO" {
            stackViewBirthday.isHidden = true
        } else {
            textfieldBirthday.text = user.birthDate
        }
        if user.image != nil {
            imageViewPhoto.image = user.image
        } else {
            guard let urlprofile = user.url_profile, !urlprofile.isEmpty else {
                self.imageViewPhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                return
            }
            let requestDispatcher = INSRequestDispatcher()
            requestDispatcher.downloadData(url: user.url_profile, completion: { (success, data)  in
                if success, let dataImage = data as? Data {
                    self.user.image = UIImage(data: dataImage)
                    self.imageViewPhoto.image = self.user.image
                } else {
                    self.imageViewPhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                }
            })

        }
    }
    
    @objc func editProfile(_ sender: UITapGestureRecognizer) {
        print("Me presionaste")
        buttonSave.isHidden = false
        textfieldName.isEnabled = true
        textfieldName.text = user.name
        stackViewSurnames.isHidden = false
        textfieldEmail.isEnabled = true
        textfieldPhone.isEnabled = true
        textfieldBirthday.isEnabled = true
        textfieldName.becomeFirstResponder()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        self.showActivityIndicator()
        let params = [Keys.names:textfieldName.trimmingWhiteSpaces, Keys.surnames:texfieldSurnames.trimmingWhiteSpaces, Keys.email:textfieldEmail.trimmingWhiteSpaces, Keys.telephone:textfieldPhone.trimmingWhiteSpaces, Keys.birthday:textfieldBirthday.trimmingWhiteSpaces]
        INSRequestDispatcher().dispatchRequest(opCode: .INS_UPDATE_PERFIL_CODE, requestParameters: params, completionHandler: { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.showAlertWith(title: "PERFIL ACTUALIZADO", imageName: "checkIcon", alertType: .imageBelowMessage)
                } else {
                    self.showAlertControllerWith(message: error?.errorMessage)
                }
            }
        })
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.view.endEditing(true)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: datePicker.date)
        textfieldBirthday.text = strDate
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.view.endEditing(true)
    }
}

extension INSMyProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textfieldBirthday {
            stackviewDatePicker.isHidden = false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textfieldName {
            textfieldName.text = textfieldName.trimmingWhiteSpaces
            texfieldSurnames.becomeFirstResponder()
        } else if textField == texfieldSurnames {
            texfieldSurnames.text = texfieldSurnames.trimmingWhiteSpaces
            textfieldEmail.becomeFirstResponder()
        } else if textField == textfieldEmail {
            textfieldEmail.text = textfieldEmail.trimmingWhiteSpaces
            textfieldPhone.becomeFirstResponder()
        } else if textField == textfieldPhone {
            textfieldPhone.text = textfieldPhone.trimmingWhiteSpaces
            textfieldBirthday.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
}

extension INSMyProfileViewController {
    //MARK: Keyboard management
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboard = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboard.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}
