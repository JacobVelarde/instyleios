//
//  INSFilterTalents.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 11/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSFilterTalentsManager {
    let dataManager = INSDataController.sharedDataController
    var applyingFilters: [String: Any] = [:]

    private func getConstantFilters() -> [ItemFilter] {
        let filterOptions:[ItemFilter] = [
            ItemFilter(id:"EDAD", name:"EDAD"),
            ItemFilter(id:"ALTURA", name:"ALTURA"),
            ItemFilter(id:"PESO", name:"PESO"),
            ItemFilter(id:"CALZADO", name:"CALZADO"),
            ItemFilter(id:"CAMISA", name:"CAMISA"),
            ItemFilter(id:"BUSTO", name:"BUSTO"),
            ItemFilter(id:"SACO", name:"SACO"),
            ItemFilter(id:"CINTURA", name:"CINTURA"),
            ItemFilter(id:"PANTALON", name:"PANTALON"),
            ItemFilter(id:"CADERA", name:"CADERA"),
            ItemFilter(id:"OJOS", name:"OJOS"),
            ItemFilter(id:"CABELLO", name:"CABELLO"),
        ]
        return filterOptions
    }
    
    internal func getFilterOptions(gender:String?, profile:String? = "TALENTOS") -> [ItemFilter] {
        guard gender != "" && gender != nil else {
            return getConstantFilters()
        }
        
        if profile == "COORDINADORES" {
            return []
        }
        
        if gender == "FEMENINO" {
            return getConstantFilters().filter{$0.id != "CAMISA" && $0.id != "SACO" && $0.id != "PANTALON"}
        } else {
            return getConstantFilters().filter{$0.id != "BUSTO" && $0.id != "CINTURA" && $0.id != "CADERA"}
        }
    }
    
    internal func getGetListFor(filter: ItemFilter, gender:String? = "FEMENINO") -> [String]{
        var key = filter.id?.lowercased()
        switch filter.id {
        case "SACO", "CAMISA","PANTALON", "BUSTO", "CINTURA", "CADERA":
            key = "generico"
            break
        case "CALZADO":
            if gender == "FEMENINO" {
                key = "calzado_mujer"
            } else {
                key = "calzado_hombre"
            }
            break
        default:
            break
        }
        
        if let path = Bundle.main.path(forResource: "INSTalentFilters", ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                let json = try? JSONSerialization.jsonObject(with: data)
                if let dictionary = json as? [String: Any] {
                    let array = dictionary[key ?? ""] as? [Any]
                    if let items = array {
                        let filterArray = items.map{$0 as! String }
                        return filterArray
                    }
                }
            } catch {
            }
        }
        return []

    }
    
    func getTalentPanel(completion: @escaping (_ success : Bool, _ userList: [Any]?) -> Void) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_TALENT_PANEL_CODE, requestParameters: nil, queryParameters: [Keys.user_id: INSDataController.sharedDataController.user.idUser ?? -1], completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    completion(true, INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode:.INS_GET_TALENT_PANEL_CODE))
                } else {
                    completion(false, nil)
                }
            }
        })
    }
    
    func fetchTalentFilter(parameters:[String:Any], opCode: INSConstants.OperationCodes, completion: @escaping (_ success : Bool, _ userList: [Any]?) -> Void) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: opCode, requestParameters: parameters, completionHandler: { (success, jsonResponse, response, error) in
//        requestDispatcher.dispatchRequest(opCode: .INS_FILTER_TALENTS_CODE, requestParameters: parameters, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
//                    completion(true, INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode:.INS_FILTER_TALENTS_CODE) as? [INSTalent])
                    completion(true, INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode:opCode))
                } else {
                    completion(false, nil)
                }
            }
        })
    }
}

class ItemFilter {
    var id: String?
    var name: String?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
