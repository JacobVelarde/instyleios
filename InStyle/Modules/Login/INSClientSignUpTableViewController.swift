//
//  INSClientSignUpTableViewController.swift
//  InStyle
//
//  Created by Alejandro López on 1/13/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSClientSignUpTableViewController: UITableViewController {
    
    @IBOutlet weak var textfieldFullName: INSFormTextField!
    @IBOutlet weak var textfieldCompanyName: INSFormTextField!
    @IBOutlet weak var textfieldCompanyAddress: INSFormTextField!
    @IBOutlet weak var textfieldCompanyEmail: INSFormTextField!
    @IBOutlet weak var textfieldConfirmCompanyEmail: INSFormTextField!
    @IBOutlet weak var textfieldPassword: INSFormTextField!
    @IBOutlet weak var textfieldCompanyRol: INSFormTextField!
    @IBOutlet weak var textfieldPhoneNumber: INSFormTextField!
    @IBOutlet weak var textfieldLinkedInUrl: INSFormTextField!
    @IBOutlet weak var textfieldWebPageUrl: INSFormTextField!
    @IBOutlet weak var textfieldInstagramUrl: INSFormTextField!
    @IBOutlet weak var buttonSubmit: UIButton!
    fileprivate var dataManager = INSFormDataManager()
    fileprivate let client = INSCustomer()
    fileprivate var regionCodes: [INSFormDataManager.RegionCodeInfo]?
    
    fileprivate enum TextfieldType: Int {
        case fullName = 1, companyName, companyAddress, companyEmail, confirmCompanyEmail, password, companyRol, phoneNumber, linkedInUrl, webPageUrl, instagramUrl
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataManager.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        tableView.addGestureRecognizer(tapGesture)
        showActivityIndicator()
        dataManager.getRegionCodes()
    }

    fileprivate func updateTextField(_ textfield: INSFormTextField) {
        if textfield.isValidated && (textfield == textfieldInstagramUrl || textfield == textfieldLinkedInUrl || textfield == textfieldWebPageUrl) {
            textfieldWebPageUrl.updateUIForCorrectInfo()
            textfieldInstagramUrl.updateUIForCorrectInfo()
            textfieldLinkedInUrl.updateUIForCorrectInfo()
        }
        else if textfield.isValidated || textfield.isEditing {
            textfield.updateUIForCorrectInfo()
        }
        else if !textfield.isValidated && (textfield == textfieldWebPageUrl || textfield == textfieldLinkedInUrl || textfield == textfieldInstagramUrl) {
            textfieldWebPageUrl.updateUIForWrongInfo()
            textfieldInstagramUrl.updateUIForWrongInfo()
            textfieldLinkedInUrl.updateUIForWrongInfo()
        }
        else {
            textfield.updateUIForWrongInfo()
        }
    }
    
    fileprivate func updateSubmitButton() {
        if textfieldFullName.isValidated, textfieldCompanyName.isValidated, textfieldCompanyAddress.isValidated, textfieldCompanyEmail.isValidated, textfieldConfirmCompanyEmail.isValidated, textfieldPassword.isValidated, textfieldCompanyRol.isValidated, textfieldPhoneNumber.isValidated, (textfieldLinkedInUrl.isValidated || textfieldWebPageUrl.isValidated || textfieldInstagramUrl.isValidated) {
            buttonSubmit.backgroundColor = #colorLiteral(red: 0.1058823529, green: 0.5215686275, blue: 0.568627451, alpha: 1)
            buttonSubmit.titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            buttonSubmit.isEnabled = true
        } else {
            buttonSubmit.backgroundColor = #colorLiteral(red: 0.6862745098, green: 0.6901960784, blue: 0.6901960784, alpha: 1)
            buttonSubmit.titleLabel?.textColor = #colorLiteral(red: 0.3725490196, green: 0.3764705882, blue: 0.3764705882, alpha: 1)
            buttonSubmit.isEnabled = false
        }
    }
    
    @objc func tapAction(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @objc func selectCodeRegionAction() {
        guard let codes = regionCodes else { return }
        let alert = UIAlertController(title: "REGION_CODES_TITLE".localized, message: "SELECT_REGION_CODE_MESSAGE".localized, preferredStyle: .actionSheet)
        for code in codes {
            let optionFormat = "\(code.countryName)  \(code.countryCode)"
            alert.addAction(UIAlertAction(title: optionFormat, style: .default, handler: { (action) in
                self.textfieldPhoneNumber.setRegionCode(code)
            }))
        }
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        showActivityIndicator()
        client.name = textfieldFullName.text
        client.company = textfieldCompanyName.text
        client.companyAddress = textfieldCompanyAddress.text
        client.email = textfieldCompanyEmail.text
        client.jobPosition = textfieldCompanyRol.text
        client.telephone = textfieldPhoneNumber.text
        client.linkedInUrl = textfieldLinkedInUrl.text
        client.websiteUrl = textfieldWebPageUrl.text
        client.instagramUrl = textfieldInstagramUrl.text
        dataManager.signupClient(client, password: textfieldPassword.text)
    }
}

extension INSClientSignUpTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.wasEdited, !textField.isValidated {
            updateTextField(textField as! INSFormTextField)
        }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            guard let textfieldType = TextfieldType(rawValue: textField.tag) else { return true }
            switch textfieldType {
            case .fullName:
                return updatedText.count <= INSConstants.FormFieldLength.fullNameMaxLength
            case .companyEmail, .confirmCompanyEmail:
                return updatedText.count <= INSConstants.FormFieldLength.emailMaxLength
            case .phoneNumber:
                return updatedText.count <= INSConstants.FormFieldLength.phoneNumberMaxLength
            default:
                return true
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        var isValidInfo: Bool = false
        if let text = textField.text {
            textField.text = text.trimmingCharacters(in: .whitespacesAndNewlines)
            
            guard let textfieldType = TextfieldType(rawValue: textField.tag) else { return }
            switch textfieldType {
            case .fullName:
                isValidInfo = textField.validateFullNameFormat()
            case .companyEmail:
                isValidInfo = textField.validateEmailFormat()
            case .confirmCompanyEmail:
                isValidInfo = textField.validateEmailFormat() && textField.text == textfieldCompanyEmail.text
            case .phoneNumber:
                isValidInfo = textField.validatePhoneNumberFormat()
            case .linkedInUrl, .webPageUrl, .instagramUrl:
                if !textField.text!.isEmpty {
                    isValidInfo = textField.validateUrlFormat()
                } else if ((textfieldWebPageUrl.isValidated && !textfieldWebPageUrl.text!.isEmpty) || (textfieldLinkedInUrl.isValidated && !textfieldLinkedInUrl.text!.isEmpty) || (textfieldInstagramUrl.isValidated &&  !textfieldInstagramUrl.text!.isEmpty)) {
                    isValidInfo = true
                }
            default:
                isValidInfo = textField.text?.count ?? 0 > INSConstants.FormFieldLength.defaultFieldMinLength
            }
        }
        
        textField.wasEdited = true
        textField.isValidated = isValidInfo
        updateTextField(textField as! INSFormTextField)
        updateSubmitButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let textfieldType = TextfieldType(rawValue: textField.tag) else { return true }
        switch textfieldType {
        case .fullName:
            textfieldCompanyName.becomeFirstResponder()
        case .companyName:
            textfieldCompanyAddress.becomeFirstResponder()
        case .companyAddress:
            textfieldCompanyEmail.becomeFirstResponder()
        case .companyEmail:
            textfieldConfirmCompanyEmail.becomeFirstResponder()
        case .confirmCompanyEmail:
            textfieldPassword.becomeFirstResponder()
        case .password:
            textfieldCompanyRol.becomeFirstResponder()
        case .companyRol:
            textfieldPhoneNumber.becomeFirstResponder()
        case .phoneNumber:
            textfieldLinkedInUrl.becomeFirstResponder()
        case .linkedInUrl:
            textfieldWebPageUrl.becomeFirstResponder()
        case .webPageUrl:
            textfieldInstagramUrl.becomeFirstResponder()
        default:
            view.endEditing(true)
        }

        return true
    }
}

extension INSClientSignUpTableViewController: FormDataManagerDelegate {
    
    func didSuccessForOperaion(_ operation: WsOperation, response: Any?) {
        self.dismissActivityIndicator()

        switch operation {
        case .clientSignUp:
            let message = response as? String ?? "DEFAULT_WEB_SUCCESS_MEESAGE".localized
            self.showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: message, actions: [UIAlertAction(title: "DEFAULT_ALERT_ACTION".localized, style: .default, handler: { action in
                self.performSegue(withIdentifier: "unwindToLogin", sender: self)
            })
            ])
        case .regionCodes:
            guard let codes = response as? [INSFormDataManager.RegionCodeInfo] else { break }
            regionCodes = codes
            textfieldPhoneNumber.setPhoneNumberLeftView(self)
        default:
            break
        }
        
    }
    
    func didFailForOperation(_ operation: WsOperation, message: String?) {
        self.dismissActivityIndicator()
        self.showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: message)
    }
}

extension INSFormTextField {
        
    fileprivate func setPhoneNumberLeftView(_ controller: INSClientSignUpTableViewController) {
        let defaultRegionCode = "+52"
        let _leftView = UIView(frame: CGRect(x: 10, y: 0, width: 100, height: self.frame.size.height))
        let leftButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: self.frame.size.height))
        leftButton.setTitleColor(INSConstants.ThemeColor.darkGray, for: .normal)
        leftButton.titleLabel?.font = self.font
        leftButton.addTarget(controller, action: #selector(controller.selectCodeRegionAction), for: .touchUpInside)
        _leftView.addSubview(leftButton)
        self.leftView = _leftView
        self.leftViewMode = .always
        let defaultRegion = controller.regionCodes?.filter({ (regionCode) -> Bool in
            regionCode.countryCode == defaultRegionCode
        })
        if let region = defaultRegion?.first {
            setRegionCode(region)
        }
    }
    
    fileprivate func setRegionCode(_ regionCode: INSFormDataManager.RegionCodeInfo) {
        guard let button = self.leftView?.subviews.first as? UIButton else {
            return
        }
        button.setImage(regionCode.flag, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 40)
        button.setTitle(regionCode.countryCode, for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }
}
