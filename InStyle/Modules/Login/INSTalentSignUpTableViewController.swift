//
//  INSTalentSignUpTableViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 17/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import MobileCoreServices

class INSTalentSignUpTableViewController: UITableViewController {
    fileprivate weak var currentPhotoButtonPressed: UIButton?
    fileprivate var photoBook: [Int: UIImage] = [:]
    fileprivate var selfie: UIImage?
    fileprivate let minimumPhotos = 6
    fileprivate let talent = INSTalent()
    fileprivate let dataManager = INSFormDataManager()
    fileprivate let cameraManager = INSCameraManager()
    fileprivate var currentEditing: UITextField?

    @IBOutlet weak var buttonFemaleOption: UIButton!
    @IBOutlet weak var buttonMaleOption: UIButton!
    @IBOutlet weak var textfieldName: INSFormTextField!
    @IBOutlet weak var textfieldSurname: INSFormTextField!
    @IBOutlet weak var textfieldEmail: INSFormTextField!
    @IBOutlet weak var textfieldConfirmEmail: INSFormTextField!
    @IBOutlet weak var textfieldPassword: INSFormTextField!
    @IBOutlet weak var textfieldMonthOfBirth: INSFormTextField!
    @IBOutlet weak var textfieldDayOfBirth: INSFormTextField!
    @IBOutlet weak var textfieldYearOfBirth: INSFormTextField!
    @IBOutlet weak var instagramProfileTextField: INSFormTextField!
    @IBOutlet weak var photoBookCollection: UICollectionView!
    @IBOutlet weak var buttonSelfie: UIButton!
    @IBOutlet weak var switchTermsNConditions: UISwitch!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var termsAndConditionsButton: UIButton!

    fileprivate enum TextfieldType: Int {
        case name = 2, surname, email, confirmEmail, password, monthOfBirth, dayOfBirth, yearOfBirth, instagramProfile
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(takePhoto(_:)), name: INSItemCollectionViewCell.itemCollectionSelectedNotificationName, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        photoBookCollection.register(UINib(nibName: "INSItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photoCell")
        dataManager.delegate = self
        termsAndConditionsButton.titleLabel?.numberOfLines = 0
        termsAndConditionsButton.titleLabel?.adjustsFontSizeToFitWidth = true
        textfieldDayOfBirth.setInputViewDatePicker(target: self, selector: #selector(tapOnDone))
        textfieldMonthOfBirth.setInputViewDatePicker(target: self, selector: #selector(tapOnDone))
        textfieldYearOfBirth.setInputViewDatePicker(target: self, selector: #selector(tapOnDone))
        cameraManager.delegate = self
        tableView.addGestureRecognizer(tapGesture)
        tableView.rowHeight = UITableView.automaticDimension
    }

    @objc fileprivate func tapAction(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    fileprivate func updateTextField(_ textfield: INSFormTextField) {
        if textfield.isValidated || textfield.isEditing {
            textfield.updateUIForCorrectInfo()
        } else {
            textfield.updateUIForWrongInfo()
        }
    }

    fileprivate func updateSubmitButton() {
        if (buttonMaleOption.isSelected || buttonFemaleOption.isSelected), textfieldName.isValidated, textfieldSurname.isValidated, textfieldEmail.isValidated, textfieldConfirmEmail.isValidated, textfieldPassword.isValidated, textfieldMonthOfBirth.isValidated, textfieldDayOfBirth.isValidated, textfieldYearOfBirth.isValidated, photoBook.count == minimumPhotos, let _ = selfie, switchTermsNConditions.isOn {
            buttonSubmit.backgroundColor = #colorLiteral(red: 0.1058823529, green: 0.5215686275, blue: 0.568627451, alpha: 1)
            buttonSubmit.titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            buttonSubmit.isEnabled = true
        } else {
            buttonSubmit.backgroundColor = #colorLiteral(red: 0.6862745098, green: 0.6901960784, blue: 0.6901960784, alpha: 1)
            buttonSubmit.titleLabel?.textColor = #colorLiteral(red: 0.3725490196, green: 0.3764705882, blue: 0.3764705882, alpha: 1)
            buttonSubmit.isEnabled = false
        }
    }

    @objc fileprivate func takePhoto(_ notification: Notification) {
        guard let buttonPressed = notification.object as? UIButton else { return }
        currentPhotoButtonPressed = buttonPressed
        cameraManager.launchCamera(for: kUTTypeImage)
    }
    
    fileprivate func showSuccesfulSignUpMessage(for response: Any?) {
        let message = response as? String ?? "DEFAULT_WEB_SUCCESS_MEESAGE".localized
        self.showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: message, actions:
            [(UIAlertAction(title: "DEFAULT_ALERT_ACTION".localized, style: .default, handler: { (action) in
                self.performSegue(withIdentifier: "unwindToLogin", sender: self)
            }))
        ])
    }

    //MARK: Actions
    
    @objc fileprivate func tapOnDone() {
        if let datePicker = currentEditing?.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM dd yyyy"
            let dateValues = dateFormatter.string(from: datePicker.date).components(separatedBy: " ")
            textfieldDayOfBirth.text = dateValues[0]
            textfieldMonthOfBirth.text = dateValues[1]
            textfieldYearOfBirth.text = dateValues[2]
        }
        currentEditing?.resignFirstResponder()
    }

    @IBAction func showTermsNConditionsAction(_ sender: UIButton) {
        //TODO: Open the terms and conditions pdf or flow
    }

    @IBAction func genderSelectionAction(_ sender: UIButton) {
        sender.isSelected = true
        if sender == buttonFemaleOption {
            sender.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.5960784314, blue: 0.6509803922, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            buttonMaleOption.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
            buttonMaleOption.setTitleColor(#colorLiteral(red: 0.1254901961, green: 0.5960784314, blue: 0.6509803922, alpha: 1), for: .normal)
        } else {
            sender.backgroundColor = #colorLiteral(red: 0.1254901961, green: 0.5960784314, blue: 0.6509803922, alpha: 1)
            sender.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            buttonFemaleOption.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
            buttonFemaleOption.setTitleColor(#colorLiteral(red: 0.1254901961, green: 0.5960784314, blue: 0.6509803922, alpha: 1), for: .normal)
        }
        updateSubmitButton()
    }

    @IBAction func switchTermsValue(_ sender: UISwitch) {
        updateSubmitButton()
    }

    @IBAction func takeSelfiePhoto(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        cameraManager.launchCamera(for: kUTTypeImage)
    }

    @IBAction func submitAction(_ sender: UIButton) {
        talent.gender = buttonMaleOption.isSelected ? INSTalent.GenderType.male : INSTalent.GenderType.female
        talent.name = textfieldName.text
        talent.surnames = textfieldSurname.text
        talent.email = textfieldEmail.text
        talent.birthDate = "\(textfieldYearOfBirth.text!)-\(textfieldMonthOfBirth.text!)-\(textfieldDayOfBirth.text!)"
        talent.instagramProfile = instagramProfileTextField.text
        talent.book = photoBook.map { (data) -> INSWebMultimedia in
            return INSWebMultimedia(data.value.jpegData(compressionQuality: 1.0)!)
        }
        talent.selfies = INSTalent.Selfies(face: INSWebMultimedia(selfie?.jpegData(compressionQuality: 1.0)), sideFace: nil, wholeBody: nil)
        self.showActivityIndicator()
        dataManager.signupTalent(talent, password: textfieldPassword.text)
    }
}

extension INSTalentSignUpTableViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentEditing = textField
        if textField == instagramProfileTextField {
            textField.text = "instagram.com/"
        }
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.wasEdited, !textField.isValidated {
            updateTextField(textField as! INSFormTextField)
        }

        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)

            guard let textfieldType = TextfieldType(rawValue: textField.tag) else { return true }
            switch textfieldType {
            case .name, .surname:
                return updatedText.count <= INSConstants.FormFieldLength.fullNameMaxLength
            case .email, .confirmEmail:
                return updatedText.count <= INSConstants.FormFieldLength.emailMaxLength
            case .dayOfBirth, .monthOfBirth:
                return updatedText.count <= INSConstants.FormFieldLength.dayNMonthMaxLength
            case .yearOfBirth:
                return updatedText.count <= INSConstants.FormFieldLength.yearMaxLength
            default:
                return true
            }
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        var isValidInfo: Bool = true
        if let text = textField.text {
            textField.text = text.trimmingCharacters(in: .whitespacesAndNewlines)

            guard let textfieldType = TextfieldType(rawValue: textField.tag) else { return }
            switch textfieldType {
            case .name:
                isValidInfo = textField.validateNameOrSurnameFormat()
            case .surname:
                isValidInfo = textField.validateNameOrSurnameFormat()
            case .email:
                isValidInfo = textField.validateEmailFormat()
            case .confirmEmail:
                isValidInfo = textField.validateEmailFormat() && textField.text == textfieldEmail.text
            case .dayOfBirth:
                isValidInfo = textField.validateDayOfBirth()
            case .monthOfBirth:
                isValidInfo = textField.validateMonthOfBirth()
            case .yearOfBirth:
                isValidInfo = textField.validateYearOfBirth()
            case .instagramProfile:
                if textField.text != "instagram.com/" {
                    isValidInfo = textField.validateUrlFormat()
                } else {
                    isValidInfo = true
                    textField.text = ""
                }
            default:
                isValidInfo = textField.text?.count ?? 0 > INSConstants.FormFieldLength.defaultFieldMinLength
            }
        }

        textField.wasEdited = true
        textField.isValidated = isValidInfo
        if textField == textfieldDayOfBirth || textField == textfieldMonthOfBirth || textField == textfieldYearOfBirth {
            //validate all date fields because they're all set at once
            textfieldDayOfBirth.isValidated = true
            textfieldMonthOfBirth.isValidated = true
            textfieldYearOfBirth.isValidated = true
        }
        updateTextField(textField as! INSFormTextField)
        updateSubmitButton()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let textfieldType = TextfieldType(rawValue: textField.tag) else { return true }
        switch textfieldType {
        case .name:
            textfieldSurname.becomeFirstResponder()
        case .surname:
            textfieldEmail.becomeFirstResponder()
        case .email:
            textfieldConfirmEmail.becomeFirstResponder()
        case .confirmEmail:
            textfieldPassword.becomeFirstResponder()
        case .password:
            textfieldMonthOfBirth.becomeFirstResponder()
        default:
            view.endEditing(true)
        }

        return true
    }
}

extension INSTalentSignUpTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return minimumPhotos
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? INSItemCollectionViewCell else {
            return UICollectionViewCell()
        }

        itemCell.buttonIcon.tag = indexPath.row
        return itemCell
    }
}

extension INSTalentSignUpTableViewController: CameraManagementDelegate {
    func didPhotoTaken(image: UIImage) {
        currentPhotoButtonPressed?.setImage(image, for: .normal)
        if currentPhotoButtonPressed == buttonSelfie {
            selfie = image
        } else {
            if let buttonIndex = currentPhotoButtonPressed?.tag {
                photoBook[buttonIndex] = image
            }
        }
        updateSubmitButton()
    }
}

extension INSTalentSignUpTableViewController: FormDataManagerDelegate {

    func didSuccessForOperaion(_ operation: WsOperation, response: Any?) {
        if operation == .talentSignUp {
            if let object = response as? [String: Int], let idRequest = object["id_solicitud"] {
                let id = String(idRequest)
                dataManager.uploadPhotos(selfie: selfie!, photoBook: photoBook, for: id)
            }
            else {
                self.dismissActivityIndicator()
                showSuccesfulSignUpMessage(for: response)
            }
        }
        else {
            self.dismissActivityIndicator()
            showSuccesfulSignUpMessage(for: response)
        }
    }

    func didFailForOperation(_ operation: WsOperation, message: String?) {
        self.dismissActivityIndicator()
        self.showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: message)
    }
}
