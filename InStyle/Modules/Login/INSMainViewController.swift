//
//  INSMainViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 28/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSMainViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topView: UIImageView!
    @IBOutlet weak var bottomView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
    
    fileprivate let contentDataSize = 100_000
    fileprivate var items: [AreaItems]?
    fileprivate let dataController = INSDataController.sharedDataController
    fileprivate let requestDispatcher = INSRequestDispatcher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUp()
        registerCells()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setGradientBackground()
    }
    
    fileprivate func setUp() {
        setTableContent()
        // Setting title attributes
        let attributedString = NSMutableAttributedString(attributedString: titleLabel.attributedText!)
        attributedString.addLineSpacing(10.0)
        attributedString.addWordSpacing(3.0)
        titleLabel.attributedText = attributedString
        titleLabel.textAlignment = .center
        heightTableViewConstraint.constant = INSUtility.getValueForHeightMainTableView()
    }
    
    fileprivate func setTableContent() {
        let firstAttributedString = NSMutableAttributedString(string: "MODEL_MANAGEMENT_OPTION".localized)
        firstAttributedString.addAttribute(.foregroundColor, value: INSConstants.ThemeColor.darkTurquoise, range: NSRange(location: firstAttributedString.string.count - 2, length: 1))
        let secondAttributedString = NSMutableAttributedString(string: "MODEL_IMAGE_HOSTESS_OPTION".localized)
        secondAttributedString.addAttribute(.foregroundColor, value: INSConstants.ThemeColor.darkTurquoise, range: NSRange(location: secondAttributedString.string.count - 2, length: 1))
        let thirdAttributedString = NSMutableAttributedString(string: "ACTORS_TALENTS_OPTION".localized)
        thirdAttributedString.addAttribute(.foregroundColor, value: INSConstants.ThemeColor.darkTurquoise, range: NSRange(location: thirdAttributedString.string.count - 2, length: 1))
        
        items = [
            AreaItems.init(firstAttributedString, image: "photo_mm", segueId: "showDashboardSegue"),
            AreaItems.init(secondAttributedString, image: "photo_mie", segueId: "showMainDashboardSegue"),
            AreaItems.init(thirdAttributedString, image: "photo_at", segueId: "showDashboardSegue")
        ]
        // set the tableview to the middle of the long list
        tableView.scrollToRow(at: IndexPath(row: contentDataSize/2, section: 0), at: .middle, animated: false)
    }
        
    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "INSAreaTableViewCell", bundle: Bundle(for: INSAreaTableViewCell.self)), forCellReuseIdentifier: "areaCell")
    }
    
    fileprivate func setGradientBackground() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.white.withAlphaComponent(1.0).cgColor, UIColor.white.withAlphaComponent(0.1).cgColor]
        gradientLayer.frame = topView.bounds
        gradientLayer.locations = [0.5, 1.0]
        topView.layer.insertSublayer(gradientLayer, at: 0)
        
        let gradientLayerBottom = CAGradientLayer()
        gradientLayerBottom.colors = [UIColor.white.withAlphaComponent(1.0).cgColor, UIColor.white.withAlphaComponent(0.1).cgColor]
        gradientLayerBottom.frame = bottomView.bounds
        gradientLayerBottom.locations = [0.5, 1.0]
        gradientLayerBottom.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayerBottom.endPoint = CGPoint(x: 0.5, y: 0.0)
        bottomView.layer.insertSublayer(gradientLayerBottom, at: 0)
    }
    
    fileprivate func getTalentProfile() {
        self.showActivityIndicator()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_TALENT_PROFILE_CODE, requestParameters: ["user_id": dataController.user.idUser ?? "-1"]) { (success, jsonResponse, urlResponse, error) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    if let code = jsonResponse["codigo"] as? Int, code != INSHTTPCodes.GO_TO_DASHBOARD.rawValue {
                        self.dataController.initTalentProfile(jsonResponse: jsonResponse["objeto"] as? [String : Any])
                    }
                    self.performSegue(withIdentifier: "showTalentFormSegue", sender: self)
                } else {
                    print(error?.errorMessage as Any)
                }
            }
        }
    }
    
    @IBAction func unwindToMainMenu(for unwindSegue: UIStoryboardSegue) {
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.showActivityIndicator()
        requestDispatcher.dispatchRequest(opCode: .INS_LOGOUT_CODE, requestParameters: nil) { (success, jsonResponse, urlResponse, error) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                self.navigationController?.popViewController(animated: true)
            }
        }        
    }
}

extension INSMainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contentDataSize
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "areaCell", for: indexPath) as! INSAreaTableViewCell
        if let item = items?[indexPath.row % 3] {
            cell.item = item
            cell.selectedCell = { (_) in
                print("You've selected  \(item.title?.string ?? String.emptyString)")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = items?[indexPath.row % 3], let segueId = item.segue, !segueId.isEmpty else { return }
        
        let currentUser = dataController.user
        if currentUser.profile == .talent, let firstTime = currentUser.firstTimeForTalent, firstTime, indexPath.row % 3 == 1 {
            getTalentProfile()
        } else {
            performSegue(withIdentifier: segueId, sender: self)
        }
    }
}
