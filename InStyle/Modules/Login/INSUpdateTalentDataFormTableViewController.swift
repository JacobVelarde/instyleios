//
//  INSUpdateTalentDataFormTableViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import MobileCoreServices

class INSUpdateTalentDataFormTableViewController: UITableViewController {
    fileprivate weak var currentPhotoButtonPressed: UIButton?
    fileprivate var dataManager = INSFormDataManager()
    fileprivate let cameraManager = INSCameraManager()
    fileprivate var eyeColorPicker = UIPickerView()
    fileprivate var hairColorPicker = UIPickerView()
    fileprivate var documentManager = INSDocumentManager()
    var talentData: INSTalent?

    fileprivate enum CellIndices: Int, CaseIterable {
        case titleCell = 0
        case genderCell = 1
        case ageCell = 2
        case heightCell = 3
        case upperBodyCell = 4
        case middleBodyCell = 5
        case lowBodyCell = 6
        case weightCell = 7
        case footwearCell = 8
        case eyesNHairCell = 9
        case specialitiesCell = 10
        case selfiesCell = 11
        case photoBookCell = 12
        case polaroidsCell = 13
        case videoCell = 14
        case filesCell = 15
        case foreignFilesCell = 16
        case submitCell = 17
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addObservers()
        registerCells()
        dataManager.delegate = self
        cameraManager.delegate = self
        documentManager.delegate = self
        verifyTalentData()
    }
    
    fileprivate func verifyTalentData() {
        // verify if current user logged in is a Talent profile
        if let talent = dataManager.session.user as? INSTalent {
            loadTalentData(talent)
        }
        // obtain the talent info to be shown
        else if let talent = talentData {
            self.showActivityIndicator()
            dataManager.getTalentDetailInfo(talent)
        }
    }
    
    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "INSSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "sliderCell")
        tableView.register(UINib(nibName: "INSCollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "collectionCell")
    }
    
    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(takePhotoAction(_:)), name: INSItemCollectionViewCell.itemCollectionSelectedNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(takeSelfiesAndPolaroids(_:)), name: INSPhotoItemCollectionViewCell.photoItemSelectedNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectSpecialityAction(_:)), name: INSFilterCollectionViewCell.filterCollectionNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didSliderValueChanged(_:)), name: INSSliderTableViewCell.sliderChangeNotificationName, object: nil)
    }
    
    fileprivate func loadTalentData(_ talent: INSTalent) {
        if let gender = talent.gender {
            if gender == .female {
                dataManager.updateInfo(INSTalent.GenderType.female, for: .gender)
                tableView.reloadData()
            }
        }
    }
    
    fileprivate func takePhoto() {
        cameraManager.launchCamera(for: kUTTypeImage)
    }
    
    fileprivate func updateGenderSelectionButton(_ button: UIButton) {
        if button.isSelected {
            button.titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            button.backgroundColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
        } else {
            button.titleLabel?.textColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    fileprivate func setThumbnailForVideo(_ button: UIButton, url: Any?, thumbnail: UIImage) {
        button.setImage(thumbnail, for: .normal)
        button.layer.cornerRadius = button.frame.size.width/2
        let videoType: INSFormDataManager.VideoType = button.tag == 600 ? .demoReel : .casting
        dataManager.updateInfo([videoType: (url, thumbnail)], for: .videos)
    }
    
    fileprivate func verifyContentAvailability(for cell: UITableViewCell) {
        if let profile = dataManager.session.user.profile, profile != .talent {
            cell.isUserInteractionEnabled = false
        }
    }
    
    //MARK: IBActions
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func femaleSelectionAction(_ sender: UIButton) {
        dataManager.updateInfo(INSTalent.GenderType.female, for: .gender)
        tableView.reloadData()
    }
    
    @IBAction func maleSelectionAction(_ sender: UIButton) {
        dataManager.updateInfo(INSTalent.GenderType.male, for: .gender)
        tableView.reloadData()
    }
    
    @IBAction func recordVideoAction(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        let videoDuration = sender.tag == 100 ? INSConstants.videoDemoReelDuration : INSConstants.videoPresentationDuration
        cameraManager.launchCamera(for: kUTTypeMovie, duration: videoDuration)
    }
    
    @IBAction func attachSecuritySocialDocumentAction(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        documentManager.openDocumentPickerForTypes([String(kUTTypePDF)])
    }
    
    @IBAction func attachJobPermissionDocumentAction(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        documentManager.openDocumentPickerForTypes([String(kUTTypePDF)])
    }
    
    @IBAction func attachSecuritySocialDocumentForeignPeopleAction(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        documentManager.openDocumentPickerForTypes([String(kUTTypePDF)])
    }
    
    @IBAction func attachJobPermissionDocumentForeignPeopleAction(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        documentManager.openDocumentPickerForTypes([String(kUTTypePDF)])
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        showActivityIndicator()
        dataManager.updateTalent()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CellIndices.allCases.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case CellIndices.titleCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath)
            return cell
        case CellIndices.genderCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
            if let femaleOption = cell.viewWithTag(100) as? UIButton, let maleOption = cell.viewWithTag(101) as? UIButton {
                femaleOption.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                maleOption.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                if dataManager.talentInfo.gender! == .male {
                    maleOption.isSelected = true
                    updateGenderSelectionButton(maleOption)
                    femaleOption.isSelected = false
                    updateGenderSelectionButton(femaleOption)
                } else {
                    femaleOption.isSelected = true
                    updateGenderSelectionButton(femaleOption)
                    maleOption.isSelected = false
                    updateGenderSelectionButton(maleOption)
                }
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.ageCell.rawValue...CellIndices.footwearCell.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as? INSSliderTableViewCell else { break }
            
            var info: INSSliderTableViewCell.SliderInfo?
            var currentValue: Any?
            
            switch CellIndices(rawValue: indexPath.row) {
            case .ageCell:
                info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .age)
                currentValue = dataManager.talentInfo.age
                if currentValue == nil {
                    dataManager.updateInfo(Int(info?.minimumValue ?? 18), for: .age)
                }
            case .heightCell:
                info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .height)
                currentValue = dataManager.talentInfo.height
                if currentValue == nil {
                    dataManager.updateInfo(info?.minimumValue ?? 1.60, for: .height)
                }
            case .upperBodyCell:
                if dataManager.talentInfo.gender! == .male {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .shirtSize)
                    currentValue = dataManager.maleTalent.shirt
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 0.0, for: .upperBodySize)
                    }
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .bustSize)
                    currentValue = dataManager.femaleTalent.bustSize
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 0.0, for: .upperBodySize)
                    }
                }
            case .middleBodyCell:
                if dataManager.talentInfo.gender! == .male {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .coatSize)
                    currentValue = dataManager.maleTalent.coat
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 0.0, for: .middleBodySize)
                    }
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .waistSize)
                    currentValue = dataManager.femaleTalent.waistSize
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 0.0, for: .middleBodySize)
                    }
                }
            case .lowBodyCell:
                if dataManager.talentInfo.gender! == .male {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .pantsSize)
                    currentValue = dataManager.maleTalent.pants
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 0.0, for: .lowerBodySize)
                    }
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .hipSize)
                    currentValue = dataManager.femaleTalent.hipSize
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 0.0, for: .lowerBodySize)
                    }
                }
            case .weightCell:
                info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .weight)
                currentValue = dataManager.talentInfo.weight
                if currentValue == nil {
                    dataManager.updateInfo(info?.minimumValue ?? 45, for: .weight)
                }
            case .footwearCell:
                if dataManager.talentInfo.gender! == .male {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .footwearMale)
                    currentValue = dataManager.maleTalent.footwear
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 25.0, for: .footwear)
                    }
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .footwearFemale)
                    currentValue = dataManager.femaleTalent.footwear
                    if currentValue == nil {
                        dataManager.updateInfo(info?.minimumValue ?? 22.0, for: .footwear)
                    }
                }
            default:
                break
            }
            
            if let _ = info {
                cell.setup(info!, currentValue: currentValue)
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.eyesNHairCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pickerCell", for: indexPath)
            if let picker1 = cell.viewWithTag(100) as? UIPickerView, let picker2 = cell.viewWithTag(101) as? UIPickerView {
                self.eyeColorPicker = picker1
                self.hairColorPicker = picker2
                picker1.reloadComponent(0)
                picker2.reloadComponent(0)
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.specialitiesCell.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as? INSCollectionTableViewCell else { break }
            cell.labeltitle.textAlignment = .center
            cell.labeltitle.text = "SPECIALITY_AREAS_SECTION".localized
            
            if dataManager.talentInfo.gender! == .male {
                let defaultSpecialities = dataManager.maleTalent.defaultSpecialitiesRawValues
                if let specialties = dataManager.maleTalent.specialityAreas {
                    let _specialities = specialties.map {
                        dataManager.maleTalent.specialityRawValueFor($0)
                    }
                    cell.setFilterCollection(for: defaultSpecialities, itemsXRow: 3, separation: 5, andCurrentInfo: _specialities)
                } else {
                    cell.setFilterCollection(for:defaultSpecialities, itemsXRow: 3, separation: 5)
                }
            } else {
                let defaultSpecialities = dataManager.femaleTalent.defaultSpecialitiesRawValues
                if let specialties = dataManager.femaleTalent.specialityAreas {
                    let _specialities = specialties.map {
                        dataManager.femaleTalent.specialityValueFor($0)
                    }
                    cell.setFilterCollection(for: defaultSpecialities, itemsXRow: 3, separation: 5, andCurrentInfo: _specialities)
                } else {
                    cell.setFilterCollection(for: defaultSpecialities, itemsXRow: 3, separation: 5)
                }
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.selfiesCell.rawValue...CellIndices.polaroidsCell.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as? INSCollectionTableViewCell else { break }
            cell.labeltitle.textAlignment = .center

            switch CellIndices(rawValue: indexPath.row) {
            case .selfiesCell:
                cell.labeltitle.text = "SELFIES_SECTION".localized
                var defaultData: [Int: (image: INSWebMultimedia?, description: String)] =
                    [0: (nil, "ROSTRO"), 1: (nil, "PERFIL"), 2: (nil, "CUERPO COMPLETO")]

                if let selfies = dataManager.talentInfo.selfies {
                    if let face = selfies.face {
                        defaultData[0]?.image = face
                    }
                    if let sideFace = selfies.sideFace {
                        defaultData[1]?.image = sideFace
                    }
                    if let wholeBody = selfies.wholeBody {
                        defaultData[2]?.image = wholeBody
                    }
                }
                cell.setPhotoCollection(type: .photoTypeNDescription, totalItems: 3, separation: 15, currentInfo: defaultData, estimatedItemHeight: 135, textColor: INSConstants.ThemeColor.darkTurquoise)
            case .photoBookCell:
                cell.labeltitle.text = "BOOK_SECTION".localized

                if let book = dataManager.talentInfo.book {
                    var photoBook: [Int: INSWebMultimedia] = [:]
                    for (index, photo) in book.enumerated() where photo != nil {
                        photoBook[index] = photo
                    }
                    cell.setPhotoCollection(type: .photoType, totalItems: 6, separation: 40, currentInfo: photoBook)
                }
                else {
                    cell.setPhotoCollection(type: .photoType, totalItems: 6, separation: 40)
                }
            case .polaroidsCell:
                cell.labeltitle.text = "POLAROIDS_SECTION".localized
                var defaultData: [Int: (image: INSWebMultimedia?, description: String)] =
                [0: (nil, "ROSTRO"),
                1: (nil, "FRENTE"),
                2: (nil, "ESPALDA"),
                3: (nil, "PERFIL IZQUIERDO"),
                4: (nil, "PERFIL DERECHO"),
                5: (nil, "3/4")]

                if let polaroids = dataManager.talentInfo.polaroids {
                    if let facePhoto = polaroids.face {
                        defaultData[0]?.image = facePhoto
                    }
                    if let frontPhoto = polaroids.front {
                        defaultData[1]?.image = frontPhoto
                    }
                    if let backPhoto = polaroids.back {
                        defaultData[2]?.image = backPhoto
                    }
                    if let leftSidePhoto = polaroids.leftSideFace {
                        defaultData[3]?.image = leftSidePhoto
                    }
                    if let rightSidePhoto = polaroids.rightSideFace {
                        defaultData[4]?.image = rightSidePhoto
                    }
                    if let treeQuartersPhoto = polaroids.threeQuarters {
                        defaultData[5]?.image = treeQuartersPhoto
                    }
                }
                cell.setPhotoCollection(type: .photoTypeNDescription, totalItems: 6, separation: 15, currentInfo: defaultData, estimatedItemHeight: 135)
            default:
                return cell
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.videoCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath)
            if let _videos = dataManager.talentInfo.videos,
                let demoReelButton = cell.viewWithTag(600) as? UIButton,
                let castingButton = cell.viewWithTag(601) as? UIButton {
                if let thumbnail = _videos.casting?.thumbnail {
                    castingButton.setImage(UIImage(data: thumbnail), for: .normal)
                }
                else if let webUrl = _videos.casting?.webUrl, let castingUrl = URL(string: webUrl) {
                    INSUtility.getThumbnailImage(from: castingUrl) { (image) in
                        if let thumbnailImage = image {
                            DispatchQueue.main.async {
                                self.setThumbnailForVideo(castingButton, url: castingUrl.absoluteString, thumbnail: thumbnailImage)
                            }
                        }
                    }
//                    dataManager.getDataFrom(castingUrl) { (success, image) in
//                        castingButton.backgroundColor = INSConstants.ThemeColor.darkTurquoise
//                    }
                }
                if let thumbnail = _videos.demoReel?.thumbnail {
                    demoReelButton.setImage(UIImage(data: thumbnail), for: .normal)
                }
                else if let webUrl = _videos.demoReel?.webUrl, let demoReelUrl = URL(string: webUrl) {
                    INSUtility.getThumbnailImage(from: demoReelUrl) { (image) in
                        if let thumbnailImage = image {
                            DispatchQueue.main.async {
                                self.setThumbnailForVideo(demoReelButton, url: demoReelUrl.absoluteString, thumbnail: thumbnailImage)
                            }
                        }
                    }
//                    dataManager.getDataFrom(demoReelUrl) { (success, image) in
//                        demoReelButton.backgroundColor = INSConstants.ThemeColor.darkTurquoise
//                    }
                }
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.filesCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "filesCell", for: indexPath)
            if let securitySocial = cell.viewWithTag(100) as? UIButton, let ine = cell.viewWithTag(101) as? UIButton {
                securitySocial.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                ine.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.foreignFilesCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "foreignFilesCell", for: indexPath)
            if let securitySocial = cell.viewWithTag(100) as? UIButton, let fm3 = cell.viewWithTag(101) as? UIButton {
                securitySocial.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                fm3.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            }
            verifyContentAvailability(for: cell)
            return cell
        case CellIndices.submitCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell", for: indexPath)
            if let saveButton = cell.viewWithTag(100) as? UIButton {
                saveButton.layoutIfNeeded()
                saveButton.layer.cornerRadius = saveButton.frame.size.height/2
            }
            verifyContentAvailability(for: cell)
            return cell
        default:
            break;
        }
        
        return UITableViewCell()
    }
    
    //MARK: Notification observers
    
    @objc func takePhotoAction(_ notification: Notification) {
        guard let buttonPressed = notification.object as? UIButton else { return }
        currentPhotoButtonPressed = buttonPressed
        takePhoto()
    }
    
    @objc func takeSelfiesAndPolaroids(_ notification: Notification) {
        guard let buttonPressed = notification.object as? UIButton else { return }
        currentPhotoButtonPressed = buttonPressed
        takePhoto()
    }
    
    @objc func selectSpecialityAction(_ notification: Notification) {
        guard let buttonPressed = notification.object as? UIButton,
            let optionSelected = buttonPressed.titleLabel?.text else {
                return
        }
        dataManager.updateInfo((buttonPressed.isSelected, optionSelected), for: .specialities)
    }
    
    @objc func didSliderValueChanged(_ notification: Notification) {
        guard let slider = notification.object as? UISlider else { return }
        let sliderPosition = slider.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at:sliderPosition) {
            switch CellIndices(rawValue: indexPath.row) {
            case .ageCell:
                dataManager.updateInfo(Int(slider.value), for: .age)
            case .heightCell:
                dataManager.updateInfo(slider.value, for: .height)
            case .upperBodyCell:
                dataManager.updateInfo(slider.value, for: .upperBodySize)
            case .middleBodyCell:
                dataManager.updateInfo(slider.value, for: .middleBodySize)
            case .lowBodyCell:
                dataManager.updateInfo(slider.value, for: .lowerBodySize)
            case .weightCell:
                dataManager.updateInfo(slider.value, for: .weight)
            case .footwearCell:
                dataManager.updateInfo(slider.value, for: .footwear)
            default:
                break
            }
        }
    }
}

extension INSUpdateTalentDataFormTableViewController: CameraManagementDelegate {
    
    func didPhotoTaken(image: UIImage) {
        if let buttonPosition = currentPhotoButtonPressed?.convert(CGPoint.zero, to: self.tableView),
            let indexPath = self.tableView.indexPathForRow(at:buttonPosition) {
            switch CellIndices(rawValue: indexPath.row) {
            case .selfiesCell:
                let selfieType = INSFormDataManager.SelfieType(rawValue: currentPhotoButtonPressed?.tag ?? -1)
                dataManager.updateInfo([selfieType: image], for: .selfies)
            case .photoBookCell:
                dataManager.updateInfo([currentPhotoButtonPressed?.tag: image], for: .photoBook)
            case .polaroidsCell:
                let polaroidType = INSFormDataManager.PolaroidType(rawValue: currentPhotoButtonPressed?.tag ?? -1)
                dataManager.updateInfo([polaroidType: image], for: .polaroids)
            default:
                break
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func didMovieRecorded(urlVideo: URL, thumbnailImage: UIImage) {
        setThumbnailForVideo(currentPhotoButtonPressed!, url: urlVideo, thumbnail: thumbnailImage)
    }
}

extension INSUpdateTalentDataFormTableViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == eyeColorPicker {
            return INSTalent.EyeColor.allCases.count
        } else {
            return INSTalent.HairColor.allCases.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == eyeColorPicker {
            dataManager.updateInfo(INSTalent.EyeColor.allCases[row], for: .eyeColor)
        } else {
            dataManager.updateInfo(INSTalent.HairColor.allCases[row], for: .hairColor)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel = view as? UILabel
        if pickerLabel == nil {
            pickerLabel = UILabel()
        }
        var text = ""
        if pickerView == eyeColorPicker {
            text = INSTalent.EyeColor.allCases[row].rawValue
            if dataManager.talentInfo.eyeColor == nil {
                dataManager.updateInfo(INSTalent.EyeColor.allCases[row], for: .eyeColor)
            }
        } else {
            text = INSTalent.HairColor.allCases[row].rawValue
            if dataManager.talentInfo.hairColor == nil {
                dataManager.updateInfo(INSTalent.HairColor.allCases[row], for: .hairColor)
            }
        }
        pickerLabel!.text = text
        pickerLabel!.textAlignment = .center
        pickerLabel!.font = UIFont(name: INSConstants.ThemeFontNames.AvenirMedium, size: 15.0)
        pickerLabel!.textColor = INSConstants.ThemeColor.darkGray
        return pickerLabel!
    }
}

extension INSUpdateTalentDataFormTableViewController: DocumentManagerDelegate {
    func didCancelDocumentSelection() {
        print("Document selection canceled")
    }
    
    func didSelectedDocument(filePath: URL) {
        let buttonPosition = currentPhotoButtonPressed!.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at:buttonPosition),
            let cell = tableView.cellForRow(at: indexPath) {
            var docType: INSFormDataManager.DocumentType?
            if cell.reuseIdentifier == "foreignFilesCell" {
                docType = currentPhotoButtonPressed?.tag == 100 ? .foreignSocialSecurity : .jobPermisson
            }
            else if cell.reuseIdentifier == "filesCell" {
                docType = currentPhotoButtonPressed?.tag == 100 ? .localSocialSecurity : .ine
            }
            dataManager.updateInfo([docType: filePath], for: .videos)
        }
    }
}

extension INSUpdateTalentDataFormTableViewController: FormDataManagerDelegate {
    func didSuccessForOperaion(_ operation: WsOperation, response: Any?) {
        dismissActivityIndicator()
        if operation == .getTalentData {
            tableView.reloadData()
        }
        else {
            let message = response as? String ?? "DEFAULT_WEB_SUCCESS_MEESAGE".localized
            showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: message, actions: [UIAlertAction(title: "DEFAULT_ALERT_ACTION".localized, style: .default, handler: { (action) in
                self.performSegue(withIdentifier: "showDashboardSegue", sender: self)
            })])
        }
    }
    
    func didFailForOperation(_ operation: WsOperation, message: String?) {
        dismissActivityIndicator()
        showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized,
                                message: message ?? "DEFAULT_WEB_ERROR_MEESAGE".localized)
    }
}
