//
//  INSFormDataManager.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 02/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

protocol FormDataManagerDelegate: NSObject {
    func didSuccessForOperaion(_ operation: WsOperation, response: Any?)
    func didFailForOperation(_ operation: WsOperation, message: String?)
}

enum WsOperation: Int {
    case talentSignUp, clientSignUp, regionCodes, talentUpdate, uploadTalentPhotos, getTalentData
}

class INSFormDataManager {
    enum InfoType: Int {
        case gender, age, height, upperBodySize, middleBodySize, lowerBodySize, weight, footwear, eyeColor, hairColor, specialities, selfies, photoBook, polaroids, videos, documents
    }
    
    enum SelfieType: Int {
        case faceType, sideFaceType, wholeBodyType
    }
    
    enum PolaroidType: Int {
        case faceType, frontType, backType, leftSideFaceType, rightSideFaceType, threeQuartersType
    }
    
    enum VideoType: Int {
        case demoReel, casting
    }
    
    enum DocumentType: Int {
        case localSocialSecurity, ine, foreignSocialSecurity, jobPermisson
    }
    
    var talentInfo: INSTalent
    var session = INSDataController.sharedDataController
    var maleTalent = INSMaleTalent()
    var femaleTalent = INSFemaleTalent()
    weak var delegate: FormDataManagerDelegate?
    typealias RegionCodeInfo = (flag: UIImage, countryName: String, countryCode: String)
    fileprivate typealias VideoInfoFormat = [VideoType: (url: Any?, thumbnail: UIImage)]
    
    public init() {
        if let talent = session.user as? INSTalent, let gender = talent.gender {
            if gender == .male {
                maleTalent.setMaleTalent(from: talent)
                talentInfo = maleTalent
            } else {
                femaleTalent.setFemaleTalent(from: talent)
                talentInfo = femaleTalent
            }
        } else {
            if let profile = session.user.profile, profile == .talent {
                maleTalent.idUser = session.user.idUser
            }
            talentInfo = maleTalent
        }
    }
    
    internal func updateInfo(_ info: Any, for type: InfoType) {
        switch type {
        case .gender:
            switch info as? INSTalent.GenderType {
            case .male:
                maleTalent.gender = .male
                talentInfo = maleTalent
            case .female:
                femaleTalent.gender = .female
                talentInfo = femaleTalent
            default:
                break
            }
        case .age:
            talentInfo.age = info as? Int
            updateGenderFromTalent(for: .age)
        case .height:
            talentInfo.height = info as? Float
            updateGenderFromTalent(for: .height)
        case .upperBodySize:
            if talentInfo.gender == INSTalent.GenderType.male {
                maleTalent.shirt = info as? Float
            } else {
                femaleTalent.bustSize = info as? Float
            }
            updateTalentFromGender()
        case .middleBodySize:
            if talentInfo.gender == INSTalent.GenderType.male {
                maleTalent.coat = info as? Float
            } else {
                femaleTalent.waistSize = info as? Float
            }
            updateTalentFromGender()
        case .lowerBodySize:
            if talentInfo.gender == INSTalent.GenderType.male {
                maleTalent.pants = info as? Float
            } else {
                femaleTalent.hipSize = info as? Float
            }
            updateTalentFromGender()
        case .weight:
            talentInfo.weight = info as? Float
            updateGenderFromTalent(for: .weight)
        case .footwear:
            if talentInfo.gender == INSTalent.GenderType.male {
                maleTalent.footwear = info as? Float
            } else {
                femaleTalent.footwear = info as? Float
            }
            updateTalentFromGender()
        case .eyeColor:
            talentInfo.eyeColor = info as? INSTalent.EyeColor
            updateGenderFromTalent(for: .eyeColor)
        case .hairColor:
            talentInfo.hairColor = info as? INSTalent.HairColor
            updateGenderFromTalent(for: .hairColor)
        case .specialities:
            let data = info as? (selected: Bool, optionSelected: String)
            if let option = data?.optionSelected, let isSelected = data?.selected {
                let speciality: INSTalent.Specialities?
                if talentInfo.gender == INSTalent.GenderType.male {
                    speciality = maleTalent.specialityFor(option)
                } else {
                    speciality = femaleTalent.specialityFor(option)
                }
                if talentInfo.specialityAreas == nil, let _ = speciality {
                    talentInfo.specialityAreas = [speciality!]
                } else {
                    if isSelected, let _ = speciality {
                        talentInfo.specialityAreas?.append(speciality!)
                    } else if !isSelected, let _ = talentInfo.specialityAreas?.contains(speciality!) {
                        talentInfo.specialityAreas?.removeAll(where: { $0 == speciality! })
                    }
                }
            }
            updateGenderFromTalent(for: .specialities)
        case .selfies:
            guard let photoInfo = info as? [SelfieType: UIImage],
                let photoIndex = photoInfo.keys.first,
                let image = photoInfo[photoIndex] else {
                    break
            }
            
            if talentInfo.selfies == nil {
                talentInfo.selfies = INSTalent.Selfies(face: nil, sideFace: nil, wholeBody: nil)
            }
                                    
            let imageData = image.jpegData(compressionQuality: INSConstants.noCompression)
            switch photoIndex {
            case .faceType:
                talentInfo.selfies?.face = INSWebMultimedia(imageData, takenFromCamera: true)
            case .sideFaceType:
                talentInfo.selfies?.sideFace = INSWebMultimedia(imageData, takenFromCamera: true)
            case .wholeBodyType:
                talentInfo.selfies?.wholeBody = INSWebMultimedia(imageData, takenFromCamera: true)
            }
            updateGenderFromTalent(for: .selfies)
        case .photoBook:
            guard let photoInfo = info as? [Int: UIImage],
                let photoIndex = photoInfo.keys.first,
                let image = photoInfo[photoIndex] else {
                    break
            }
            
            if talentInfo.book == nil {
                talentInfo.book = Array(repeating: nil, count: 6)
            }
            
            talentInfo.book?[photoIndex] = INSWebMultimedia(image.jpegData(compressionQuality: INSConstants.noCompression), takenFromCamera: true)
            updateGenderFromTalent(for: .photoBook)
        case .polaroids:
            guard let photoInfo = info as? [PolaroidType: UIImage],
                let photoIndex = photoInfo.keys.first,
                let image = photoInfo[photoIndex] else {
                    break
            }

            if talentInfo.polaroids == nil {
                talentInfo.polaroids = INSTalent.Polaroids()
            }
            
            let imageData = image.jpegData(compressionQuality: INSConstants.noCompression)
            
            switch photoIndex {
            case .faceType:
                talentInfo.polaroids?.face = INSWebMultimedia(imageData, takenFromCamera: true)
            case .frontType:
                talentInfo.polaroids?.front = INSWebMultimedia(imageData, takenFromCamera: true)
            case .backType:
                talentInfo.polaroids?.back = INSWebMultimedia(imageData, takenFromCamera: true)
            case .leftSideFaceType:
                talentInfo.polaroids?.leftSideFace = INSWebMultimedia(imageData, takenFromCamera: true)
            case .rightSideFaceType:
                talentInfo.polaroids?.rightSideFace = INSWebMultimedia(imageData, takenFromCamera: true)
            case .threeQuartersType:
                talentInfo.polaroids?.threeQuarters = INSWebMultimedia(imageData, takenFromCamera: true)
            }
            updateGenderFromTalent(for: .polaroids)
        case .videos:
            guard let data = info as? VideoInfoFormat,
                let videoType = data.keys.first,
                let videoData = data[videoType] else {
                break
            }
            if talentInfo.videos == nil {
                talentInfo.videos = INSTalent.SelfVideos()
            }
            
            switch videoType {
            case .demoReel:
                var demoReel: INSWebMultimedia?
                if let filePathUrl = videoData.url as? URL {
                    demoReel = INSWebMultimedia(filePathUrl: filePathUrl.absoluteString, thumbnail: videoData.thumbnail.jpegData(compressionQuality: INSConstants.noCompression)!, mediaType: .video)
                }
                else if let webUrl = videoData.url as? String {
                    demoReel = INSWebMultimedia(webUrl: webUrl, thumbnail: videoData.thumbnail.jpegData(compressionQuality: INSConstants.noCompression)!, mediaType: .video)
                }
                talentInfo.videos?.demoReel = demoReel
            case .casting:
                var casting: INSWebMultimedia?
                if let filePathUrl = videoData.url as? URL {
                    casting = INSWebMultimedia(filePathUrl: filePathUrl.absoluteString, thumbnail: videoData.thumbnail.jpegData(compressionQuality: INSConstants.noCompression)!, mediaType: .video)
                }
                else if let webUrl = videoData.url as? String {
                    casting = INSWebMultimedia(webUrl: webUrl, thumbnail: videoData.thumbnail.jpegData(compressionQuality: INSConstants.noCompression)!, mediaType: .video)
                }
                talentInfo.videos?.casting = casting
            }
            updateGenderFromTalent(for: .videos)
        case .documents:
            guard let documentInfo = info as? [DocumentType: URL],
                let type = documentInfo.keys.first,
                let filePathUrl = documentInfo.values.first else {
                break
            }
            
            let documentation = INSPersonalDocument(document: nil, filePathUrl: filePathUrl.absoluteString, webUrl: nil)
            if talentInfo.foreign {
                if type == .foreignSocialSecurity {
                    talentInfo.foreignDocuments?.socialSecurity = documentation
                } else if type == .jobPermisson {
                    talentInfo.foreignDocuments?.jobPermission = documentation
                }
            }
            else {
                if type == .localSocialSecurity {
                    talentInfo.localDocuments?.socialSecurity = documentation
                } else if type == .ine {
                    talentInfo.localDocuments?.INE = documentation
                }
            }
            updateGenderFromTalent(for: .documents)
        }
    }
    
    fileprivate func updateTalentFromGender() {
        if let _ = talentInfo as? INSMaleTalent {
            talentInfo = maleTalent
        } else {
            talentInfo = femaleTalent
        }
    }
    
    fileprivate func updateGenderFromTalent(for type: InfoType) {
        switch type {
        case .age:
            femaleTalent.age = talentInfo.age
            maleTalent.age = talentInfo.age
        case .height:
            femaleTalent.height = talentInfo.height
            maleTalent.height = talentInfo.height
        case .weight:
            femaleTalent.weight = talentInfo.weight
            maleTalent.weight = talentInfo.weight
        case .eyeColor:
            femaleTalent.eyeColor = talentInfo.eyeColor
            maleTalent.eyeColor = talentInfo.eyeColor
        case .hairColor:
            femaleTalent.hairColor = talentInfo.hairColor
            maleTalent.hairColor = talentInfo.hairColor
        case .specialities:
            femaleTalent.specialityAreas = talentInfo.specialityAreas
            maleTalent.specialityAreas = talentInfo.specialityAreas
        case .selfies:
            femaleTalent.selfies = talentInfo.selfies
            maleTalent.selfies = talentInfo.selfies
        case .photoBook:
            femaleTalent.book = talentInfo.book
            maleTalent.book = talentInfo.book
        case .polaroids:
            femaleTalent.polaroids = talentInfo.polaroids
            maleTalent.polaroids = talentInfo.polaroids
        case .videos:
            femaleTalent.videos = talentInfo.videos
            maleTalent.videos = talentInfo.videos
        case .documents:
            femaleTalent.localDocuments = talentInfo.localDocuments
            femaleTalent.foreignDocuments = talentInfo.foreignDocuments
            maleTalent.localDocuments = talentInfo.localDocuments
            maleTalent.foreignDocuments = talentInfo.foreignDocuments
        default:
            break
        }
    }
    
    fileprivate func generateDataForTalentRegistration(_ talent: INSTalent, password: String?) -> [String: Any] {
        guard let name = talent.name, let surnames = talent.surnames, let email = talent.email, let pass = password, let birthDate = talent.birthDate, let gender = talent.gender, let instagramProfile = talent.instagramProfile else {
            return [:]
        }
        
        return [ "tipo_solicitud": "2",
                 "nombres": name,
                 "apellidos": surnames,
                 "email": email,
                 "password": pass,
                 "fecha_nacimiento": birthDate,
                 "genero": gender == .male ? "MALE_OPTION".localized : "FEMALE_OPTION".localized,
                 "url_instagram": instagramProfile
        ]
    }
    
    fileprivate func generateDataForPartialTalentRegistration(_ talent: INSTalent, password: String?) -> [String: Any] {
        guard let name = talent.name, let surnames = talent.surnames, let email = talent.email, let birthDate = talent.birthDate, let gender = talent.gender else {
            return [:]
        }
        
        var params = [ "tipo_solicitud": "2",
                 "nombres": name,
                 "apellidos": surnames,
                 "email": email,
                 "fecha_nacimiento": birthDate,
                 "genero": gender == .male ? "MALE_OPTION".localized : "FEMALE_OPTION".localized
        ]
        
        if let _password = password  {
            params["password"] = _password
        }
        
        return params
    }
        
    fileprivate func generateDataForClientRegistration(_ client: INSCustomer, password: String?) -> [String: Any] {
        guard let name = client.name, let company = client.company, let address = client.companyAddress, let email = client.email, let pass = password, let jobPosition = client.jobPosition, let phone  = client.telephone, let linkedIn = client.linkedInUrl, let website = client.websiteUrl, let instagram = client.instagramUrl else {
            return [:]
        }
        
        let fullName = name.components(separatedBy: " ")
        if fullName.count == 2 {
            client.surnames = fullName[1]
        } else if fullName.count == 3 {
            client.surnames = "\(fullName[1]) \(fullName[2])"
        } else if fullName.count == 4 {
            client.surnames = "\(fullName[2]) \(fullName[3])"
        }
        
        return [ "tipo_solicitud": "1",
                 "nombres": name,
                 "apellidos": client.surnames ?? "",
                 "empresa": company,
                 "direccion": address,
                 "email": email,
                 "password": pass,
                 "cargo": jobPosition,
                 "telefono": phone,
                 "url_linked": linkedIn,
                 "url_website": website,
                 "url_instagram": instagram
        ]
    }

    fileprivate func generateDataForTalentUpdate() -> [String: Any] {
        var data: [String: Any] = [:]
        if let age = talentInfo.age {
            data["edad"] = age
        }
        if let gender = talentInfo.gender {
            data["genero"] = gender == .male ? "Masculino" : "Femenino"
        }
        if let height = talentInfo.height {
            data["altura_mts"] = Double(height).rounded(toPlaces: 2)
        }
        if let weight = talentInfo.weight {
            data["peso_kg"] = Double(weight).rounded(toPlaces: 1)
        }
        if let footwear = talentInfo.footwear {
            data["calzado"] = Double(footwear).rounded(toPlaces: 1)
        }
        if let eyes = talentInfo.eyeColor {
            data["ojos"] = eyes.rawValue
        }
        if let hair = talentInfo.hairColor {
            data["cabello"] = hair.rawValue
        }
        if let maleTalent = talentInfo as? INSMaleTalent {
            if let coat = maleTalent.coat {
                data["h_saco"] = Double(coat).rounded(toPlaces: 2)
            }
            if let shirt = maleTalent.shirt {
                data["h_camisa"] = Double(shirt).rounded(toPlaces: 2)
            }
            if let pants = maleTalent.pants {
                data["h_pantalon"] = Double(pants).rounded(toPlaces: 2)
            }
            data["m_pecho"] = 0.0
            data["m_cintura"] = 0.0
            data["m_cadera"] = 0.0
        }
        if let femaleTalent = talentInfo as? INSFemaleTalent {
            if let bust = femaleTalent.bustSize {
                data["m_pecho"] = Double(bust).rounded(toPlaces: 2)
            }
            if let waist = femaleTalent.waistSize {
                data["m_cintura"] = Double(waist).rounded(toPlaces: 2)
            }
            if let hip = femaleTalent.hipSize {
                data["m_cadera"] = Double(hip).rounded(toPlaces: 2)
            }
            
            data["h_pantalon"] = 0.0
            data["h_camisa"] = 0.0
            data["h_saco"] = 0.0
        }
        
        data["modelo"] = talentInfo.specialityAreas?.contains(.model) ?? false
        data["edecan"] = talentInfo.specialityAreas?.contains(.GIO) ?? false
        data["actriz"] = talentInfo.specialityAreas?.contains(.actor) ?? false
        data["imagen"] = talentInfo.specialityAreas?.contains(.image) ?? false
        data["promotor"] = talentInfo.specialityAreas?.contains(.promoter) ?? false
        data["animador"] = talentInfo.specialityAreas?.contains(.animator) ?? false
        data["conductor"] = talentInfo.specialityAreas?.contains(.showDriver) ?? false
        data["coordinador"] = talentInfo.specialityAreas?.contains(.coordinator) ?? false
        data["bailarin"] = talentInfo.specialityAreas?.contains(.dancer) ?? false
        data["influencer"] = talentInfo.specialityAreas?.contains(.influencer) ?? false
        data["nacionalidad"] = 1
        return data
    }
    
    fileprivate func generateDataForTalentCompletion() -> [String: Any] {
        var data: [String: Any] = [:]
        
        if let idUser = talentInfo.idUser {
            data["id_usuario"] = idUser
        }
        // selfies
        if let faceSelfie = talentInfo.selfies?.face {
            data["selfie_rostro"] = faceSelfie
        }
        if let wholeBodySelfie = talentInfo.selfies?.wholeBody {
            data["selfie_ccompleto"] = wholeBodySelfie
        }
        if let sideFaceSelfie = talentInfo.selfies?.sideFace {
            data["selfie_perfill"] = sideFaceSelfie
        }
        // books
        if let books = talentInfo.book {
            for (index, book) in books.enumerated() {
                data["book_\(index + 1)"] = book
            }
        }
        // polaroids
        if let facePolaroid = talentInfo.polaroids?.face {
            data["pola_rostro"] = facePolaroid
        }
        if let frontPolaroid = talentInfo.polaroids?.front {
            data["pola_frente"] = frontPolaroid
        }
        if let backPolaroid = talentInfo.polaroids?.back {
            data["pola_espalda"] = backPolaroid
        }
        if let leftSidePolaroid = talentInfo.polaroids?.leftSideFace {
            data["pola_izquierda"] = leftSidePolaroid
        }
        if let rightSidePolaroid = talentInfo.polaroids?.rightSideFace {
            data["pola_derecha"] = rightSidePolaroid
        }
        if let threeQuartersPolaroid = talentInfo.polaroids?.threeQuarters {
            data["pola_34"] = threeQuartersPolaroid
        }
        // videos
        if let videoDemo = talentInfo.videos?.demoReel {
            data["video_demo"] = videoDemo
        }
        if let videoCasting = talentInfo.videos?.casting {
            data["vide_casting"] = videoCasting
        }
        // documents
        if talentInfo.foreign {
            if let socialSecurityFilePath = talentInfo.foreignDocuments?.socialSecurity?.filePathUrl {
                data["doc_seguro"] = socialSecurityFilePath
            }
            else if let workPermissonFilePath = talentInfo.foreignDocuments?.jobPermission?.filePathUrl {
                data["doc_permisoine"] = workPermissonFilePath
            }
        }
        else {
           if let socialSecurityFilePath = talentInfo.localDocuments?.socialSecurity?.filePathUrl {
                data["doc_seguro"] = socialSecurityFilePath
            }
            else if let workPermissonFilePath = talentInfo.localDocuments?.INE?.filePathUrl {
                data["doc_permisoine"] = workPermissonFilePath
            }
        }
       
        return data
    }

    internal func completeTalentData() {
        let data = generateDataForTalentCompletion()
        if !data.isEmpty {
            let requestDisptacher = INSRequestDispatcher()
            requestDisptacher.dispatchRequest(opCode: .INS_COMPLETE_TALENT_INFO_CODE, requestParameters: data) { (success, jsonResponse, urlResponse, error) in
                self.delegate?.didSuccessForOperaion(.talentUpdate, response: nil)
//                if success {
//
//                }
//                else {
//
//                }
            }
        }
        else {
            delegate?.didSuccessForOperaion(.talentUpdate, response: nil)
        }
    }
        
    fileprivate func generateUploadPhotosPayload(selfie: UIImage, photoBook: [Int: UIImage], for idRequest: String) -> [String: Any] {
        var data: [String: Any] = [:]
        data["id_solicitud"] = idRequest
        data["perfil"] = selfie.jpegData(compressionQuality: 0.95)
        data["book_1"] = photoBook[0]!.jpegData(compressionQuality: 0.95)
        data["book_2"] = photoBook[1]?.jpegData(compressionQuality: 0.95)
        data["book_3"] = photoBook[2]?.jpegData(compressionQuality: 0.95)
        data["book_4"] = photoBook[3]?.jpegData(compressionQuality: 0.95)
        data["book_5"] = photoBook[4]?.jpegData(compressionQuality: 0.95)
        data["book_6"] = photoBook[5]?.jpegData(compressionQuality: 0.95)
        return data
    }
    
    //MARK: - Web consumption
    internal func signupTalent(_ talent: INSTalent, password: String?) {
        let data = generateDataForTalentRegistration(talent, password: password)
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_REGISTER_TALENT_OPCODE, requestParameters: data, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = jsonResponse["codigo"] as? Int, (code == 206 || code == 201) {
                _success = true
            }
            
            DispatchQueue.main.async {
                if _success {
                    if let object = jsonResponse["objeto"] as? [String: Int] {
                        self.delegate?.didSuccessForOperaion(.talentSignUp, response: object)
                    } else if let message = jsonResponse["mensaje"] as? String {
                        self.delegate?.didSuccessForOperaion(.talentSignUp, response: message)
                    }
                } else {
                    self.delegate?.didFailForOperation(.talentSignUp, message: jsonResponse["mensaje"] as? String)
                }
            }
        })
    }
    
    internal func signupClient(_ client: INSCustomer, password: String?) {
        let data = generateDataForClientRegistration(client, password: password)
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_REGISTER_CUSTOMER_OPCODE, requestParameters: data, completionHandler: { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                if success {
                    self.delegate?.didSuccessForOperaion(.clientSignUp, response: jsonResponse["mensaje"] as? String)
                } else {
                    self.delegate?.didFailForOperation(.clientSignUp, message: jsonResponse["mensaje"] as? String)
                }
            }
        })
    }
    
    internal func updateTalent() {
        let data = generateDataForTalentUpdate()
        if !data.isEmpty {
            let requestDispatcher = INSRequestDispatcher()
            requestDispatcher.dispatchRequest(opCode: .INS_UPDATE_TALENT_DATA_OPCODE, requestParameters: data, completionHandler: { (success, jsonResponse, response, error) in
                DispatchQueue.main.async {
                    if success {
                        self.completeTalentData()
                        //                        self.delegate?.didSuccessForOperaion(.talentUpdate, response: jsonResponse["mensaje"] as? String)
                    } else {
                        self.delegate?.didFailForOperation(.talentUpdate, message: jsonResponse["mensaje"] as? String)
                    }
                }
            })
        }
    }
    
    internal func getRegionCodes() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_REGION_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                if success {
                    guard let object = jsonResponse["objeto"], let codes = object as? [[String: String]] else {
                        self.delegate?.didFailForOperation(.regionCodes, message: "DEFAULT_WEB_ERROR_MEESAGE".localized)
                        return
                    }
                    
                    var regionCodes: [RegionCodeInfo] = []
                    for code in codes {
                        if let countryName = code["nombre"],
                            let countryCode = code["prefijo"],
                            let countryFlag = code["bandera"] {
                            requestDispatcher.downloadData(url: countryFlag) { (success, data) in
                                if success, let _data = data as? Data, let image = UIImage(data: _data) {
                                    regionCodes.append((image, countryName, countryCode))
                                }
                                if regionCodes.count == codes.count {
                                    // At this point all resources were obtained, so return the info
                                    self.delegate?.didSuccessForOperaion(.regionCodes, response: regionCodes)
                                }
                            }
                        }
                    }
                } else {
                    self.delegate?.didFailForOperation(.regionCodes, message: jsonResponse["mensaje"] as? String)
                }
            }
        })
    }
    
    internal func uploadPhotos(selfie: UIImage, photoBook: [Int: UIImage], for idRequest: String) {
        let data = generateUploadPhotosPayload(selfie: selfie, photoBook: photoBook, for: idRequest)
        if !data.isEmpty {
            let requestDispatcher = INSRequestDispatcher()
            requestDispatcher.dispatchRequest(opCode: .INS_UPLOAD_PHOTOS_CODE, requestParameters: data, completionHandler: { (success, jsonResponse, response, error) in
                DispatchQueue.main.async {
                    if success {
                        self.delegate?.didSuccessForOperaion(.uploadTalentPhotos, response: jsonResponse["mensaje"])
                    } else {
                        self.delegate?.didFailForOperation(.uploadTalentPhotos, message: error?.errorMessage)
                    }
                }
            })
        }
    }
    
    internal func getTalentDetailInfo(_ talent: INSTalent) {
        guard let idUser = talent.idUser else {
            print("There's no id user for talent")
            return
        }
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_TALENT_DETAIL_CODE, requestParameters: nil, queryParameters: [Keys.user_id: idUser]) { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                if success {
                    if let object = jsonResponse["objeto"] as? [String: Any] {
                        let _talent = talent.setTalentProfile(from: talent, and: object)
                        if let gender = _talent.gender {
                            if gender == .male {
                                self.maleTalent.setMaleTalent(from: _talent)
                                self.talentInfo = self.maleTalent
                            } else {
                                self.femaleTalent.setFemaleTalent(from: talent)
                                self.talentInfo = self.femaleTalent
                            }
                        }
                        self.delegate?.didSuccessForOperaion(.getTalentData, response: nil)
                    }
                    else {
                        self.delegate?.didFailForOperation(.getTalentData, message: nil)
                    }
                } else {
                    self.delegate?.didFailForOperation(.getTalentData, message: error?.errorMessage)
                }
            }
        }
    }
    
    internal func getDataFrom(_ url: String, completion: @escaping(_ : Bool, _ : Any?) -> Void) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.downloadData(url: url) { (success, data) in
            completion(success, data)
        }
    }

    internal func signupTalent(_ talent: INSTalent, password:String?, completion: @escaping (_ success : Bool, _ jsonResponse:[String:Any]?, _ error: INSError?) -> Void) {
        let data = generateDataForPartialTalentRegistration(talent, password: password)
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_ADD_TALENT_CODE, requestParameters: data, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = jsonResponse["codigo"] as? Int, code == 200 {
                _success = true
            }
            
            DispatchQueue.main.async {
                if _success {
                    completion(true, jsonResponse, nil)
                } else {
                    completion(false, jsonResponse, nil)
                }
            }
        })
    }

}
