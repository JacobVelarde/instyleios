//
//  INSUpdateTalentDataFormTableViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import MobileCoreServices

class INSUpdateTalentDataFormTableViewController: UITableViewController {
    fileprivate weak var currentPhotoButtonPressed: UIButton?
    fileprivate var dataManager = INSTalentDataManager()
    fileprivate let cameraManager = INSCameraManager()
    fileprivate var eyeColorPicker = UIPickerView()
    fileprivate var hairColorPicker = UIPickerView()

    fileprivate enum CellIndices: Int, CaseIterable {
        case titleCell = 0
        case genderCell = 1
        case ageCell = 2
        case heightCell = 3
        case upperBodyCell = 4
        case middleBodyCell = 5
        case lowBodyCell = 6
        case weightCell = 7
        case footwearCell = 8
        case eyesNHairCell = 9
        case specialitiesCell = 10
        case selfiesCell = 11
        case photoBookCell = 12
        case polaroidsCell = 13
        case videoCell = 14
        case filesCell = 15
        case foreignFilesCell = 16
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addObservers()
        registerCells()
        cameraManager.delegate = self
    }
    
    fileprivate func registerCells() {
        tableView.register(UINib(nibName: "INSSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "sliderCell")
        tableView.register(UINib(nibName: "INSCollectionTableViewCell", bundle: nil), forCellReuseIdentifier: "collectionCell")
    }
    
    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(takePhotoAction(_:)), name: INSItemCollectionViewCell.itemCollectionSelectedNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectSpecialityAction(_:)), name: INSFilterCollectionViewCell.filterCollectionNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didSliderValueChanged(_:)), name: INSSliderTableViewCell.sliderChangeNotificationName, object: nil)
    }
    
    fileprivate func takePhoto() {
        cameraManager.launchCamera(for: kUTTypeImage)
    }
    
    fileprivate func updateGenderSelectionButton(_ button: UIButton) {
        if button.isSelected {
            button.titleLabel?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            button.backgroundColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
        } else {
            button.titleLabel?.textColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    //MARK: IBActions
    @IBAction func femaleSelectionAction(_ sender: UIButton) {
        dataManager.updateInfo(INSTalentDataManager.GenderType.female, for: .gender)
        sender.isSelected = true
        updateGenderSelectionButton(sender)
        let index = IndexPath(row: CellIndices.genderCell.rawValue, section: 0)
        if let cell = tableView.cellForRow(at: index), let maleButton = cell.viewWithTag(101) as? UIButton {
            maleButton.isSelected = false
            updateGenderSelectionButton(maleButton)
        }
        tableView.reloadData()
    }
    
    @IBAction func maleSelectionAction(_ sender: UIButton) {
        dataManager.updateInfo(INSTalentDataManager.GenderType.male, for: .gender)
        sender.isSelected = true
        updateGenderSelectionButton(sender)
        let index = IndexPath(row: CellIndices.genderCell.rawValue, section: 0)
        if let cell = tableView.cellForRow(at: index), let femaleButton = cell.viewWithTag(100) as? UIButton {
            femaleButton.isSelected = false
            updateGenderSelectionButton(femaleButton)
        }
        tableView.reloadData()
    }
    
    @IBAction func recordVideoAction(_ sender: UIButton) {
        currentPhotoButtonPressed = sender
        cameraManager.launchCamera(for: kUTTypeMovie)
    }
    
    @IBAction func attachSecuritySocialDocumentAction(_ sender: UIButton) {
    }
    
    @IBAction func attachJobPermissionDocumentAction(_ sender: UIButton) {
    }
    
    @IBAction func attachSecuritySocialDocumentForeignPeopleAction(_ sender: UIButton) {
    }
    
    @IBAction func attachJobPermissionDocumentForeignPeopleAction(_ sender: UIButton) {
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CellIndices.allCases.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case CellIndices.titleCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath)
            return cell
        case CellIndices.genderCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
            if let femaleOption = cell.viewWithTag(100) as? UIButton, let maleOption = cell.viewWithTag(101) as? UIButton {
                femaleOption.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                maleOption.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            }
            return cell
        case CellIndices.ageCell.rawValue...CellIndices.footwearCell.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as? INSSliderTableViewCell else { break }
            
            var info: INSSliderTableViewCell.SliderInfo?
            var currentValue: Any?
            
            switch CellIndices(rawValue: indexPath.row) {
            case .ageCell:
                info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .age)
                currentValue = dataManager.talentInfo.age
            case .heightCell:
                info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .height)
                currentValue = dataManager.talentInfo.height
            case .upperBodyCell:
                if dataManager.isMale() {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .shirtSize)
                    currentValue = dataManager.maleTalent.shirt
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .bustSize)
                    currentValue = dataManager.femaleTalent.bustSize
                }
            case .middleBodyCell:
                if dataManager.isMale() {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .coatSize)
                    currentValue = dataManager.maleTalent.coat
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .waistSize)
                    currentValue = dataManager.femaleTalent.waistSize
                }
            case .lowBodyCell:
                if dataManager.isMale() {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .pantsSize)
                    currentValue = dataManager.maleTalent.pants
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .hipSize)
                    currentValue = dataManager.femaleTalent.hipSize
                }
            case .weightCell:
                info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .weight)
                currentValue = dataManager.talentInfo.weight
            case .footwearCell:
                if dataManager.isMale() {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .footwearMale)
                    currentValue = dataManager.maleTalent.footwear
                } else {
                    info = INSSliderTableViewCell.SliderInfo(sliderTemplate: .footwearFemale)
                    currentValue = dataManager.femaleTalent.footwear
                }
            default:
                break
            }
            
            if let _ = info {
                cell.setup(info!, currentValue: currentValue)
            }
            return cell
        case CellIndices.eyesNHairCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pickerCell", for: indexPath)
            if let picker1 = cell.viewWithTag(100) as? UIPickerView, let picker2 = cell.viewWithTag(101) as? UIPickerView {
                self.eyeColorPicker = picker1
                self.hairColorPicker = picker2
                picker1.reloadComponent(0)
                picker2.reloadComponent(0)
            }
            return cell
        case CellIndices.specialitiesCell.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as? INSCollectionTableViewCell else { break }
            cell.labeltitle.textAlignment = .center
            cell.labeltitle.text = "ÁREAS DE ESPECIALIDAD"
            
            if dataManager.isMale() {
                if let specialties = dataManager.maleTalent.specialityAreas {
                    cell.setFilterCollection(for: dataManager.maleTalent.specialitiesRawValues, itemsXRow: 3, separation: 5, andCurrentInfo: specialties.map { $0.rawValue })
                } else {
                    cell.setFilterCollection(for: dataManager.maleTalent.specialitiesRawValues, itemsXRow: 3, separation: 5)
                }
            } else {
                if let specialties = dataManager.femaleTalent.specialityAreas {
                    cell.setFilterCollection(for: dataManager.femaleTalent.specialitiesRawValues, itemsXRow: 3, separation: 5, andCurrentInfo: specialties.map { $0.rawValue })
                } else {
                    cell.setFilterCollection(for: dataManager.femaleTalent.specialitiesRawValues, itemsXRow: 3, separation: 5)
                }
            }
            return cell
        case CellIndices.selfiesCell.rawValue...CellIndices.polaroidsCell.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "collectionCell", for: indexPath) as? INSCollectionTableViewCell else { break }
            cell.labeltitle.textAlignment = .center

            switch CellIndices(rawValue: indexPath.row) {
            case .selfiesCell:
                cell.labeltitle.text = "SELFIES"

                if let selfies = dataManager.talentInfo.selfies {
                    var selfiesTaken: [Int: UIImage] = [:]

                    if let facePhoto = selfies.face { selfiesTaken[0] = UIImage(data: facePhoto) }
                    if let sideFacePhoto = selfies.sideFace { selfiesTaken[1] = UIImage(data: sideFacePhoto) }
                    if let wholeBodyPhoto = selfies.wholeBody { selfiesTaken[2] = UIImage(data: wholeBodyPhoto) }
                    cell.setPhotoCollection(totalItems: 3, currentInfo: selfiesTaken)
                } else {
                    cell.setPhotoCollection(totalItems: 3)
                }
            case .photoBookCell:
                cell.labeltitle.text = "BOOK"

                if let book = dataManager.talentInfo.book {
                    var photoBook: [Int: UIImage] = [:]
                    
                    for (index, photo) in book.enumerated() where photo != nil {
                        photoBook[index] = UIImage(data: photo!)
                    }
                    cell.setPhotoCollection(totalItems: 6, currentInfo: photoBook)
                }
                else {
                    cell.setPhotoCollection(totalItems: 6)
                }
            case .polaroidsCell:
                cell.labeltitle.text = "POLAROIDS"

                if let polaroids = dataManager.talentInfo.polaroids {
                    var polaroidsTaken: [Int: UIImage] = [:]

                    if let facePhoto = polaroids.face { polaroidsTaken[0] = UIImage(data: facePhoto) }
                    if let frontPhoto = polaroids.front { polaroidsTaken[1] = UIImage(data: frontPhoto) }
                    if let backPhoto = polaroids.back { polaroidsTaken[2] = UIImage(data: backPhoto) }
                    if let leftSidePhoto = polaroids.leftSideFace { polaroidsTaken[3] = UIImage(data: leftSidePhoto) }
                    if let rightSidePhoto = polaroids.rightSideFace { polaroidsTaken[4] = UIImage(data: rightSidePhoto) }
                    if let treeQuartersPhoto = polaroids.threeQuarters { polaroidsTaken[5] = UIImage(data: treeQuartersPhoto) }

                    cell.setPhotoCollection(totalItems: 6, currentInfo: polaroidsTaken)
                } else {
                    cell.setPhotoCollection(totalItems: 6)
                }
            default:
                return cell
            }
            return cell
        case CellIndices.videoCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath)
            return cell
        case CellIndices.filesCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "filesCell", for: indexPath)
            if let securitySocial = cell.viewWithTag(100) as? UIButton, let ine = cell.viewWithTag(101) as? UIButton {
                securitySocial.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                ine.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            }
            return cell
        case CellIndices.foreignFilesCell.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "foreignFilesCell", for: indexPath)
            if let securitySocial = cell.viewWithTag(100) as? UIButton, let fm3 = cell.viewWithTag(101) as? UIButton {
                securitySocial.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                fm3.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
            }
            return cell
        default:
            break;
        }
        
        return UITableViewCell()
    }
    
    //MARK: Notification observers
    
    @objc func takePhotoAction(_ notification: Notification) {
        guard let buttonPressed = notification.object as? UIButton else { return }
        currentPhotoButtonPressed = buttonPressed
        takePhoto()
    }
    
    @objc func selectSpecialityAction(_ notification: Notification) {
        guard let buttonPressed = notification.object as? UIButton,
            let optionSelected = buttonPressed.titleLabel?.text else {
                return
        }
        dataManager.updateInfo((buttonPressed.isSelected, optionSelected), for: .specialities)
    }
    
    @objc func didSliderValueChanged(_ notification: Notification) {
        guard let slider = notification.object as? UISlider else { return }
        let sliderPosition = slider.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at:sliderPosition) {
            switch CellIndices(rawValue: indexPath.row) {
            case .ageCell:
                dataManager.updateInfo(Int(slider.value), for: .age)
            case .heightCell:
                dataManager.updateInfo(slider.value, for: .height)
            case .upperBodyCell:
                dataManager.updateInfo(slider.value, for: .upperBodySize)
            case .middleBodyCell:
                dataManager.updateInfo(slider.value, for: .middleBodySize)
            case .lowBodyCell:
                dataManager.updateInfo(slider.value, for: .lowerBodySize)
            case .weightCell:
                dataManager.updateInfo(slider.value, for: .weight)
            case .footwearCell:
                dataManager.updateInfo(slider.value, for: .footwear)
            default:
                break
            }
        }
    }
}

extension INSUpdateTalentDataFormTableViewController: CameraManagementDelegate {
    
    func didPhotoTaken(image: UIImage) {
        if let buttonPosition = currentPhotoButtonPressed?.convert(CGPoint.zero, to: self.tableView),
            let indexPath = self.tableView.indexPathForRow(at:buttonPosition) {
            switch CellIndices(rawValue: indexPath.row) {
            case .selfiesCell:
                let selfieType = INSTalentDataManager.SelfieType(rawValue: currentPhotoButtonPressed?.tag ?? -1)
                dataManager.updateInfo([selfieType: image], for: .selfies)
                tableView.reloadData()
            case .photoBookCell:
                dataManager.updateInfo([currentPhotoButtonPressed?.tag: image], for: .photoBook)
                tableView.reloadData()
            case .polaroidsCell:
                let polaroidType = INSTalentDataManager.PolaroidType(rawValue: currentPhotoButtonPressed?.tag ?? -1)
                dataManager.updateInfo([polaroidType: image], for: .polaroids)
                tableView.reloadData()
            default:
                break
            }
        }
    }
    
    func didMovieRecorded(urlVideo: URL, thumbnailImage: UIImage) {
        currentPhotoButtonPressed?.setImage(thumbnailImage, for: .normal)
        currentPhotoButtonPressed?.layer.cornerRadius = (currentPhotoButtonPressed?.frame.size.width ?? 0)/2
        let videoType: INSTalentDataManager.VideoType = currentPhotoButtonPressed?.tag == 100 ? .demoReel : .casting
        dataManager.updateInfo([videoType: (urlVideo, thumbnailImage)], for: .videos)
    }
}

extension INSUpdateTalentDataFormTableViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == eyeColorPicker {
            return INSTalent.EyeColor.allCases.count
        } else {
            return INSTalent.HairColor.allCases.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == eyeColorPicker {
            return INSTalent.EyeColor.allCases[row].rawValue
        } else {
            return INSTalent.HairColor.allCases[row].rawValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == eyeColorPicker {
            dataManager.updateInfo(INSTalent.EyeColor.allCases[row], for: .eyeColor)
        } else {
            dataManager.updateInfo(INSTalent.HairColor.allCases[row], for: .hairColor)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var text = ""
        if pickerView == eyeColorPicker {
            text = INSTalent.EyeColor.allCases[row].rawValue
        } else {
            text = INSTalent.HairColor.allCases[row].rawValue
        }
        return NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3725490196, green: 0.3764705882, blue: 0.3764705882, alpha: 1)])
    }
}
