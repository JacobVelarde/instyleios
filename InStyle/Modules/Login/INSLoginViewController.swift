//
//  INSLoginViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 11/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSLoginViewController: INSBaseViewController {
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var buttonEnter: UIButton!
    @IBOutlet weak var topSeparatorConstraint: NSLayoutConstraint!

    let dataController = INSDataController.sharedDataController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    fileprivate func setup() {
        if let email = UserDefaults.standard.value(forKey: "email") as? String, email.count > 0 {
            textFieldEmail.text = email
        }
        buttonEnter.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        topSeparatorConstraint.constant = INSUtility.getValueForTopSeparatorConstraintInMainViews()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToStaffRoles" {
            let viewController:INSInitViewController = segue.destination as! INSInitViewController
            viewController.isTowardStaffRolesView = true
        }
    }

    //MARK: Actions
    @IBAction func enterAction(_ sender: Any) {
        let email = textFieldEmail.trimmingWhiteSpaces
        let password = textFieldPassword.trimmingWhiteSpaces

        guard !email.isEmpty else {
            self.showAlertControllerWith(title: nil, message: "EMPTY_EMAIL_TEXTFIELD".localized)
            return
        }
        
        guard !password.isEmpty else {
            self.showAlertControllerWith(title: nil, message: "EMPTY_PASSWORD_TEXTFIELD".localized)
            return
        }
        
        guard textFieldEmail.validateEmailFormat() else {
            self.showAlertControllerWith(title: nil, message: "INVALID_EMAIL".localized)
            return
        }
        
        //TODO: validate email format after uploading the textfield extension
        self.showActivityIndicator()
         let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_LOGIN_OPCODE, requestParameters: [Keys.username: email, Keys.password: password, Keys.imei: UserDefaults.standard.value(forKey: Keys.imei) ?? String.emptyString], completionHandler: { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.dataController.isLoggedIn = true
                    self.textFieldPassword.text = ""
                    UserDefaults.standard.setValue(email, forKey: "email")
                    if let isFromPushNotification = UserDefaults.standard.value(forKey: "pushNotification") as? Bool, isFromPushNotification {
                        UserDefaults.standard.removeObject(forKey: "pushNotification")
                        self.performSegue(withIdentifier: "showDashboardFromLoginSegue", sender: self)
                    } else {
                        self.performSegue(withIdentifier: "showAreaOfIntrestSegue", sender: self)
                    }
                } else {
                    if let _error = error, let errorMessage = _error.errorMessage, !errorMessage.isEmpty {
                        self.showAlertControllerWith(message: error?.errorMessage)
                    } else {
                        self.showAlertControllerWith(title: String.emptyString, message: "DEFAULT_WEB_ERROR_MEESAGE".localized)
                    }
                }
            }
        })
    }
    
    @IBAction func recoverPasswordAction(_ sender: Any) {
        let email = textFieldEmail.trimmingWhiteSpaces

        guard !email.isEmpty else {
            self.showAlertControllerWith(title: nil, message: "EMPTY_EMAIL_TEXTFIELD".localized)
            return
        }
        
        guard textFieldEmail.validateEmailFormat() else {
            self.showAlertControllerWith(title: nil, message: "INVALID_EMAIL".localized)
            return
        }

        self.showActivityIndicator()
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_RETRIEVE_PASSWORD_OPCODE, requestParameters: ["username":
            email], completionHandler: { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                self.dismissActivityIndicator()
                if success {
                    self.showAlertWith(message: jsonResponse["mensaje"] as? String, imageName: "little_plane")
                }else {
                    self.showAlertWith(message: error?.errorMessage)
                }
            }
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func unwind(for unwindSegue: UIStoryboardSegue) {
    }    
}

extension INSLoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldEmail {
            textFieldEmail.text = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textFieldPassword.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textFieldEmail {
            textFieldEmail.text = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
}
