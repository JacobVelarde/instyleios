//
//  ViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 10/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSInitViewController: UIViewController {
    
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet weak var labelPersonalBooker: UILabel!
    @IBOutlet weak var buttonAccess: UIButton!
    
    @IBOutlet weak var stackViewStaffRoles: UIStackView!
    @IBOutlet weak var buttonCustomer: UIButton!
    @IBOutlet weak var buttonTalent: UIButton!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var topSeparatorConstraint: NSLayoutConstraint!
    @IBOutlet weak var topSeparatorConstraintRolesView: NSLayoutConstraint!

    var isTowardStaffRolesView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUp()
    }
    
    fileprivate func setUp(){
        let attrStr = NSMutableAttributedString(string: labelPersonalBooker.text ?? "")
        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: INSConstants.ThemeColor.darkTurquoise, range: (labelPersonalBooker.text! as NSString).range(of: "A"))
        labelPersonalBooker.attributedText = attrStr
        
        if !isTowardStaffRolesView {
            navigationController?.setNavigationBarHidden(true, animated: false)
            topSeparatorConstraint.constant = INSUtility.getValueForTopSeparatorConstraintInMainViews()
            buttonAccess.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
            labelPersonalBooker.fadeIn(duration: 1.0, delay: 1.0, completion: {
                (finished: Bool) -> Void in
            })
            buttonAccess.fadeIn(duration: 1.0, delay: 1.0, completion: {
                (finished: Bool) -> Void in
            })
        } else {
            navigationController?.setNavigationBarHidden(false, animated: false)
            topSeparatorConstraintRolesView.constant = INSUtility.getValueForTopSeparatorConstraintInMainViews() - (navigationController?.navigationBar.frame.height ?? 40)
        }
    }

    @IBAction func accessAction(_ sender: Any) {
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}

