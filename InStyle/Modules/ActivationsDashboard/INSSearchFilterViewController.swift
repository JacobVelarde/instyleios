//
//  INSSearchFilterViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 13/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSSearchFilterViewController: UIViewController {
    @IBOutlet weak var searchFilterCollectionView: UICollectionView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendarContainer: UIView!
    @IBOutlet weak var activationsListContainer: UIView!
    @IBOutlet weak var activationsSelectionContainer: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var cleanFiltersButton: UIButton!
    
    fileprivate let filterManager = INSFilterManager()
    fileprivate var filterOptions: [String] = []
    fileprivate var filterPickerView = UIPickerView()
    fileprivate var pickerToolbar = UIToolbar()
    fileprivate var filterValues: [Any] = [[:]]
    fileprivate var pickerBackground = UIView()
    fileprivate var filterCategorySelected: UIButton?
    fileprivate var selectionController: INSActivationSelectionViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        registerCells()
        setPickerComponent()
        calendarContainer.isHidden = true
        filterOptions = filterManager.filterOptions()
        filterManager.setDefaultFilters()
        filterManager.delegate = self
        showActivityIndicator()
        filterManager.fetchActivations()
        cleanFiltersButton.layer.borderColor = INSConstants.ThemeColor.darkTurquoise.cgColor        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(filterCategorySelected(_:)), name: INSFilterCollectionViewCell.filterCollectionNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectedActivations(_:)), name: INSActivationListViewController.seeSelectionResultsNotification, object: nil)
        setFilterComponent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func setFilterComponent() {
        let separatorSize = 20
        let sizeItem = (Int(searchFilterCollectionView.frame.size.width) - separatorSize) / 3 - 15
        let height = 30
        let collectionLayout = searchFilterCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionLayout?.itemSize = CGSize(width: sizeItem, height: height)
        searchFilterCollectionView.reloadData()
        searchFilterCollectionView.layoutIfNeeded()
    }
    
    fileprivate func setPickerComponent() {
        let pickerSize = CGSize(width: self.view.bounds.width, height: 200)
        filterPickerView.frame = CGRect(x: 0, y: self.view.bounds.size.height, width: pickerSize.width, height: pickerSize.height)
        filterPickerView.dataSource = self
        filterPickerView.delegate = self
        filterPickerView.backgroundColor = UIColor.white
        
        pickerBackground.frame = UIScreen.main.bounds
        pickerBackground.backgroundColor = UIColor.black
        pickerBackground.alpha = 0.0
                
        pickerToolbar.frame = CGRect(x: 0, y: filterPickerView.frame.origin.y, width: pickerSize.width, height: 45)
        pickerToolbar.barStyle = .default
        pickerToolbar.isTranslucent = true
        pickerToolbar.backgroundColor = INSConstants.ThemeColor.darkGray
        pickerToolbar.tintColor = INSConstants.ThemeColor.darkTurquoise
        pickerToolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Seleccionar", style: .plain, target: self, action: #selector(applyFilter))
        let closeButton = UIBarButtonItem(title: "Cerrar", style: .plain, target: self, action: #selector(dismissPicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickerToolbar.items = [closeButton, spacer, doneButton]
        UIApplication.shared.keyWindow?.addSubview(self.pickerBackground)
        UIApplication.shared.keyWindow?.addSubview(self.pickerToolbar)
        UIApplication.shared.keyWindow?.addSubview(self.filterPickerView)
        pickerBackground.isHidden = true
    }
    
    fileprivate func registerCells() {
        searchFilterCollectionView.register(UINib(nibName: "INSFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "filterCell")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "activationDetailSegue" {
            if let destinationController = segue.destination as? INSActivationDetailViewController,
                let activation = sender as? INSActivation {
                destinationController.activationDetail = activation
            }
        } else if segue.identifier == "showCalendar" {
            if let destinationController = segue.destination as? INSCalendarViewController {
                destinationController.delegate = self
                destinationController.view.translatesAutoresizingMaskIntoConstraints = false
            }
        } else if segue.identifier == "showSelection" {
            if let destinationController = segue.destination as? INSActivationSelectionViewController {
                selectionController = destinationController
                selectionController?.view.translatesAutoresizingMaskIntoConstraints = false
            }
        }
    }
    
    fileprivate func showCalendar() {
        calendarContainer.isHidden = false
        activationsListContainer.isHidden = true
        searchButton.isHidden = true
    }
    
    fileprivate func showActivationsList() {
        calendarContainer.isHidden = true
        activationsListContainer.isHidden = false
        searchButton.isHidden = false
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        showActivityIndicator()
        filterManager.fetchActivations()
    }
    
    @IBAction func cleanFiltersAction(_ sender: UIButton) {
        filterCategorySelected = nil
        searchFilterCollectionView.isHidden = true
        searchFilterCollectionView.reloadSections(IndexSet(integer: 0))
        filterManager.setDefaultFilters()
        searchFilterCollectionView.performBatchUpdates(nil) { (result) in
            self.searchFilterCollectionView.isHidden = false
        }
    }
    
    @IBAction func showSelectionResults(_ sender: UIButton) {
        activationsSelectionContainer.isHidden = false
    }
    
    @objc func filterCategorySelected(_ notification: Notification) {
        if let optionPressed = notification.object as? UIButton, let title = optionPressed.titleLabel?.text {
            optionPressed.isSelected = false
            optionPressed.backgroundColor = UIColor.white
            filterCategorySelected = optionPressed
            
            if title.contains(INSFilterManager.FilterOptions.period.rawValue) {
                showCalendar()
            }
            else {
                if !calendarContainer.isHidden {
                    showActivationsList()
                }
                if title.contains(INSFilterManager.FilterOptions.client.rawValue) {
                    filterValues = filterManager.filterLists.clients
                } else if title.contains(INSFilterManager.FilterOptions.brand.rawValue) {
                    filterValues = filterManager.filterLists.brands
                } else if title.contains(INSFilterManager.FilterOptions.talent.rawValue) {
                    filterValues = filterManager.filterLists.categories
                } else if title.contains(INSFilterManager.FilterOptions.booker.rawValue) {
                    filterValues = filterManager.filterLists.bookers
                } else if title.contains(INSFilterManager.FilterOptions.coordinator.rawValue) {
                    filterValues = filterManager.filterLists.coordinators
                } else if title.contains(INSFilterManager.FilterOptions.operator.rawValue) {
                    filterValues = filterManager.filterLists.operators
                } else if title.contains(INSFilterManager.FilterOptions.dynamic.rawValue) {
                    filterValues = filterManager.filterLists.dynamics
                } else if title.contains(INSFilterManager.FilterOptions.place.rawValue) {
                    filterValues = filterManager.filterLists.locations as [Any]
                }
                
                self.view.isUserInteractionEnabled = false
                self.pickerToolbar.isUserInteractionEnabled = true
                self.filterPickerView.isUserInteractionEnabled = true
                if filterPickerView.frame.origin.y == self.view.bounds.size.height {
                    UIView.animate(withDuration: 0.5) {
                        self.pickerBackground.isHidden = false
                        self.pickerBackground.alpha = 0.3
                        self.filterPickerView.reloadAllComponents()
                        self.filterPickerView.frame.origin.y -= self.filterPickerView.frame.size.height
                        self.pickerToolbar.frame.origin.y -= (self.pickerToolbar.frame.size.height + self.filterPickerView.frame.size.height)
                    }
                } else {
                    self.filterPickerView.reloadComponent(0)
                }
            }
        }
    }
    
    @objc func applyFilter() {
        if !filterValues.isEmpty {
            let currentSelection = filterValues[filterPickerView.selectedRow(inComponent: 0)]
            if let category = currentSelection as? INSFilterManager.categoryValue {
                filterValues = filterManager.filterLists.talents.filter {
                    return $0.categoryId == INSTalent.Category(rawValue: Int(category.id) ?? 0)
                }
                filterPickerView.reloadAllComponents()
            }
            else {
                filterCategorySelected?.isSelected = true
                filterCategorySelected?.backgroundColor = INSConstants.ThemeColor.darkTurquoise
                if let customer = currentSelection as? INSCustomer {
                    filterManager.applyingFilters["cl"] = Int(customer.idUser ?? "-1")
                } else if let talent = currentSelection as? INSTalent {
                    filterManager.applyingFilters["ta"] = Int(talent.idUser ?? "-1")
                    filterManager.applyingFilters["tac"] = talent.categoryId?.rawValue ?? -1
                } else if let brand = currentSelection as? INSBrand {
                    filterManager.applyingFilters["ma"] = Int(brand.id ?? "-1")
                } else if let booker = currentSelection as? INSBooker {
                    filterManager.applyingFilters["bo"] = Int(booker.idUser ?? "-1")
                } else if let coordinator = currentSelection as? INSCoordinator {
                    filterManager.applyingFilters["co"] = Int(coordinator.idUser ?? "-1")
                } else if let `operator` = currentSelection as? INSOperator {
                    filterManager.applyingFilters["op"] = Int(`operator`.idUser ?? "-1")
                } else if let dynamic = currentSelection as? INSDynamic {
                    filterManager.applyingFilters["di"] = Int(dynamic.id?.rawValue ?? -1)
                } else if let location = currentSelection as? String {
                    filterManager.applyingFilters["se"] = location
                }
                dismissPicker()
            }
        }
    }
    
    @objc func dismissPicker() {
       self.view.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.5) {
            self.pickerBackground.isHidden = true
            self.pickerBackground.alpha = 0.0
            self.filterPickerView.frame.origin.y += self.filterPickerView.frame.size.height
            self.pickerToolbar.frame.origin.y += (self.pickerToolbar.frame.size.height + self.filterPickerView.frame.size.height)
        }
    }
    
    @objc func selectedActivations(_ notification: Notification) {
        guard let _activations = notification.object as? [INSActivation], !_activations.isEmpty else { return }
        activationsSelectionContainer.isHidden = false
        selectionController?.selectedActivations = _activations
    }
//
//    @objc func enableActivationsSelection(_ notification: Notification) {
//        guard let isSelectionEnabled = notification.object as? Bool else { return }
//        seeResultsViewSection.isHidden = isSelectionEnabled ? false : true
//        self.view.layoutIfNeeded()
//    }
}

extension INSSearchFilterViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath) as? INSFilterCollectionViewCell {
            cell.buttonOption.setTitle(filterOptions[indexPath.row], for: .normal)
            cell.buttonOption.tag = indexPath.row
            return cell
        }
        return UICollectionViewCell()
    }
}

extension INSSearchFilterViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        pickerToolbar.items?.last?.isEnabled = filterValues.isEmpty ? false : true
        return filterValues.count
    }
}

extension INSSearchFilterViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let clients = filterValues as? [INSCustomer], !clients.isEmpty {
            return clients[row].company
        } else if let talents = filterValues as? [INSTalent], !talents.isEmpty {
            return talents[row].completeName
        } else if let categories = filterValues as? [INSFilterManager.categoryValue], categories.count > 0 {
            return categories[row].name
        } else if let brands = filterValues as? [INSBrand], !brands.isEmpty {
            return brands[row].name
        } else if let bookers = filterValues as? [INSBooker], !bookers.isEmpty {
            return bookers[row].completeName
        } else if let coordinators = filterValues as? [INSCoordinator], !coordinators.isEmpty {
            return coordinators[row].completeName
        } else if let operators = filterValues as? [INSOperator], !operators.isEmpty {
            return operators[row].name
        } else if let dynamics = filterValues as? [INSDynamic], !dynamics.isEmpty {
            return dynamics[row].description
        } else if let locations = filterValues as? [String], !locations.isEmpty {
            return locations[row]
        }
        return nil
    }
}

extension INSSearchFilterViewController: FilterDelegate {
    
    func didSuccessForOperaion(_ operation: WsFilterOperation, response: Any?) {
        DispatchQueue.main.async {
            self.dismissActivityIndicator()
            NotificationCenter.default.post(name: Notification.Name("refreshActivations"), object: nil, userInfo: ["data": self.filterManager.filteredActivations])
        }
    }
    
    func didFailForOperation(_ operation: WsFilterOperation, message: String?) {
        DispatchQueue.main.async {
            self.dismissActivityIndicator()
            self.showAlertControllerWith(title: "DEFAULT_ALERT_TITLE".localized, message: message)
        }
    }
}

extension INSSearchFilterViewController: CalendarDelegate {
    func activationsFor(selectedDates: [Date]) {
        guard let startDate = selectedDates.first, let endDate = selectedDates.last else {
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let _startDate = dateFormatter.string(from: startDate)
        let _endDate = dateFormatter.string(from: endDate)
        filterManager.applyingFilters["pe"] = "\(_startDate)*\(_endDate)"
        filterCategorySelected?.isSelected = true
        filterCategorySelected?.backgroundColor = INSConstants.ThemeColor.darkTurquoise
        showActivationsList()
        showActivityIndicator()
        filterManager.fetchActivations()
    }
}
