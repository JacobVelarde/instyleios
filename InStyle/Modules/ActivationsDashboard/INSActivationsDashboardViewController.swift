//
//  INSActivationsDashboardViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 12/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

enum Activations: Int {
    case completed = 1
    case inProgress = 2
    case next = 3
    case total = 4
}

class INSActivationsDashboardViewController: UIViewController {

    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var buttonCompleted: UIButton!
    @IBOutlet weak var buttonInProgress: UIButton!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonTotal: UIButton!
    @IBOutlet weak var labelCompleted: UILabel!
    @IBOutlet weak var labelInProgress: UILabel!
    @IBOutlet weak var labelNext: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var tableViewActivations: UITableView!
    @IBOutlet weak var constraintHeightTopView: NSLayoutConstraint!
    @IBOutlet weak var tableViewActivationsByDate: UITableView!
    @IBOutlet weak var constraintTableViewByDateHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonAddActivation: UIButton!
    
    fileprivate let dataManager = INSActivationsDataManager.activationsDataManager
    private var activations: INSActivations?
    private var sectionHeader: String?
    private var activationsByDateList: [INSActivation]?
    private var tableViewHeight = 0
    private var selectedDate: String?
    private var calendarController: INSCalendarViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        activationsAction(buttonInProgress)
        // Do any additional setup after loading the view.
        setUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataManager.delegate = self
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func setUp() {
        if INSDataController.sharedDataController.user.profile == .talent {
            buttonAddActivation.alpha = 0
            buttonAddActivation.isUserInteractionEnabled = false
        }
        self.tableViewActivations.sectionHeaderHeight =  UITableView.automaticDimension
        self.tableViewActivations.estimatedSectionHeaderHeight = 25;
        tableViewActivations.register(UINib(nibName: "INSActivationTableViewCell", bundle: Bundle(for: INSActivationTableViewCell.self)), forCellReuseIdentifier: "activationCell")
        tableViewActivationsByDate.register(UINib(nibName: "INSActivationByDateTableViewCell", bundle: Bundle(for: INSActivationByDateTableViewCell.self)), forCellReuseIdentifier: "activationByDateCell")
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "activationDetailSegue" {
            let viewController:INSActivationDetailViewController = segue.destination as! INSActivationDetailViewController
            viewController.activationDetail = sender as? INSActivation
        } else if segue.identifier == "showCalendar" {
            if let destinationController = segue.destination as? INSCalendarViewController {
                calendarController = destinationController
                calendarController?.delegate = self
                calendarController?.calendarType = .singleDaySelection
                calendarController?.view.translatesAutoresizingMaskIntoConstraints = false
            }
        }
    }

    func fetchActivationsFor(activation: Activations) {
        var screen: String?
        switch activation {
            case .completed:
                screen = "completadas"
                break
            case .inProgress:
                screen = "encurso"
                break
            case .next:
                screen = "proximas"
                break
            case .total:
                screen = "total"
                break
        }
        self.showActivityIndicator()
        dataManager.fetchActivationsFor(activationStatus: screen ?? "")
    }

    private func refreshTopView() {
        self.tableViewActivations.separatorStyle = .none
        labelCompleted.text = activations?.completed
        labelInProgress.text = activations?.inProgress
        labelNext.text = activations?.next
        labelTotal.text = activations?.total
        let rows: Int = activations?.activationsList.count ?? 0
        if rows == 0 {
            constraintHeightTopView.constant = 200
        } else if rows >= 5 {
            constraintHeightTopView.constant = 410
        } else {
            constraintHeightTopView.constant = CGFloat(rows * 40)
        }
        tableViewActivations.reloadData()
    }

    private func refreshTableByDate() {
        self.tableViewActivationsByDate.separatorStyle = .none
        tableViewActivationsByDate.reloadData()
        tableViewActivationsByDate.layoutIfNeeded()
        self.constraintTableViewByDateHeight.constant = tableViewActivationsByDate.contentSize.height
        updateViewConstraints()
    }

    //MARK: IBAction's button
    @IBAction func activationsAction(_ sender: UIButton) {
        buttonCompleted.setImage(UIImage(named: "activationsCompletedIconOff"), for: .normal)
        buttonInProgress.setImage(UIImage(named: "runnerIconOff"), for: .normal)
        buttonNext.setImage(UIImage(named: "calendarIconOff"), for: .normal)
        buttonTotal.setImage(UIImage(named: "totalActivationsIconOff"), for: .normal)
        let activation: Activations = Activations(rawValue: sender.tag) ?? Activations.inProgress
        switch activation {
        case .completed:
            buttonCompleted.setImage(UIImage(named: "activationsCompletedIconOn"), for: .normal)
            sectionHeader = "ACTIVACIONES COMPLETADAS"
            break
        case .inProgress:
            buttonInProgress.setImage(UIImage(named: "runnerIconOn"), for: .normal)
            sectionHeader = "ACTIVACIONES EN CURSO"
            break
        case .next:
            buttonNext.setImage(UIImage(named: "calendarIconOn"), for: .normal)
            sectionHeader = "ACTIVACIONES PRÓXIMAS"
            break
        case .total:
            buttonTotal.setImage(UIImage(named: "totalActivationsIconOn"), for: .normal)
            sectionHeader = "ACTIVACIONES TOTALES"
            break
        }
        fetchActivationsFor(activation: activation)
    }

    @IBAction func menuAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func addAction(_ sender: Any) {
        self.performSegue(withIdentifier: "shoeNewActivationSegue", sender: self)
    }
}

extension INSActivationsDashboardViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewActivations {
            return activations?.activationsList.count ?? 0
        } else if tableView == tableViewActivationsByDate {
            return activationsByDateList?.count ?? 0
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewActivations {
            if let activation = activations?.activationsList[indexPath.row] {
                let cell = tableView.dequeueReusableCell(withIdentifier: "activationCell", for: indexPath) as! INSActivationTableViewCell
                cell.activation = activation
                cell.buttonAction = {(_) in
                    self.showActivityIndicator()
                    self.dataManager.fetchDetailFor(activation: activation, forListType: .all)
                }
                tableView.separatorStyle = .singleLine
                return cell
            }
        } else if tableView == tableViewActivationsByDate {
            let cell = tableView.dequeueReusableCell(withIdentifier: "activationByDateCell", for: indexPath) as! INSActivationByDateTableViewCell
            if let activation = activationsByDateList?[indexPath.row] {
                cell.setActivation(activation: activation, selectedDate: selectedDate)
                cell.labelAction = {(_) in
                    self.showActivityIndicator()
                    self.dataManager.fetchDetailFor(activation: activation, forListType: .byDate)
                }
                tableView.separatorStyle = .singleLine
                return cell
            }
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tableViewActivations {
            let view:INSHeaderTableView = INSHeaderTableView.instanceFromNib() as! INSHeaderTableView
            view.labelTitleHeader.text = sectionHeader
            return view
        }
        return nil
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableViewActivations {
            return UITableView.automaticDimension
        }
        return 0.0
    }
}

extension INSActivationsDashboardViewController: ActivationsDataManagerDelegate {
    func didSuccessForOperation(_ operation: INSConstants.OperationCodes, response: Any?) {
        self.dismissActivityIndicator()
        switch operation {
        case .INS_GET_ACTIVATIONS_PANEL_CODE:
            self.activations = response as? INSActivations
            self.refreshTopView()
        case .INS_GET_ACTIVATION_DETAIL_CODE:
            self.performSegue(withIdentifier: "activationDetailSegue", sender: response as! INSActivation)
        case .INS_GET_ACTIVATIONS_BY_DATE_CODE:
            activationsByDateList = response as? [INSActivation]
            refreshTableByDate()
        case .INS_GET_CALENDAR_ACTIVATION_DAYS_CODE:
            if let days = response as? [Int] {
                calendarController?.activationDays = days
            } else {
                calendarController?.activationDays = nil
            }
            break
        default:
            break
        }
    }

    func didFailForOperation(_ operation: INSConstants.OperationCodes, message: String?) {
        self.dismissActivityIndicator()
        if operation == .INS_GET_CALENDAR_ACTIVATION_DAYS_CODE {
            calendarController?.activationDays = nil
        } else {
            if var _message = message, _message.isEmpty {
                _message = "DEFAULT_WEB_ERROR_MEESAGE".localized
                showAlertControllerWith(message: _message)
            }
            showAlertControllerWith(message: message)
        }
    }
}

extension INSActivationsDashboardViewController: CalendarDelegate {
    func activationsFor(selectedDates: [Date]) {
        if let _selectedDate = selectedDates.first {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = "yyyy-MM-dd"
            selectedDate = dateFormatter.string(from: _selectedDate)
            if !isLoaderShowing() {
                showActivityIndicator()
            }
            dataManager.fetchActivationsFor(date: selectedDate ?? "")
        }
    }

    func switchingMonth(date: Date?) {
        guard let _date = date else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "yyyy-MM"
        showActivityIndicator()
        dataManager.fetchCalendarActivationDays(for: dateFormatter.string(from: _date))
    }
}
