//
//  INSMapViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 17/05/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol LocationDelegate : NSObject {
    func selected(latitude : Double, longitude : Double)
}

class INSMapViewController: INSBaseViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var buttonAction: UIButton!
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocationCoordinate2D?
    private var newAnnotation = MKPointAnnotation()
    weak var delegate:LocationDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        buttonAction.setTitle("TAKE_CURRENT_LOCATION".localized, for: .normal)
        buttonAction.titleLabel?.textAlignment = .center
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 200
        locationManager.requestWhenInUseAuthorization()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(handleTap(gestureRecognizer:)))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func handleTap(gestureRecognizer: UILongPressGestureRecognizer) {
        mapView.removeAnnotations(mapView.annotations)

        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)

        // Add annotation:
        newAnnotation = MKPointAnnotation()
        newAnnotation.coordinate = coordinate
        mapView.addAnnotation(newAnnotation)
        buttonAction.setTitle("TAKE_ANOTHER_LOCATION".localized, for: .normal)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location.coordinate
            let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
            mapView.setRegion(region, animated: false)
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func takeAnnotationAction(_ sender: Any) {
        if buttonAction.title(for: .normal) == "TAKE_ANOTHER_LOCATION".localized {
            delegate?.selected(latitude: newAnnotation.coordinate.latitude, longitude: newAnnotation.coordinate.longitude)
        } else {
            delegate?.selected(latitude: currentLocation?.latitude ?? 0.0, longitude: currentLocation?.longitude ?? 0.0)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
