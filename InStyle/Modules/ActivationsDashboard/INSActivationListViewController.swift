//
//  INSActivationListViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 14/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSActivationListViewController: UIViewController {
    @IBOutlet weak var activationTableView: UITableView!
    @IBOutlet weak var showResultsBottomView: UIView!
    fileprivate var activations: [INSActivation]?
    fileprivate var dataManager = INSDataController.sharedDataController
    fileprivate var activationsSelection: [Int] = []
    fileprivate var isSelectionEnabled: Bool = false
    internal static var seeSelectionResultsNotification = Notification.Name(rawValue: "seeSelectionResultsNotification")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        activationTableView.register(UINib(nibName: "INSActivationTableViewCell", bundle: nil), forCellReuseIdentifier: "activationCell")
        NotificationCenter.default.addObserver(self, selector: #selector(refreshActivations(_:)), name: Notification.Name(rawValue: "refreshActivations"), object: nil)
        
        activationTableView.sectionHeaderHeight =  UITableView.automaticDimension
        activationTableView.estimatedSectionHeaderHeight = 25;
        activationTableView.tableFooterView = UIView()
    }
    
    @objc fileprivate func refreshActivations(_ notification: Notification) {
        guard let _activations = notification.userInfo?["data"] as? [INSActivation] else { return }
        activations = _activations
        activationTableView.reloadData()
    }
    
    @IBAction func showSelectionResultsAction(_ sender: UIButton) {
        var selectedActivations: [INSActivation] = []
        for index in activationsSelection {
            guard let _activation = activations?[index] else { continue }
            selectedActivations.append(_activation)
        }
        
        NotificationCenter.default.post(name: INSActivationListViewController.seeSelectionResultsNotification, object: selectedActivations)
    }
    
    func fetchDetailFor(activation: INSActivation) {
        self.parent?.showActivityIndicator()
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_ACTIVATION_DETAIL_CODE, requestParameters: nil, queryParameters: [Keys.idEvent: activation.id ?? ""], completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = jsonResponse["codigo"] as? Int, code == 201 {
                _success = true
            }
            DispatchQueue.main.async {
                self.parent?.dismissActivityIndicator()
                if _success {
                    if let responseDict = jsonResponse["objeto"] as? [String:Any] {
                        activation.updateActivation(activation: responseDict)
                        self.parent?.performSegue(withIdentifier: "activationDetailSegue", sender: activation)
                    }
                }
            }
        })
    }
}

extension INSActivationListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "activationCell", for: indexPath) as? INSActivationTableViewCell {
            cell.activation = activations?[indexPath.row]
            cell.buttonAction = {(_) in
                if let _activation = self.activations?[indexPath.row] {
                    self.fetchDetailFor(activation: _activation)
                }
            }
            if isSelectionEnabled {
                cell.selectorButton.isSelected = activationsSelection.contains(indexPath.row)
                cell.selectorButton.isHidden = false
                cell.selectorAction = {(_) in
                    if cell.selectorButton.isSelected {
                        self.activationsSelection.append(indexPath.row)
                    } else {
                        self.activationsSelection = self.activationsSelection.filter { $0 != indexPath.row }
                    }
                }
            } else {
                cell.selectorButton.isHidden = true
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view:INSHeaderTableView = INSHeaderTableView.instanceFromNib() as! INSHeaderTableView
        view.labelTitleHeader.text = "Activaciones"
        view.buttonIcon.isHidden = false
        view.buttonIcon.setImage(nil, for: .normal)
        view.buttonIcon.setTitle("Seleccionar", for: .normal)
        view.buttonWidthConstraimt.constant = 100
        view.buttonAction = {(_) in
            self.isSelectionEnabled = !self.isSelectionEnabled
            self.showResultsBottomView.isHidden = self.isSelectionEnabled ? false : true
            self.activationTableView.reloadData()
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}
