//
//  INSNewActivationViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 25/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSNewActivationViewController: INSBaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackViewContainer: UIStackView!
    @IBOutlet weak var textfieldActivationName: INSFormTextField!
    @IBOutlet weak var textfieldActivationType: INSFormTextField!
    @IBOutlet weak var textfieldCustomer: INSFormTextField!
    @IBOutlet weak var textfieldProducer: INSFormTextField!
    @IBOutlet weak var textfieldBrand: INSFormTextField!
    @IBOutlet weak var textfieldSede: INSFormTextField!
    @IBOutlet weak var textfieldOperator: INSFormTextField!
    @IBOutlet weak var textfieldStartHour: INSFormTextField!
    @IBOutlet weak var textfieldDate: INSFormTextField!
    @IBOutlet weak var textfieldEndHour: INSFormTextField!
    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
    
    var arrayPicker: [Any] = []
    var selectedOption: Any?
    var textfieldSelected: UITextField?
    var talentsArray: [INSTalent] = []
    var coordinatorsArray: [INSCoordinator] = []
    var backArray: [INSTalent] = []

    let dataManager = INSActivationsDataManager.activationsDataManager
    let pickerView = UIPickerView()
    var loadingContainer: UIView?

    private var selectedTalentList:[Any] = []
    private var selectedCoordinatorList:[Any] = []
    private var selectedBackList:[Any] = []

    var talentsFiltersContainer:INSGenericFilterCollectionView?
    var talentsCollectionView:INSGenericCollectionView?

    var coordinatorsFiltersContainer:INSGenericFilterCollectionView?
    var coordinatorsCollectionView:INSGenericCollectionView?
    
    var backTalentsFiltersContainer:INSGenericFilterCollectionView?
    var backTalentsCollectionView:INSGenericCollectionView?

    let dataController = INSDataController.sharedDataController

    var sections:[String] = ["TALENTOS", "COORDINADORES", "BACK", "RESUMEN DE COSTOS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        dataManager.fetchTalents(completion: { (success, talentList) in
            if success {
                self.talentsArray = talentList ?? []
                self.backArray = talentList ?? []
                self.createTalentsCollection()
            }
        })
        dataManager.fetchCoordinators(completion: {(success, coordinatorList) in
            if success {
                self.coordinatorsArray = coordinatorList ?? []
                self.createCoordinatorsCollection()
                self.createBackTalentsCollection()
            }
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        heightTableViewConstraint.constant = tableView.contentSize.height
    }

    func setup(){
        tableView.register(UINib(nibName: "CostsTableViewCell"
        , bundle: Bundle(for: CostsTableViewCell.self)), forCellReuseIdentifier: "costsCell")
        
        self.navigationController?.isNavigationBarHidden = false
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        dataManager.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        textfieldActivationType.inputView = pickerView
        textfieldCustomer.inputView = pickerView
    }

    func createTalentsCollection() {
        talentsFiltersContainer = INSGenericFilterCollectionView.instanceFromNib(delegate: self, role: .talents) as? INSGenericFilterCollectionView
        talentsFiltersContainer?.stackViewProfile.isHidden = true
        talentsFiltersContainer?.filterOptions = []
        talentsFiltersContainer?.labelHeader.text = "TALENTS".localized
        stackViewContainer.addArrangedSubview(talentsFiltersContainer!)

        talentsCollectionView = INSGenericCollectionView.instanceFromNib(selectionType: .MultipleSelection, collectionType: .normal) as? INSGenericCollectionView
        talentsCollectionView?.allItems = talentsArray
        talentsCollectionView?.delegate = self
        stackViewContainer.addArrangedSubview(talentsCollectionView!)
    }
    
    func createBackTalentsCollection() {
        backTalentsFiltersContainer = INSGenericFilterCollectionView.instanceFromNib(delegate: self, role: .talents) as? INSGenericFilterCollectionView
        backTalentsFiltersContainer?.stackViewProfile.isHidden = true
        backTalentsFiltersContainer?.filterOptions = []
        backTalentsFiltersContainer?.labelHeader.text = "BACK".localized
        stackViewContainer.addArrangedSubview(backTalentsFiltersContainer!)

        backTalentsCollectionView = INSGenericCollectionView.instanceFromNib(selectionType: .MultipleSelection, collectionType: .normal, isBack: true) as? INSGenericCollectionView
        backTalentsCollectionView?.allItems = talentsArray
        backTalentsCollectionView?.delegate = self
        stackViewContainer.addArrangedSubview(backTalentsCollectionView!)
    }

    func createCoordinatorsCollection() {
        coordinatorsFiltersContainer = INSGenericFilterCollectionView.instanceFromNib(delegate: self, role: .coordinators) as? INSGenericFilterCollectionView
        coordinatorsFiltersContainer?.stackViewProfile.isHidden = true
        coordinatorsFiltersContainer?.filterOptions = []
        coordinatorsFiltersContainer?.labelHeader.text = "COORDINATORS".localized
        stackViewContainer.addArrangedSubview(coordinatorsFiltersContainer!)

        coordinatorsCollectionView = INSGenericCollectionView.instanceFromNib(selectionType: .MultipleSelection, collectionType: .normal) as? INSGenericCollectionView
        coordinatorsCollectionView?.allItems = coordinatorsArray
        coordinatorsCollectionView?.delegate = self
        stackViewContainer.addArrangedSubview(coordinatorsCollectionView!)
    }
    
    // MARK: - IBAction's
    @IBAction func closeAction(_ sender: Any) {
        self.talentsArray.removeAll()
        self.coordinatorsArray.removeAll()
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func requestActivationAction(_ sender: Any) {
        showAlertWith(message: "TU SOLICITUD\nHA SIDO ENVIADA", imageName: "selectedIcon", alertType: .imageUpMeessage)
    }

    @IBAction func checkAction(_ sender: Any) {
        if checkbox.isSelected {
            checkbox.setImage(UIImage(named: "offCheck"), for: .normal)
            checkbox.isSelected = false
        } else {
            checkbox.setImage(UIImage(named: "selectedIcon"), for: .normal)
            checkbox.isSelected = true
        }
        updateSubmitButton()
    }


    fileprivate func updateSubmitButton() {
        if textfieldActivationName.isValidated, textfieldActivationType.isValidated, textfieldCustomer.isValidated, textfieldProducer.isValidated, textfieldBrand.isValidated, textfieldOperator.isValidated, textfieldDate.isValidated, textfieldStartHour.isValidated, textfieldEndHour.isValidated, selectedTalentList.count > 0, selectedCoordinatorList.count > 0, checkbox.isSelected {
            buttonSubmit.backgroundColor = #colorLiteral(red: 0.1058823529, green: 0.5215686275, blue: 0.568627451, alpha: 1)
            buttonSubmit.isEnabled = true
        } else {
            buttonSubmit.backgroundColor = #colorLiteral(red: 0.6862745098, green: 0.6901960784, blue: 0.6901960784, alpha: 1)
            buttonSubmit.isEnabled = false
        }
    }

    fileprivate func updateTextField(_ textfield: INSFormTextField) {
        if textfield.isValidated || textfield.isEditing {
            textfield.updateUIForCorrectInfo()
        } else {
            textfield.updateUIForWrongInfo()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMapViewSegue" {
            if let controller = segue.destination as? INSMapViewController {
                controller.delegate = self
            }
        }
    }
}

extension INSNewActivationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPicker.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return getStringForObject(item: arrayPicker[row])
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOption = arrayPicker[row]
        textfieldSelected?.text = getStringForObject(item: selectedOption)
    }

    func getStringForObject(item: Any?) -> String? {
        if item is INSActivationType {
            let temp = item as! INSActivationType
            return temp.description
        } else if item is INSCustomer {
            let temp = item as! INSCustomer
            return temp.company
        }
        return ""
    }
}

extension INSNewActivationViewController: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == textfieldActivationType {
            if (textfieldSelected == textfieldActivationType) {
                return true
            }
            self.showActivityIndicator()
            textField.text = ""
            textfieldSelected = textField
            dataManager.fetchActivationsType()
            return false
        } else if textField == textfieldCustomer {
            if (textfieldSelected == textfieldCustomer) {
                return true
            }
            self.showActivityIndicator()
            textField.text = ""
            textfieldSelected = textField
            dataManager.fetchCustomers()
            return false
        } else if textField == textfieldDate {
            self.showCalendarAlert()
            return false
        } else if textField == textfieldSede {
            self.performSegue(withIdentifier: "showMapViewSegue", sender: nil)
            return false
        } else {
            textfieldSelected = nil
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        var isValidInfo: Bool = true
        if let text = textField.text {
            textField.text = text.trimmingCharacters(in: .whitespacesAndNewlines)

            switch textField {
            case textfieldActivationName,
                 textfieldActivationType,
                 textfieldCustomer,
                 textfieldProducer,
                 textfieldBrand,
                 textfieldOperator,
                 textfieldDate,
                 textfieldSede,
                 textfieldStartHour,
                 textfieldEndHour:
                isValidInfo = textField.text?.count ?? 0 > INSConstants.FormFieldLength.defaultFieldMinLength
            default:
                isValidInfo = textField.text?.count ?? 0 > INSConstants.FormFieldLength.defaultFieldMinLength
            }
        }

        textField.isValidated = isValidInfo
        updateTextField(textField as! INSFormTextField)
        updateSubmitButton()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textfieldActivationName {
            textfieldActivationName.text = textfieldActivationName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldActivationType.becomeFirstResponder()
        } else if textField == textfieldActivationType {
            textfieldCustomer.becomeFirstResponder()
        } else if textField == textfieldCustomer {
            textfieldProducer.becomeFirstResponder()
        } else if textField == textfieldProducer {
            textfieldBrand.becomeFirstResponder()
        } else if textField == textfieldBrand {
            textfieldSede.becomeFirstResponder()
        } else if textField == textfieldSede {
            textfieldOperator.becomeFirstResponder()
        } else if textField == textfieldOperator {
            textfieldDate.becomeFirstResponder()
        } else if textField == textfieldDate {
            textfieldStartHour.becomeFirstResponder()
        } else if textField == textfieldStartHour {
            textfieldEndHour.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.isValidated {
            updateTextField(textField as! INSFormTextField)
        }
        return true
    }

    func createPickerViewFor(textfield: UITextField) {
        pickerView.reloadAllComponents()
        textfield.becomeFirstResponder()
    }

    private func showCalendarAlert() {
        let viewController:INSCalendarAlertViewController = UIStoryboard(name: "ActivationsDashboard", bundle: nil).instantiateViewController(withIdentifier: "calendarAlert") as! INSCalendarAlertViewController
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.delegate = self
        self.present(viewController, animated: false, completion: nil)
    }
}

extension INSNewActivationViewController: ActivationsDataManagerDelegate {
    func didSuccessForOperation(_ operation: INSConstants.OperationCodes, response: Any?) {
        self.dismissActivityIndicator()
        switch operation {
        case .INS_GET_ACTIVATIONS_TYPE_CODE,
             .INS_GET_CUSTOMERS_CODE:
            arrayPicker = response as! [Any]
            createPickerViewFor(textfield: textfieldSelected!)
            break
        default:
            break
        }
    }

    func didFailForOperation(_ operation: INSConstants.OperationCodes, message: String?) {
        arrayPicker = []
        self.dismissActivityIndicator()
        showAlertControllerWith(message: message)
    }
}

extension INSNewActivationViewController: CalendarDelegate {
    func activationsFor(selectedDates: [Date]) {
        if let _selectedDate = selectedDates.first {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let selectedDate = dateFormatter.string(from: _selectedDate)
            print("selectedDate \(selectedDate)")
            textfieldDate.text = selectedDate
            textFieldDidEndEditing(textfieldDate)
        }
    }
}

extension INSNewActivationViewController {
    //MARK: Keyboard management
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboard = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboard.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}

extension INSNewActivationViewController: FiltersDelegate, CollectionCellSelectedDelegate{
    func search() {
    }
    
    func talentSpecialtyPressed() {
    }
    
    func genderSelected(_ gender: String, role: Role?) {
        applyFilters(gender: gender, role: role!)
    }

    func profileSelected(_ profile: String) {

    }

    func itemSelected(_ item: Any, _ cell: Any) {

    }

    func itemSelected(_ item: Any?, _ isBack:Bool?) {
        guard item != nil else {
            return
        }
        if item is INSTalent {
            if isBack == true {
                selectedBackList.append(item!)
            } else {
                selectedTalentList.append(item!)
            }
        } else if item is INSCoordinator {
            selectedCoordinatorList.append(item!)
        }

        updateSubmitButton()
        tableView.reloadData()
        DispatchQueue.main.async(execute: {
            self.heightTableViewConstraint.constant = self.tableView.contentSize.height
        })
    }

    func itemDeselected(at index: Int, _ item: Any?, _ isBack:Bool?) {
        guard item != nil else {
            return
        }
        if item is INSTalent {
            if isBack == true {
                selectedBackList.remove(at: index)
            } else {
                selectedTalentList.remove(at: index)
            }
        } else if item is INSCoordinator {
            selectedCoordinatorList.remove(at: index)
        }
        updateSubmitButton()
        tableView.reloadData()
        DispatchQueue.main.async(execute: {
            self.heightTableViewConstraint.constant = self.tableView.contentSize.height
        })
    }


    func applyFilters(gender:String? = nil, role:Role) {
        guard gender != nil else { return }
        var tempList: [Any]?

        switch gender {
        case "FEMENINO":
            if role == .talents {
                tempList = talentsArray.filter({$0.genderValid == "Femenino"})
            } else  if role == .coordinators {
                tempList = coordinatorsArray.filter({$0.gender == "Femenino"})
            }
            break
        case "MASCULINO":
            if role == .talents {
                tempList = talentsArray.filter({$0.genderValid == "Masculino"})
            } else  if role == .coordinators {
                tempList = coordinatorsArray.filter({$0.gender == "Masculino"})
            }
            break
        default:
            break
        }

        if role == .talents {
            talentsCollectionView?.allItems = tempList
        } else if role == .coordinators {
            coordinatorsCollectionView?.allItems = tempList
        }

    }
    
    func itemStarPressed(_ item: Any?, _ cell: INSItemWithStarCollectionViewCell) {
    }
}

extension INSNewActivationViewController : LocationDelegate {
    func selected(latitude:Double, longitude:Double) {
        textfieldSede.text = "\(latitude),\(longitude)"
        textFieldDidEndEditing(textfieldSede)
    }
}

extension INSNewActivationViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return selectedTalentList.count + 1
        } else if section == 1 {
            return selectedCoordinatorList.count + 1
        } else if section == 2 {
            return selectedBackList.count + 1
        }
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "costsCell", for: indexPath) as! CostsTableViewCell
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.labelName.text = "TALENTO(S)"
                cell.labelCategory.text = "CATEGORÍA"
                cell.labelCost.text = "PRECIO X HR"
                cell.backgroundColor = INSConstants.ThemeColor.textfieldBackground
            } else {
                let talent = selectedTalentList[indexPath.row - 1] as! INSTalent
                cell.labelName.text = talent.name
                cell.labelCategory.text = ""
                cell.labelCost.text = "$0.00"
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell.labelName.text = "CORDINADORE(S)"
                cell.labelCategory.text = "CATEGORÍA"
                cell.labelCost.text = "PRECIO X HR"
                cell.backgroundColor = INSConstants.ThemeColor.textfieldBackground
            } else {
                let coord = selectedCoordinatorList[indexPath.row - 1 ] as! INSCoordinator
                cell.labelName.text = coord.name
                cell.labelCategory.text = ""
                cell.labelCost.text = "$0.00"
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                cell.labelName.text = "TALENTO(S)"
                cell.labelCategory.text = "CATEGORÍA"
                cell.labelCost.text = "PRECIO X HR"
                cell.backgroundColor = INSConstants.ThemeColor.textfieldBackground
            } else {
                let talent = selectedBackList[indexPath.row - 1] as! INSTalent
                cell.labelName.text = talent.name
                cell.labelCategory.text = ""
                cell.labelCost.text = "$0.00"
            }
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                cell.labelName.text = "COSTO TALENTOS"
                cell.labelCategory.text = "HORAS"
                cell.labelCost.text = "IMPORTE"
                cell.backgroundColor = INSConstants.ThemeColor.textfieldBackground
            } else {
                
            }
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 55.0
        }
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let titleHeader = sections[section]
        let view:INSHeaderTableView = INSHeaderTableView.instanceFromNib() as! INSHeaderTableView
        view.labelTitleHeader.text = titleHeader
        view.labelTitleHeader.textColor = INSConstants.ThemeColor.turquoise
        view.buttonIcon.isHidden = true
        view.backgroundView.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
       
}
