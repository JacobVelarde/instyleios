//
//  INSActivationSelectionViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 09/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSActivationSelectionViewController: UIViewController {
    @IBOutlet weak var earningsButton: UIButton!
    @IBOutlet weak var investmentButton: UIButton!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    internal var selectedActivations: [INSActivation]? {
        didSet {
            resultsTableView.reloadData()
        }
    }
    fileprivate var cellHeight = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        earningsButton.layer.borderColor = INSConstants.ThemeColor.darkTurquoise.cgColor
        investmentButton.layer.borderColor = INSConstants.ThemeColor.darkTurquoise.cgColor
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        
        resultsTableView.register(UINib(nibName: "INSProgressTableViewCell", bundle: Bundle(for: INSProgressTableViewCell.self)), forCellReuseIdentifier: "progressCell")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableViewHeightConstraint.constant = CGFloat(cellHeight * (selectedActivations?.count ?? 0))
        view.layoutIfNeeded()
    }
}

extension INSActivationSelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedActivations?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "progressCell", for: indexPath) as? INSProgressTableViewCell else {
            print("Couldn't dequeue the cell into a INSProgressTableViewCell type")
            return UITableViewCell()
        }
        
        cell.titleLabel.text = "Activación 1"
        cell.rightLabel.text = "$1,000,000.00"
        cell.progressBar.progress = 0.5
        return cell
    }
}

extension INSActivationSelectionViewController: UITableViewDelegate {}
