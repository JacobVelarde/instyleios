//
//  INSActivationDetailViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 15/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSActivationDetailViewController: INSBaseViewController {
    @IBOutlet weak var textfieldEventName: INSFormTextField!
    @IBOutlet weak var textfieldActivationType: INSFormTextField!
    var activationDetail: INSActivation?
    @IBOutlet weak var textfieldCustomer: INSFormTextField!
    @IBOutlet weak var textfieldProducer: INSFormTextField!
    @IBOutlet weak var textfieldBrand: INSFormTextField!
    @IBOutlet weak var textfieldCoordinator: INSFormTextField!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelSchedule: UILabel!
    @IBOutlet weak var collectionTalents: UICollectionView!
    @IBOutlet weak var labelTotalTalents: UILabel!
    @IBOutlet weak var labelCostoPerHourTalent: UILabel!
    @IBOutlet weak var labelHoursTalent: UILabel!
    @IBOutlet weak var labelTotalAmountTalents: UILabel!
    @IBOutlet weak var labelTotalCoordinator: UILabel!
    @IBOutlet weak var labelCostPerHourCoordinator: UILabel!
    @IBOutlet weak var labelHoursCoordinator: UILabel!
    @IBOutlet weak var labelTotalAmountCoordinator: UILabel!
    @IBOutlet weak var labelTotalAmount: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
//    var activeField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        populateData()
    }
    
    func setup() {
        self.navigationController?.isNavigationBarHidden = false
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func populateData() {
        textfieldEventName.text = activationDetail?.eventName
        textfieldActivationType.text = activationDetail?.type
        textfieldCustomer.text = activationDetail?.customer
        textfieldProducer.text = activationDetail?.producer
        textfieldBrand.text = activationDetail?.brand
        textfieldCoordinator.text = getCoodinators()
        labelAddress.text = activationDetail?.location
        labelSchedule.text = getSchedule()
        
        let talentCostHr = ((activationDetail?.talents.first?.costo_hr ?? "0") as NSString).doubleValue
        let coordinatorCostHr = ((activationDetail?.coordinatorCost ?? "0") as NSString).doubleValue

        labelTotalTalents.text = String(format:"%d", activationDetail?.talents.count ?? 0)
        labelCostoPerHourTalent.text = talentCostHr.formatAmount()
        labelHoursTalent.text = activationDetail?.duration ?? "0"
        labelTotalAmountTalents.text = activationDetail?.talentsTotalCost?.formatAmount()

        labelTotalCoordinator.text = String(format:"%d", activationDetail?.totalCoordinators ?? 0)
        labelCostPerHourCoordinator.text = coordinatorCostHr.formatAmount()
        labelHoursCoordinator.text = activationDetail?.duration
        labelTotalAmountCoordinator.text = activationDetail?.coordinatorsTotalCost?.formatAmount()
        labelTotalAmount.text = activationDetail?.totalCostEvent?.formatAmount()
    }
    
    func getSchedule() -> String {
        let eventDate = activationDetail?.eventDate ?? ""
        let startTime = activationDetail?.startTime ?? ""
        let endTime = activationDetail?.endTime ?? ""
        return eventDate + "   " + startTime + " - " + endTime
    }
    
    func getCoodinators() -> String? {
        let string:NSMutableString = ""
        for item in activationDetail?.coordinators ?? []{
            let index = activationDetail?.coordinators.firstIndex(of: item)
            if index == (activationDetail?.coordinators.count ?? 0) - 1{
                string.append(item)
                continue
            }
            string.append(item + ", ")
        }
        return string as String
    }
    
    func enableTextFelds() {
        textfieldEventName.isEnabled = true
        textfieldActivationType.isEnabled = true
        textfieldCustomer.isEnabled = true
        textfieldBrand.isEnabled = true
    }
    
    //MARK: button's IBAction
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func editAction(_ sender: Any) {
        enableTextFelds()
    }
    
    //MARK: Keyboard management
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let keyboard = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboard.height, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0, right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
}

extension INSActivationDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    //MARK: - Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activationDetail?.talents.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "talentCell", for: indexPath) as! INSTalentCollectionViewCell
        cell.talent = activationDetail?.talents[indexPath.row]
        return cell
    }
}

extension INSActivationDetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textfieldEventName {
            textfieldEventName.text = textfieldEventName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldActivationType.becomeFirstResponder()
        } else if textField == textfieldActivationType {
            textfieldActivationType.text = textfieldActivationType.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldCustomer.becomeFirstResponder()
        } else if textField == textfieldCustomer {
            textfieldCustomer.text = textfieldCustomer.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldProducer.becomeFirstResponder()
        } else if textField == textfieldProducer {
            textfieldProducer.text = textfieldProducer.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldBrand.becomeFirstResponder()
        } else if textField == textfieldBrand {
            textfieldBrand.text = textfieldBrand.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            textfieldCoordinator.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
}
