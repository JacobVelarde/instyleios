//
//  INSActivationsParserHandler.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 03/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSActivationsParserHandler {

    internal static func parseActivations(jsonResponse:[String: Any]?) -> INSActivations? {
        guard let responseDict = jsonResponse?[Keys.object] as? [String:Any] else {
            return nil
        }
        return INSActivations.init(jsonResponse: responseDict)
    }

    internal static func parseActivationsByDate(jsonResponse:[String: Any]?) -> INSActivations? {
        guard let responseDict = jsonResponse?[Keys.object] as? [String:Any] else {
            return nil
        }
        return INSActivations.init(jsonResponse: responseDict)
    }

    internal static func parseActivationDetail(jsonResponse:[String: Any]?, forListType: listType, currentActivations: INSActivations?) -> INSActivation? {
        guard let responseDict = jsonResponse?[Keys.object] as? [String:Any] else {
            return nil
        }
        switch forListType {
        case .all:
            if let activation = currentActivations?.activationsList.filter({$0.id == responseDict[Keys.idEvent] as? String}).first {
                activation.updateActivation(activation: responseDict)
                return activation
            }
        case .byDate:
            if let activation = currentActivations?.activationsList.filter({$0.id == responseDict[Keys.idEvent] as? String}).first {
                activation.updateActivation(activation: responseDict)
                return activation
            }
        }
        return nil
    }

    internal static func parseCalendarActivationDays(jsonResponse: [String: Any]) -> [Int]? {
        guard let responseDict = jsonResponse[Keys.object] as? [String:Any],
            let days = responseDict["dias"] as? [Int] else {
            return nil
        }

        return days
    }

    internal static func parseArray(jsonResponse:[String: Any]?, opCode: INSConstants.OperationCodes) -> [Any]? {
        let responseDict = jsonResponse?[Keys.object] as? [Any]
        let responseDict2 = jsonResponse?[Keys.object] as? [String:Any]
        guard responseDict != nil || responseDict2 != nil else {
            return nil
        }
        var array: [Any] = []
        switch opCode {
        case .INS_GET_CUSTOMERS_CODE:
            for item in responseDict ?? [] {
                let customer = INSCustomer.init(json: item as? [String : Any])
                array.append(customer)
            }
        case .INS_GET_ACTIVATIONS_TYPE_CODE:
            for item in responseDict ?? [] {
                let activationType = INSActivationType.init(json: item as? [String : Any])
                array.append(activationType)
            }
        case .INS_GET_TALENTS_CODE,
             .INS_FILTER_TALENTS_CODE:
            for item in responseDict ?? [] {
                let talent = INSTalent.init(jsonResponse: item as? [String : Any])
                array.append(talent)
            }
            break
        case .INS_GET_TALENT_PANEL_CODE,
             .INS_DREAM_TEAM_GET:
            let coordinators = responseDict2?["coordinadores"] as? [Any]
            let talents = responseDict2?["talentos"] as? [Any]
            for item in talents ?? [] {
                let talent = INSTalent.init(jsonResponse: item as? [String : Any])
                array.append(talent)
            }
            for item in coordinators ?? [] {
                let talent = INSCoordinator.init(jsonResponse: item as? [String : Any])
                array.append(talent)
            }
        case .INS_GET_COORDINATORS_CODE,
             .INS_FILTER_COORDINATORS_CODE:
            for item in responseDict ?? [] {
                let talent = INSCoordinator.init(jsonResponse: item as? [String : Any])
                array.append(talent)
            }
            break
        default:
            break
        }
        return array
    }

    internal static func parseProducers(jsonResponse:[String: Any]?) -> [INSProducer]? {
        print("producers \(jsonResponse)")
        guard jsonResponse?.count ?? 0 > 0 else {
            return nil
        }
        return nil
            /*var arrayProducers: [INSProducer] = []
    //        if let responseDict = jsonResponse?[Keys.object] as? [Any] {
            for item in jsonResponse ?? [:]{
                    let producer = INSProducer.init(json: item as? [String : Any])
                    arrayProducers.append(producer)
                }
    //        }
            return arrayProducers*/
        }
}
