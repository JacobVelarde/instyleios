//
//  INSParserHandler.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 21/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

enum listType: Int {
    case all
    case byDate
}

protocol ActivationsDataManagerDelegate: NSObject {
    func didSuccessForOperation(_ operation: INSConstants.OperationCodes, response: Any?)
    func didFailForOperation(_ operation: INSConstants.OperationCodes, message: String?)
}

class INSActivationsDataManager: NSObject{
    public static let activationsDataManager = INSActivationsDataManager()
    private let dataController = INSDataController.sharedDataController
    var activations: INSActivations?
    var activationDetail: INSActivation?
    weak var delegate: ActivationsDataManagerDelegate?
    var activationsByCalendar: INSActivations?

    //MARK: Request's methods
    internal func fetchActivationsFor(activationStatus: String) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_ACTIVATIONS_PANEL_CODE, requestParameters: nil, queryParameters: [Keys.screen: activationStatus], completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    self.activations = INSActivationsParserHandler.parseActivations(jsonResponse: jsonResponse)
                    self.delegate?.didSuccessForOperation(.INS_GET_ACTIVATIONS_PANEL_CODE, response: self.activations)
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_ACTIVATIONS_PANEL_CODE, message: error?.errorMessage)
                }
            }
        })
    }

    func fetchDetailFor(activation: INSActivation, forListType:listType) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_ACTIVATION_DETAIL_CODE, requestParameters: nil, queryParameters: [Keys.idEvent: activation.id ?? ""], completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    if forListType == .all {
                        self.activationDetail = INSActivationsParserHandler.parseActivationDetail(jsonResponse: jsonResponse, forListType: forListType, currentActivations: self.activations)
                    } else if forListType == .byDate {
                        self.activationDetail = INSActivationsParserHandler.parseActivationDetail(jsonResponse: jsonResponse, forListType: forListType, currentActivations: self.activationsByCalendar)
                    }
                    self.delegate?.didSuccessForOperation(.INS_GET_ACTIVATION_DETAIL_CODE, response: self.activationDetail)
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_ACTIVATION_DETAIL_CODE, message: error?.errorMessage)
                }
            }
        })
    }

    internal func fetchActivationsFor(date: String) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_ACTIVATIONS_BY_DATE_CODE, requestParameters: nil, queryParameters: [Keys.yearMonthDay: date], completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    self.activationsByCalendar = INSActivationsParserHandler.parseActivationsByDate(jsonResponse: jsonResponse)
                    self.delegate?.didSuccessForOperation(.INS_GET_ACTIVATIONS_BY_DATE_CODE, response: self.activationsByCalendar?.activationsList)
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_ACTIVATIONS_BY_DATE_CODE, message: error?.errorMessage)
                }
            }
        })
    }

    //MARK: Web services to new activation
    internal func fetchActivationsType() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_ACTIVATIONS_TYPE_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    self.delegate?.didSuccessForOperation(.INS_GET_ACTIVATIONS_TYPE_CODE, response: INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode: .INS_GET_ACTIVATIONS_TYPE_CODE))
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_ACTIVATIONS_TYPE_CODE, message: error?.errorMessage)
                }
            }
        })
    }

    public func fetchActivationsType(completion: @escaping (_ success : Bool, _ activationList: [Any]?) -> Void) {
            let requestDispatcher = INSRequestDispatcher()
            requestDispatcher.dispatchRequest(opCode: .INS_GET_ACTIVATIONS_TYPE_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
                var _success = success
                if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                    _success = true
                }
                DispatchQueue.main.async {
                    if _success {
                        completion(true, INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode: .INS_GET_ACTIVATIONS_TYPE_CODE))
                    } else {
                        completion(false, nil)
                    }
                }
        })
    }
    
    internal func fetchCustomers() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_CUSTOMERS_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    self.delegate?.didSuccessForOperation(.INS_GET_CUSTOMERS_CODE, response: INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode: .INS_GET_CUSTOMERS_CODE))
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_CUSTOMERS_CODE, message: error?.errorMessage)
                }
            }
        })
    }

    internal func fetchProducers() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_PRODUCERS_CODE, requestParameters: ["id_cliente":"13"], completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {                   self.delegate?.didSuccessForOperation(.INS_GET_CUSTOMERS_CODE, response: /*self.parseProducers(jsonResponse: jsonResponse)*/nil)
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_CUSTOMERS_CODE, message: error?.errorMessage)
                }
            }
        })
    }

    internal func fetchCalendarActivationDays(for date: String) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_CALENDAR_ACTIVATION_DAYS_CODE , requestParameters: nil, queryParameters: ["anio_mes": date]) { (success, jsonResponse, response, error) in
            DispatchQueue.main.async {
                if success {
                    let days = INSActivationsParserHandler.parseCalendarActivationDays(jsonResponse: jsonResponse)
                    self.delegate?.didSuccessForOperation(.INS_GET_CALENDAR_ACTIVATION_DAYS_CODE, response: days)
                } else {
                    self.delegate?.didFailForOperation(.INS_GET_CALENDAR_ACTIVATION_DAYS_CODE, message: nil)
                }
            }
        }
    }

    public func fetchTalents(completion: @escaping (_ success : Bool, _ talentList: [INSTalent]?) -> Void) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_TALENTS_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    completion(true, INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode:.INS_GET_TALENTS_CODE) as? [INSTalent])
                } else {
                    completion(false, nil)
                }
            }
        })
    }

    public func fetchCoordinators(completion: @escaping (_ success : Bool, _ coordinatorList: [INSCoordinator]?) -> Void) {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_COORDINATORS_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    completion(true, INSActivationsParserHandler.parseArray(jsonResponse: jsonResponse, opCode:.INS_GET_COORDINATORS_CODE) as? [INSCoordinator])
                } else {
                    completion(false, nil)
                }
            }
        })
    }
}
