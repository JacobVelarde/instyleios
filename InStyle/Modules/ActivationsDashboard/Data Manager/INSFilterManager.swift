//
//  INSFilterManager.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 14/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

protocol FilterDelegate {
    func didSuccessForOperaion(_ operation: WsFilterOperation, response: Any?)
    func didFailForOperation(_ operation: WsFilterOperation, message: String?)
}

enum WsFilterOperation: Int {
    case filterActivations
}

class INSFilterManager {
    struct FilterList {
        var clients: [INSCustomer] = []
        var brands: [INSBrand] = []
        var talents: [INSTalent] = []
        var bookers: [INSBooker] = []
        var coordinators: [INSCoordinator] = []
        var operators: [INSOperator] = []
        var dynamics: [INSDynamic] = []
        var locations: [String?] = []
        var categories: [categoryValue] = []
    }
    
    typealias categoryValue = (id: String, name: String)
    let dataManager = INSDataController.sharedDataController
    var applyingFilters: [String: Any] = [:]
    var filterLists = FilterList()
    var filteredActivations: [INSActivation] = []
    var delegate: FilterDelegate?

    enum FilterOptions: String, CaseIterable {
        case period = "PERIODO"
        case place = "SEDE"
        case brand = "MARCA"
        case dynamic = "DINÁMICA"
        case client = "CLIENTE"
        case talent = "TALENTO"
        case booker = "BOOKER"
        case coordinator = "COORDINADOR"
        case `operator` = "OPERADOR"
    }
    
    internal func filterOptions() -> [String] {
        let allFilters = FilterOptions.allCases
        switch dataManager.user.profile {
        case .admin:
            return allFilters.map { $0.rawValue }
        case .booker:
            return allFilters.filter { $0 != .booker }.map { $0.rawValue }
        case .talent:
            return allFilters.filter {
                $0 != .talent && $0 != .booker && $0 != .coordinator && $0 != .operator
            }.map { $0.rawValue }
        case .coordinator:
            return allFilters.filter {
                $0 != .client && $0 != .booker && $0 != .coordinator
            }.map { $0.rawValue }
        case .producer:
            return allFilters.filter {
                $0 != .client && $0 != .booker
            }.map { $0.rawValue }
        default:
            return []
        }
    }
    
    internal func setDefaultFilters() {
        applyingFilters = ["pe": "-1",
                           "se": "-1",
                           "ma": -1,
                           "di": -1,
                           "cl": -1,
                           "ta": -1,
                           "tac": -1,
                           "bo": -1,
                           "co": -1,
                           "op": "-1"]
    }
    
    fileprivate func parseActivationsNFilters(_ response: [String: Any]) {
        guard let object = response["objeto"] as? [String: Any],
            let activations = object["activaciones"] as? [[String: Any]],
            let _filters = object["filtros"] as? [String: Any] else {
            return
        }
        
        filteredActivations = []
        filterLists = FilterList()
        for activation in activations {
            filteredActivations.append(INSActivation(activation: activation))
        }
        
        filterLists.clients = (_filters["clientes"] as? [[String: Any]] ?? [[:]])
            .map { INSCustomer.init(withResponse: $0) }
        filterLists.brands = (_filters["marcas"] as? [[String: Any]] ?? [[:]])
            .map { INSBrand.init(filerResponse: $0) }
        filterLists.talents = (_filters["talentos"] as? [[String: Any]] ?? [[:]])
            .map { INSTalent.init(filterResponse: $0) }
        filterLists.bookers = (_filters["bookers"] as? [[String: Any]] ?? [[:]])
            .map { INSBooker.init(filterResponse: $0) }
        filterLists.coordinators = (_filters["coordinadores"] as? [[String: Any]] ?? [[:]])
            .map { INSCoordinator.init(filterResponse: $0) }
        filterLists.operators = (_filters["operadores"] as? [[String: Any]] ?? [[:]])
            .map{ INSOperator.init(filterResponse: $0) }
        filterLists.dynamics = (_filters["dinamica"] as? [[String: Any]] ?? [[:]])
            .map { INSDynamic.init(filterResponse: $0) }
        filterLists.locations = (_filters["sede"] as? [[String: String]] ?? [[:]])
            .map { $0["sede"] }
        filterLists.categories = (_filters["categorias"] as? [[String: String]] ?? [[:]])
            .map { categoryValue(id: $0["id"] ?? "", name: $0["nombre"] ?? "" ) }
    }
        
    internal func fetchActivations() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_ADMIN_ACTIVATIONS_CODE, requestParameters: applyingFilters, completionHandler: { (success, jsonResponse, response, error) in
            if success {
                self.parseActivationsNFilters(jsonResponse)
                self.delegate?.didSuccessForOperaion(.filterActivations, response: nil)
            } else {
                self.delegate?.didFailForOperation(.filterActivations, message: jsonResponse["mensaje"] as? String)
            }
            print(jsonResponse)
        })
    }
}
