//
//  INSCalendarViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 15/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import JTAppleCalendar

@objc protocol CalendarDelegate {
    @objc optional func activationsFor(selectedDates: [Date])
    @objc optional func switchingMonth(date: Date?)
}

class INSCalendarViewController: UIViewController {
    enum CalendarType {
        case rangeSelection
        case singleDaySelection
    }
    
    @IBOutlet weak var calendarView: JTACMonthView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var cleanFilterButton: UIButton!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomOptionsViewSection: UIView!
    
    fileprivate var startDate: Date?
    fileprivate var twoDatesAlreadySelected: Bool {
        return startDate != nil && calendarView.selectedDates.count > 1
    }
    fileprivate var dateComponents = Calendar.current.dateComponents([.year, .month], from: Date())
    weak var delegate: CalendarDelegate?
    internal var calendarType: CalendarType = .rangeSelection
    internal var activationDays: [Int]? {
        didSet {
            DispatchQueue.main.async {
                self.calendarView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupCalendar()
        handleMonthNYear()
        filterButton.layer.borderColor = INSConstants.ThemeColor.darkTurquoise.cgColor
        cleanFilterButton.layer.borderColor = INSConstants.ThemeColor.darkTurquoise.cgColor
        bottomOptionsViewSection.isHidden = calendarType == .rangeSelection ? false : true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // set collection view height according to its content
        let calendarHeight = calendarView.collectionViewLayout.collectionViewContentSize.height
        calendarHeightConstraint.constant = calendarHeight
    }
    
    fileprivate func setupCalendar() {
        calendarView.cellSize = 40
        if calendarType == .rangeSelection {
            calendarView.allowsMultipleSelection = true
            calendarView.allowsRangedSelection = true
            let panGensture = UILongPressGestureRecognizer(target: self, action: #selector(didStartRangeSelecting(gesture:)))
            panGensture.minimumPressDuration = 0.5
            calendarView.addGestureRecognizer(panGensture)
        } else {
            calendarView.allowsMultipleSelection = false
            calendarView.allowsRangedSelection = false
            calendarView.selectDates(from: Date(), to: Date())
        }
    }
    
    @objc fileprivate func didStartRangeSelecting(gesture: UILongPressGestureRecognizer) {
        let point = gesture.location(in: gesture.view!)
        let rangeSelectedDates = calendarView.selectedDates
        
        guard let cellState = calendarView.cellStatus(at: point) else { return }
        
        if !rangeSelectedDates.contains(cellState.date) {
            let dateRange = calendarView.generateDateRange(from: rangeSelectedDates.first ?? cellState.date, to: cellState.date)
            calendarView.selectDates(dateRange, keepSelectionIfMultiSelectionAllowed: true)
        } else {
            let calendar = Calendar(identifier: .gregorian)
            let followingDay = calendar.date(byAdding: .day, value: 1, to: cellState.date)!
            calendarView.selectDates(from: followingDay, to: rangeSelectedDates.last!, keepSelectionIfMultiSelectionAllowed: false)
        }
    }
    
    fileprivate func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? INSDateCollectionViewCell  else { return }
        cell.dateLabel.text = cellState.text
        if let days = activationDays, days.count > 0 {
            let day = days.map{ String($0) }.filter { $0 == cellState.text }
            cell.activationIndicatorView.isHidden = day.count > 0 ? false : true
        } else {
            cell.activationIndicatorView.isHidden = true
        }
        handleCellTextColor(cell: cell, cellState: cellState)
        handleCellSelected(cell: cell, cellState: cellState)
    }
        
    fileprivate func handleCellTextColor(cell: INSDateCollectionViewCell, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.isHidden = false
            cell.dateLabel.textColor = INSConstants.ThemeColor.darkGray
        } else {
            cell.dateLabel.isHidden = true
            cell.activationIndicatorView.isHidden = true
        }
    }
    
    fileprivate func handleCellSelected(cell: INSDateCollectionViewCell, cellState: CellState) {
        cell.daySelectionBackgroundView.isHidden = !cellState.isSelected
        cell.dateLabel.textColor = cell.isSelected ? UIColor.white : INSConstants.ThemeColor.darkGray
        cell.activationIndicatorView.backgroundColor = cell.isSelected ? UIColor.white : INSConstants.ThemeColor.darkTurquoise
        
        switch cellState.selectedPosition() {
        case .left:
            cell.daySelectionBackgroundView.layer.cornerRadius = 20
            if #available(iOS 11.0, *) {
                cell.daySelectionBackgroundView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
            } else {
                cell.daySelectionBackgroundView.layer.cornerRadius =  cell.bounds.height/2
            }
        case .middle:
            cell.daySelectionBackgroundView.layer.cornerRadius = 0
            if #available(iOS 11.0, *) {
                cell.daySelectionBackgroundView.layer.maskedCorners = []
            } else {
                cell.daySelectionBackgroundView.layer.cornerRadius =  0
            }
        case .right:
            cell.daySelectionBackgroundView.layer.cornerRadius = 20
            if #available(iOS 11.0, *) {
                cell.daySelectionBackgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
            } else {
                cell.daySelectionBackgroundView.layer.cornerRadius =  cell.bounds.height/2
            }
        case .full:
            cell.daySelectionBackgroundView.layer.cornerRadius = 20
            if #available(iOS 11.0, *) {
                cell.daySelectionBackgroundView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
            } else {
                cell.daySelectionBackgroundView.layer.cornerRadius =  0
            }
        default: break
        }
    }
    
    fileprivate func handleMonthNYear() {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "es_MX")
        formatter.dateFormat = "MMMM yyyy"
        let date = Calendar.current.date(from: dateComponents)!
        let dateFormat = formatter.string(from: date)
        monthLabel.text = dateFormat.components(separatedBy: " ").first?.uppercased()
        let attributedString = NSMutableAttributedString(string: monthLabel.text!)
        attributedString.addWordSpacing(1.5)
        monthLabel.attributedText = attributedString
        yearLabel.text = dateFormat.components(separatedBy: " ").last?.uppercased()
    }
    
    @IBAction func leftScrollAction(_ sender: UIButton) {
        dateComponents.month! -= 1
        handleMonthNYear()
        startDate = nil
        delegate?.switchingMonth?(date: Calendar.current.date(from: dateComponents)!)
        calendarView.reloadData()
    }
    
    @IBAction func rightScrollAction(_ sender: UIButton) {
        dateComponents.month! += 1
        handleMonthNYear()
        startDate = nil
        delegate?.switchingMonth?(date: Calendar.current.date(from: dateComponents)!)
        calendarView.reloadData()
    }
    
    @IBAction func filterAction(_ sender: UIButton) {
        delegate?.activationsFor?(selectedDates: calendarView.selectedDates)
    }
    
    @IBAction func cleanDateSelection(_ sender: UIButton) {
        calendarView.deselectAllDates(triggerSelectionDelegate: true)
        startDate = nil
    }
}

extension INSCalendarViewController: JTACMonthViewDataSource {
    
    internal func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        // get first day of current month and set it as the start date
        let startDate = Calendar.current.date(from: dateComponents)!
        
        // get the last day of current month and set it as the end date
        var endDateComponents = DateComponents()
        endDateComponents.month = 1
        endDateComponents.day = -1
        let endDate = Calendar.current.date(byAdding: endDateComponents, to: startDate)!
        
        return ConfigurationParameters(startDate: startDate, endDate: endDate, generateInDates: .forAllMonths, generateOutDates: .tillEndOfRow)
    }
}

extension INSCalendarViewController: JTACMonthViewDelegate {
    internal func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    internal func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "DateCell", for: indexPath) as! INSDateCollectionViewCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        return cell
    }
    
    internal func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        if calendarType == .singleDaySelection {
            delegate?.activationsFor?(selectedDates: [date])
        } else {
            if startDate != nil {
                calendar.selectDates(from: startDate!, to: date, triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: true)
            } else {
                startDate = date
            }
        }
        configureCell(view: cell, cellState: cellState)
    }
    
    internal func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    internal func calendar(_ calendar: JTACMonthView, shouldSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) -> Bool {
        // ommit selection for indates and outdates
        var excludedDates = calendar.visibleDates().indates.filter { $0.date == date }
        excludedDates.append(contentsOf: calendar.visibleDates().outdates.filter { $0.date == date })
        if excludedDates.isEmpty  {
            if twoDatesAlreadySelected, cellState.selectionType != .programatic || startDate != nil, !calendar.selectedDates.isEmpty, date < calendar.selectedDates[0] {
                startDate = nil
                let retval = !calendar.selectedDates.contains(date)
                calendar.deselectAllDates()
                return retval
            }
            return true
        }
        else {
            startDate = nil
            calendar.deselectAllDates()
            return false
        }
    }
    
    internal func calendar(_ calendar: JTACMonthView, shouldDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState) -> Bool {
        if twoDatesAlreadySelected && cellState.selectionType != .programatic {
            startDate = nil
            calendar.deselectAllDates()
            return false
        }
        return true
    }
}
