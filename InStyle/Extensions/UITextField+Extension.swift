//
//  UITextField+Extension.swift
//  InStyle
//
//  Created by Alejandro López on 1/15/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

fileprivate var isValid: UInt8 = 0
fileprivate var wasAlreadyEdited: UInt8 = 1

extension UITextField {
    
    var isValidated: Bool {
        get {
            guard let value = objc_getAssociatedObject(self, &isValid) as? Bool else {
                return false
            }
            return value
        }
        set {
            objc_setAssociatedObject(self, &isValid, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    var wasEdited: Bool {
        get {
            guard let value = objc_getAssociatedObject(self, &wasAlreadyEdited) as? Bool else {
                return false
            }
            return value
        }
        set {
            objc_setAssociatedObject(self, &wasAlreadyEdited, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func validateEmailFormat() -> Bool {
        let predicateToValueRegex = NSPredicate(format: "SELF MATCHES %@", INSConstants.FormRegex.emailRegex)
        return predicateToValueRegex.evaluate(with: self.text)
    }
    
    func validateFullNameFormat() -> Bool {
        let fullNameTest = NSPredicate(format: "SELF MATCHES %@", INSConstants.FormRegex.fullNameRegex)
        return fullNameTest.evaluate(with: self.text)
    }
    
    func validateNameOrSurnameFormat() -> Bool {
        let fullNameTest = NSPredicate(format: "SELF MATCHES %@", INSConstants.FormRegex.nameOrSurnameRegex)
        return fullNameTest.evaluate(with: self.text)
    }
    
    func validatePhoneNumberFormat() -> Bool {
        let phoneNumberTest = NSPredicate(format: "SELF MATCHES %@", INSConstants.FormRegex.phoneNumberRegex)
        return phoneNumberTest.evaluate(with: self.text)
    }
    
    func validateUrlFormat() -> Bool {
        let urlTest = NSPredicate(format: "SELF MATCHES %@", INSConstants.FormRegex.urlRegex)
        return urlTest.evaluate(with: self.text)
    }
    
    func validateDayOfBirth() -> Bool {
        guard let day = Int(self.text ?? "") else { return false }
        let validDayRange = 1...31
        return validDayRange.contains(day)
    }
    
    func validateMonthOfBirth() -> Bool {
        guard let month = Int(self.text ?? "") else { return false }
        let validMonthange = 1...12
        return validMonthange.contains(month)
    }
    
    func validateYearOfBirth() -> Bool {
        guard let year = Int(self.text ?? "") else { return false }
        let currentYear = Calendar.current.component(.year, from: Date())
        let validYearRange = 1900...(currentYear - 18)
        return validYearRange.contains(year)
    }
        
    var isEmpty: Bool {
        if let text = self.text, text.trimmingCharacters(in: .whitespacesAndNewlines) == "",  !text.isEmpty {
             return false
        }
        return true
    }
    
    var trimmingWhiteSpaces: String {
        return self.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
    
    @IBInspectable public var addTopBorder: Bool {
        set {
            if newValue == true {
                let bottomLine = CALayer()
                bottomLine.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 1)
                bottomLine.backgroundColor = INSConstants.ThemeColor.lightTurquoise.cgColor
                borderStyle = .none
                layer.addSublayer(bottomLine)
            }
        }
        get {
            return self.addTopBorder
        }
    }
    
    @IBInspectable public var addBottomBorder: Bool {
        set {
            if newValue == true{
                let bottomLine = CALayer()
                bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
                bottomLine.backgroundColor = INSConstants.ThemeColor.lightTurquoise.cgColor
                borderStyle = .none
                layer.addSublayer(bottomLine)
            }
        }
        get {
            return self.addBottomBorder
        }
    }
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        //Create UIDatePicker object and assign to inputview
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .date
        self.inputView = datePicker
        
        //Create a toolbar and assign it to inputAccesoryView
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancelar", style: .plain, target: nil, action: #selector(tapOnCancel))
        let done = UIBarButtonItem(title: "Aceptar", style: .plain, target: target, action: selector)
        toolbar.setItems([done, flexibleSpace, cancel], animated: false)
        self.inputAccessoryView = toolbar
    }
    
    @objc fileprivate func tapOnCancel() {
        resignFirstResponder()
    }
}
