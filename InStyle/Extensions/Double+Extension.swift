//
//  Double+Extension.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 09/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

extension Double {
    
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func formatAmount() -> String{
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale.init(identifier: "es_MX")
        let amountString = currencyFormatter.string(from: NSNumber(value: self))!
        return amountString
    }
}
