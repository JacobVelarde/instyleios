//
//  UIImageView+Extension.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 15/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

extension UIImageView {
    func load(urlStr: String) {
        guard let url = URL(string: urlStr) else { return }
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
