//
//  UIViewController+Extension.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 16/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

fileprivate var loadingContainer: UIView?

extension UIViewController {
    
    open func showAlertWith(title:String? = nil, message:String? = nil, imageName:String? = nil, alertType: AlertType? = .imageBelowMessage) {
        let viewController:INSCustomAlertViewController = UIStoryboard(name: "INSCustomAlert", bundle: nil).instantiateViewController(withIdentifier: "customAlert") as! INSCustomAlertViewController
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.initWith(title:title, message: message, imageName: imageName, alertType: alertType)
        self.present(viewController, animated: false, completion: nil)
    }
    
    open func showAlertControllerWith(title: String? = nil, message: String? = nil, actions: [UIAlertAction]? = nil) {
        print("Mensaje \(message ?? "no message")")
        let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
        if let _ = actions {
            for action in actions! {
                alert.addAction(action)
            }
        } else {
            alert.addAction(UIAlertAction(title:"Aceptar", style:.default, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    open func showActivityIndicator() {
        loadingContainer = UIView(frame: self.view.bounds)
        loadingContainer!.center = self.view.center
        loadingContainer!.backgroundColor = .clear
        
        let container = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        container.center = self.view.center
        container.backgroundColor = #colorLiteral(red: 0.3321701288, green: 0.3321786821, blue: 0.3321741223, alpha: 0.658203125)
        container.clipsToBounds = true
        container.layer.cornerRadius = 10
        loadingContainer?.addSubview(container)
        
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.center = CGPoint(x: container.frame.size.width / 2, y: container.frame.size.height / 2)
        container.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        self.navigationController?.view.addSubview(loadingContainer!)
    }
    
    open func dismissActivityIndicator() {
        loadingContainer?.removeFromSuperview()
        loadingContainer = nil
    }
    
    open func isLoaderShowing() -> Bool {
        return loadingContainer != nil
    }
    
    open func getViewControllerWith(identifier:String, storyboard:String) -> UIViewController{
        let viewController = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        return viewController
    }
    
    open func getPreviousController() -> UIViewController? {
        let controllers = self.navigationController?.viewControllers
        guard let _controllers = controllers else {
            return nil
        }
        return _controllers[_controllers.count - 2]
    }
}
