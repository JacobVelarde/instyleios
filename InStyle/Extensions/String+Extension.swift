//
//  String+Extension.swift
//  InStyle
//
//  Created by Alejandro López on 1/12/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

extension String {
    
    func isInteger() -> Bool {
        if let _ = Int(self) {
            return true
        }
        return false
    }
    
    func isDecimalNumber() -> Bool {
        if self.contains("."), Float(self) != nil || Double(self) != nil {
            return true
        }
        return false
    }    
    
    var localized: String {
        return Bundle.main.localizedString(forKey: self, value: "", table: nil)
    }
    
    static var emptyString: String {
        return ""
    }
}
