//
//  NSMutableAttributedString+Extension.swift
//  InStyle
//
//  Created by Alejandro López on 14/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func addLineSpacing(_ _lineSpacing: CGFloat, in range: NSRange? = nil) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = _lineSpacing
        if let _range = range {
            self.addAttribute(.paragraphStyle, value: paragraphStyle, range: _range)
        } else {
            let _range = NSRange(location: 0, length: self.string.count)
            self.addAttribute(.paragraphStyle, value: paragraphStyle, range: _range)
        }
    }
    
    func addWordSpacing(_ wordSpace: CGFloat, in range: NSRange? = nil) {
        if let _range = range {
            self.addAttribute(.kern, value: wordSpace, range: _range)
        } else {
            let _range = NSRange(location: 0, length: self.string.count)
            self.addAttribute(.kern, value: wordSpace, range: _range)
        }
    }
}
