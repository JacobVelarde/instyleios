//
//  NSMutableData+Extension.swift
//  InStyle
//
//  Created by Alejandro López on 18/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

extension NSMutableData {
   
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8) //, allowLossyConversion: true
        append(data!)
    }
}
