//
//  UIWindow+Extension.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 31/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

extension UIWindow {
    public var visibleViewcontroller : UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
           if let nc = vc as? UINavigationController {
               return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
           } else if let tc = vc as? UITabBarController {
               return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
           } else {
               if let pvc = vc?.presentedViewController {
                   return UIWindow.getVisibleViewControllerFrom(vc: pvc)
               } else {
                   return vc
               }
           }
       }
}
