//
//  UIVIew+Extension.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 13/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import Foundation

extension UIView {
    
    open func fadeIn(duration:TimeInterval, delay:TimeInterval, completion:@escaping ((Bool) -> Void) = {(finished : Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {self.alpha = 1.0}, completion: completion)
    }
    
    open func fadeOut(duration:TimeInterval, delay:TimeInterval, completion:@escaping ((Bool) -> Void) = {(finished : Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseOut, animations: {self.alpha = 0.0}, completion: completion)
    }
    
    open func addGesture(target:Any?, action: Selector) {
        let tapGesture = UITapGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(tapGesture)
    }
    
    @IBInspectable public var roundRightCorners: CGFloat {
        set {
            if newValue > 0{
                let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: newValue, height: newValue))
                let mask = CAShapeLayer()
                mask.path = path.cgPath
                self.layer.mask = mask
            }
        }
        get {
            return self.roundRightCorners
        }
    }
}
