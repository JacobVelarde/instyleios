//
//  INSCalendarAlertViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 07/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSCalendarAlertViewController: UIViewController {

    private var calendarController: INSCalendarViewController?
    var delegate: CalendarDelegate?
    private var selectedDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCalendarSegue" {
            if let destinationController = segue.destination as? INSCalendarViewController {
                calendarController = destinationController
                calendarController?.delegate = self
                calendarController?.calendarType = .singleDaySelection
                calendarController?.view.translatesAutoresizingMaskIntoConstraints = false
            }
        }
    }
    
    @IBAction func okAction(_ sender: Any) {
        guard selectedDate != nil else {
            return
        }
        self.dismiss(animated: true, completion: {
            self.delegate?.activationsFor?(selectedDates: [self.selectedDate!])
        })
    }
    
}

extension INSCalendarAlertViewController: CalendarDelegate {
    func activationsFor(selectedDates: [Date]) {
        if let _selectedDate = selectedDates.first {
            selectedDate = _selectedDate
//            let dateFormatter = DateFormatter()
//            dateFormatter.locale = Locale.current
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            selectedDate = dateFormatter.string(from: _selectedDate)
//            print("selectedDate \(selectedDate)")
            
        }
    }
}
