//
//  INSGenericCollectionView.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 11/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

enum SelectionType: Int {
    case MultipleSelection = 0
    case SimpleSelection = 1
}

enum CollectionType: Int {
    case normal = 0
    case withStarts = 1
}

protocol CollectionCellSelectedDelegate: NSObject {
    func itemSelected(_ item: Any?, _ isBack:Bool?)
    func itemDeselected(at index: Int, _ item: Any?, _ isBack:Bool?)
    func itemStarPressed(_ item: Any?, _ cell:INSItemWithStarCollectionViewCell)
}

class INSGenericCollectionView: UIView {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelPageNumber: UILabel!
    weak var delegate: CollectionCellSelectedDelegate?
    var filteredItems: [[Any]] = []
    var items: [Any]?
    private var selectedItems: [Any] = []
    var numberOfPages = 0
    var currentPage = 0
    var selectionType:SelectionType?
    var collectionType:CollectionType?
    var isBack:Bool? = false

    class func instanceFromNib(selectionType:SelectionType? = .SimpleSelection, collectionType:CollectionType? = .normal, isBack:Bool? = false) -> UIView {
        let view = UINib(nibName: "INSGenericCollectionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! INSGenericCollectionView
        view.selectionType = selectionType
        view.collectionType = collectionType
        view.isBack = isBack
        return view
     }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "INSSelectableCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "selectableViewCell")
        collectionView.register(UINib(nibName: "INSItemWithStarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "dreamTeamCell")
         collectionView.heightAnchor.constraint(equalToConstant: 350).isActive = true;
        if selectionType == .SimpleSelection {
            collectionView.allowsMultipleSelection = false
        } else {
            collectionView.allowsMultipleSelection = true
        }
    }
    
    var allItems: [Any]? {
        didSet {
            recalculatePages(allItems)
        }
    }
    
    private func recalculatePages(_ tempList: [Any]?) {
        filteredItems.removeAll()

        if let items = tempList?.count, items > 0 {
            let mod = items % 9
            let numberOfSections = items / 9
            var counter = 0
            if numberOfSections > 0 {
                for _ in 1...numberOfSections {
                    var internalArray:[Any] = []
                    for _ in 1...9 {
                        let object = tempList?[counter]
                        internalArray.append(object!)
                        counter += 1
                    }
                    filteredItems.append(internalArray)
                }
            }
            
            if mod > 0 {
                var internalArray:[Any] = []
                for _ in 1...mod {
                    let object = tempList?[counter]
                    internalArray.append(object!)
                    counter += 1
                }
                filteredItems.append(internalArray)
            }
            
            numberOfPages = filteredItems.count
            labelPageNumber.text = "1"
        } else {
            labelPageNumber.text = "0"
        }
        currentPage = 0
        reloadCollectionTo(page: currentPage)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func leftAction(_ sender: Any) {
        guard let numberPage = Int(labelPageNumber.text ?? "0") else { return  }

        if (numberPage - 1) > 0{
            currentPage -= 1
            reloadCollectionTo(page: currentPage)
            labelPageNumber.text = String(format:"%d", numberPage - 1)
        }

    }
    
    @IBAction func rightAction(_ sender: Any) {
        guard let numberPage = Int(labelPageNumber.text ?? "0") else { return  }
        
        if (numberPage + 1) <= numberOfPages{
            currentPage += 1
            reloadCollectionTo(page: currentPage)
            labelPageNumber.text = String(format:"%d", numberPage + 1)
        }
    }
    
    private func reloadCollectionTo(page:Int) {
        if filteredItems.count <= 0 {
            items = nil
            collectionView.reloadData()
        } else {
            items = filteredItems[page]
            collectionView.reloadData()
        }
    }
}


extension INSGenericCollectionView: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionType == .withStarts {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dreamTeamCell", for: indexPath) as! INSItemWithStarCollectionViewCell
            cell.imageViewPhoto.image = nil
            cell.imageViewPhoto.backgroundColor = INSConstants.ThemeColor.lightGray
            let item = items?[indexPath.row] as Any
            if item is INSTalent ||  item is INSCoordinator{
                let talent = item as? INSUser
                if talent?.image != nil {
                    cell.imageViewPhoto.image = talent?.image
                } else {
                    let requestDispatcher = INSRequestDispatcher()
                    requestDispatcher.downloadData(url: talent?.url_profile, completion: { (success, data)  in
                        if let dataImage = data as? Data, success {
                            talent?.image = UIImage(data: dataImage)
                            cell.imageViewPhoto.image = talent?.image
                        }else {
                            cell.imageViewPhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                        }
                    })
                }
                cell.labelName.text = talent?.name?.uppercased()
                cell.photoTapAction = {(_) in
                    self.delegate?.itemSelected(item, self.isBack)
                }
                cell.starTapAction = {(_) in
                    self.delegate?.itemStarPressed(item, cell)
                }
                if talent?.isInTeam == true {
                    cell.changeImage = true
                } else {
                    cell.changeImage = false
                }
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectableViewCell", for: indexPath) as! INSSelectableCollectionViewCell
            cell.imagePhoto.image = nil
            cell.imagePhoto.backgroundColor = INSConstants.ThemeColor.lightGray
            let item = items?[indexPath.row] as Any
            if item is INSTalent {
                let talent = item as? INSTalent
                if talent?.image != nil {
                    cell.imagePhoto.image = talent?.image
                } else {
                    let requestDispatcher = INSRequestDispatcher()
                    requestDispatcher.downloadData(url: talent?.url_profile, completion: { (success, data)  in
                        if let dataImage = data as? Data, success {
                            talent?.image = UIImage(data: dataImage)
                            cell.imagePhoto.image = talent?.image
                        }else {
                            cell.imagePhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                        }
                    })
                }
                cell.labelName.text = talent?.name?.uppercased()
                cell.tapAction = {(_) in
                    self.manageSelectedItem(talent!, cell: cell)
                }
                if selectedItems.contains(where: {($0 as! INSTalent).idUser == talent!.idUser}) {
                    cell.changeImage = true
                } else {
                    cell.changeImage = false
                }
                return cell
            } else if item is INSCoordinator {
                let coordinator = item as! INSCoordinator
                if coordinator.image != nil {
                    cell.imagePhoto.image = coordinator.image
                } else {
                    let requestDispatcher = INSRequestDispatcher()
                    requestDispatcher.downloadData(url: coordinator.url_profile, completion: { (success, data)  in
                        if success, let dataImage = data as? Data {
                            coordinator.image = UIImage(data: dataImage)
                            cell.imagePhoto.image = coordinator.image
                        } else {
                            cell.imagePhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                        }
                    })
                }
                cell.labelName.text = coordinator.name?.uppercased()
                cell.tapAction = {(_) in
                    self.manageSelectedItem(coordinator, cell: cell)
                }
                if selectedItems.contains(where: {($0 as! INSCoordinator).idUser == coordinator.idUser}) {
                    cell.changeImage = true
                } else {
                    cell.changeImage = false
                }
                return cell
            }
            return cell
        }
    }
    
    func manageSelectedItem(_ item: Any?, cell: INSSelectableCollectionViewCell) {
        guard item != nil else {
            return
        }
        if selectionType == .SimpleSelection {
            delegate?.itemSelected(item, isBack)
        } else {
            var add = false
            var _index = -1
            if item is INSTalent {
                let talent = item as! INSTalent
                if let index = selectedItems.firstIndex(where: {($0 as! INSTalent).idUser == talent.idUser}) {
                    _index = index
                } else {
                    add = true
                }
            } else if item is INSCoordinator {
                let coordinator = item as! INSCoordinator
                if let index = selectedItems.firstIndex(where: {($0 as! INSCoordinator).idUser == coordinator.idUser}) {
                    _index = index
                } else {
                    add = true
                }
            }
            
            if add == true {
                selectedItems.append(item!)
                cell.changeImage = true
                delegate?.itemSelected(item, isBack)
            } else {
                selectedItems.remove(at: _index)
                cell.changeImage = false
                delegate?.itemDeselected(at: _index, item, isBack)
            }
        }
    }
}

extension INSGenericCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width - 20) / 3, height: 110)
    }
}

//extension INSGenericCollectionView {
//func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
//        URLSession.shared.dataTask(with: url) {
//            (data, response, error) in
//            completion(data, response, error)
//            }.resume()
//    }
//
//    func downloadImage(urlStr: String?, completion: @escaping (_ success : Bool, _ image: UIImage?) -> Void) {
//        let url = URL(string: urlStr ?? "") ?? nil
//        guard url != nil else {
//            completion(false, nil)
//            return
//        }
//        getDataFromUrl(url: url!) { (data, response, error)  in
//            guard let data = data, error == nil else {
//                completion(false, nil)
//                return
//            }
//            DispatchQueue.main.async() { () -> Void in
//                if let image = UIImage(data: data) {
//                    completion(true, image)
//                } else {
//                    completion(false, nil)
//                }
//            }
//        }
//    }
//}
