//
//  INSButton.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 05/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

@IBDesignable class INSButton: UIButton {
    public var buttonAction:((_ sender: INSButton) -> Void)?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    public required init?(coder adecoder: NSCoder) {
        super.init(coder: adecoder)
        configure()
    }
    
    private func configure() {
        changeColorToNoSelected()
        self.addTarget(self, action: #selector(touchInside(_:)), for: .touchUpInside)
    }
    
    @objc func touchInside(_ sender: INSButton) {
        self.selectedBtn = true
        print("Me seleccionaron")
        buttonAction?(self)
    }
    
    func changeColorToSelected() {
        self.backgroundColor = INSConstants.ThemeColor.turquoise
        self.setTitleColor(.white, for: .normal)
    }
    
    func changeColorToNoSelected() {
        self.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        self.backgroundColor = .white
        self.setTitleColor(INSConstants.ThemeColor.turquoise, for: .normal)
    }
    
    @IBInspectable public var selectedBtn : Bool = false{
        didSet {
            if selectedBtn == true {
                changeColorToSelected()
            } else {
                changeColorToNoSelected()
            }
        }
    }
}
