//
//  INSCollectionContainerView.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 25/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

protocol ListSelectedDelegate: NSObject {
    func itemSeleted(_ item: Any?)
    func itemDeselected(at index: Int, _ item: Any?)
}

class INSCollectionContainerView: UIView {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonMale: INSButton!
    @IBOutlet weak var buttonFemale: INSButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var buttonLeft: UIButton!
    @IBOutlet weak var buttonRight: UIButton!
    @IBOutlet weak var labelPage: UILabel!
    weak var delegate: ListSelectedDelegate?
    
    var _allItems: [Any]?
    var filteredItems: [[Any]] = []
    private var selectedItems: [Any] = []
    var numberOfPages = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "INSSelectableCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "selectableViewCell")
         collectionView.heightAnchor.constraint(equalToConstant: 350).isActive = true;
        collectionView.allowsMultipleSelection = true
        collectionView.isPagingEnabled = true
        
        buttonFemale.buttonAction = { (_ sender : INSButton) in
            self.buttonAction(sender)
        }
        
        buttonMale.buttonAction = { (_ sender : INSButton) in
            self.buttonAction(sender)
        }
    }
    
    var allItems: [Any]? {
        set {
            _allItems = newValue
            recalculatePages(_allItems)
        }
        get {
            _allItems
        }
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "INSCollectionContainerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
     }
    
    func buttonAction(_ sender: UIButton) {
        var tempList: [Any]?

        switch sender.tag {
        case 100:
            if _allItems is [INSTalent] {
                tempList = _allItems?.filter({($0 as! INSTalent).genderValid == "Femenino"})
            } else if _allItems is [INSCoordinator] {
                tempList = _allItems?.filter({($0 as! INSCoordinator).gender == "Femenino"})
            }
            buttonMale.selectedBtn = false
            break
        case 101:
            if _allItems is [INSTalent] {
                tempList = _allItems?.filter({($0 as! INSTalent).genderValid == "Masculino"})
            } else if _allItems is [INSCoordinator] {
                tempList = _allItems?.filter({($0 as! INSCoordinator).gender == "Masculino"})
            }
            buttonFemale.selectedBtn = false
            break
        default:
            break
        }
        recalculatePages(tempList)
        collectionView.reloadData()
    }
    
    func recalculatePages(_ tempList: [Any]?) {
        filteredItems.removeAll()
        scrollToInitialFrame()

        if let items = tempList?.count, items > 0 {
            let mod = items % 9
            let numberOfSections = items / 9
            var counter = 0
            if numberOfSections > 0 {
                for _ in 1...numberOfSections {
                    var internalArray:[Any] = []
                    for _ in 1...9 {
                        let object = tempList?[counter]
                        internalArray.append(object!)
                        counter += 1
                    }
                    filteredItems.append(internalArray)
                }
            }
            
            if mod > 0 {
                var internalArray:[Any] = []
                for _ in 1...mod {
                    let object = tempList?[counter]
                    internalArray.append(object!)
                    counter += 1
                }
                filteredItems.append(internalArray)
            }
            
            numberOfPages = filteredItems.count
            labelPage.text = "1"
        } else {
            labelPage.text = "0"
        }
    }
    
    @IBAction func leftAction(_ sender: Any) {
        guard let numberPage = Int(labelPage.text ?? "0") else { return  }

        if (numberPage - 1) > 0{
            scrollToPreviousItem()
            labelPage.text = String(format:"%d", numberPage - 1)
        }
    }
    
    @IBAction func rightAction(_ sender: Any) {
        guard let numberPage = Int(labelPage.text ?? "0") else { return  }
        
        if (numberPage + 1) <= numberOfPages{
            scrollToNextItem()
            labelPage.text = String(format:"%d", numberPage + 1)
        }
    }
}

extension INSCollectionContainerView: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return filteredItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectableViewCell", for: indexPath) as! INSSelectableCollectionViewCell
        
        if indexPath.row < filteredItems[indexPath.section].count {
            let item = filteredItems[indexPath.section][indexPath.row] as Any
            if item is INSTalent {
                let talent = item as? INSTalent
                if talent?.image != nil {
                    cell.imagePhoto.image = talent?.image
                } else {
                    cell.imagePhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                }
                cell.labelName.text = talent?.name?.uppercased()
                cell.tapAction = {(_) in
                    self.manageSelectedItem(talent!, cell: cell)
                }
                if selectedItems.contains(where: {($0 as! INSTalent).idUser == talent!.idUser}) {
                    cell.changeImage = true
                } else {
                    cell.changeImage = false
                }
                return cell
            } else if item is INSCoordinator {
                let coordinator = item as! INSCoordinator
                if coordinator.image != nil {
                    cell.imagePhoto.image = coordinator.image
                } else {
                    cell.imagePhoto.backgroundColor = INSConstants.ThemeColor.lightGray
                }
                cell.labelName.text = coordinator.name?.uppercased()
                cell.tapAction = {(_) in
                    self.manageSelectedItem(coordinator, cell: cell)
                }
                if selectedItems.contains(where: {($0 as! INSCoordinator).idUser == coordinator.idUser}) {
                    cell.changeImage = true
                } else {
                    cell.changeImage = false
                }
                return cell
            }
        }else{
            cell.imagePhoto.image = nil
            cell.labelName.text = nil
            cell.changeImage = false
            cell.tapAction = nil
            cell.imagePhoto.backgroundColor = .clear

        }
        return cell
    }
    
    func manageSelectedItem(_ item: Any?, cell: INSSelectableCollectionViewCell) {
        guard item != nil else {
            return
        }
        var add = false
        var _index = -1
        if item is INSTalent {
            let talent = item as! INSTalent
            if let index = selectedItems.firstIndex(where: {($0 as! INSTalent).idUser == talent.idUser}) {
                _index = index
            } else {
                add = true
            }
        } else if item is INSCoordinator {
            let coordinator = item as! INSCoordinator
            if let index = selectedItems.firstIndex(where: {($0 as! INSCoordinator).idUser == coordinator.idUser}) {
                _index = index
            } else {
                add = true
            }
        }
        
        if add == true {
            selectedItems.append(item!)
            cell.changeImage = true
            delegate?.itemSeleted(item)
        } else {
            selectedItems.remove(at: _index)
            cell.changeImage = false
            delegate?.itemDeselected(at: _index, item)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = collectionView.frame.size.width
        var current = collectionView.contentOffset.x / pageWidth
        current += 1
        labelPage.text = String(format:"%d", Int(current))
    }
}

extension INSCollectionContainerView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width - 20) / 3, height: 110)
    }
}

extension INSCollectionContainerView {

    func scrollToNextItem() {
        let scrollOffset = CGFloat(floor(collectionView.contentOffset.x + collectionView.frame.size.width))
        self.scrollToFrame(scrollOffset: scrollOffset)
    }

    func scrollToPreviousItem() {
        let scrollOffset = CGFloat(floor(collectionView.contentOffset.x - collectionView.bounds.size.width))
        self.scrollToFrame(scrollOffset: scrollOffset)
    }

    func scrollToFrame(scrollOffset : CGFloat) {
        collectionView.setContentOffset(CGPoint(x: scrollOffset, y: collectionView.contentOffset.y), animated: false)
    }
    
    func scrollToInitialFrame() {
        collectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
}
