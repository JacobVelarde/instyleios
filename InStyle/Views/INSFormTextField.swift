//
//  INSFormTextField.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 26/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSFormTextField: UITextField {
    fileprivate var textfieldInsets = UIEdgeInsets(top: 3, left: 10, bottom: 3, right: 0)
    fileprivate var hidePassword: Bool = true
    
    @IBInspectable public var customPasswordField: Bool = false {
        didSet {
            if self.rightView == nil {
                let view = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: self.bounds.size.height * 0.65))
                let buttonIcon = UIButton(frame: view.frame)
                buttonIcon.setImage(#imageLiteral(resourceName: "eyeIcon"), for: .normal)
                buttonIcon.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
                buttonIcon.addTarget(self, action: #selector(showHidePassword(_:)), for: .touchUpInside)
                view.addSubview(buttonIcon)
                self.rightView = view
            }
            self.hidePassword = self.customPasswordField ? false : true
            self.rightViewMode = self.customPasswordField ? .always : .never
        }
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        if self.textAlignment != NSTextAlignment.center {
            return bounds.inset(by: textfieldInsets)
        }
        return bounds
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        if self.textAlignment != NSTextAlignment.center {
            return bounds.inset(by: textfieldInsets)
        }
        return bounds
    }
    
    func updateUIForWrongInfo() {
        self.layer.borderWidth = 0.5
        self.layer.borderColor = INSConstants.ThemeColor.textfieldErrorBorder.cgColor
        self.backgroundColor = INSConstants.ThemeColor.textfieldErrorBackground
    }
    
    func updateUIForCorrectInfo() {
        self.layer.borderWidth = 0
        self.backgroundColor = INSConstants.ThemeColor.textfieldBackground
    }
    
    @objc fileprivate func showHidePassword(_ sender: UIButton) {
        self.isSecureTextEntry = hidePassword
        hidePassword = !hidePassword
    }
    
    @IBInspectable public var insetLeft: CGFloat = 10 {
        didSet {
            textfieldInsets.left = self.insetLeft
        }
    }
}

