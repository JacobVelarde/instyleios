//
//  INSGenericFilterCollectionView.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 12/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

enum Role: Int {
    case coordinators
    case talents
}

protocol FiltersDelegate {
    func genderSelected(_ gender: String, role:Role?)
    func profileSelected(_ profile: String)
    func itemSelected(_ item: Any, _ cell: Any)
    func search()
    func talentSpecialtyPressed()
}

class INSGenericFilterCollectionView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var stackViewProfile: UIStackView!
    @IBOutlet weak var searchFilterCollectionView: UICollectionView!
    @IBOutlet weak var buttonTalents: INSButton!
    @IBOutlet weak var buttonCoordinators: INSButton!
    @IBOutlet weak var stackViewGender: UIStackView!
    @IBOutlet weak var buttonFemale: INSButton!
    @IBOutlet weak var buttonMale: INSButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonSearch: UIButton!
    @IBOutlet weak var stackViewTalentSpecialty: UIStackView!
    @IBOutlet weak var buttonTalentSpecialty: INSButton!
    
    var _filterOptions: [ItemFilter]?
    var delegate: FiltersDelegate?
    var role: Role?

    class func instanceFromNib(delegate:FiltersDelegate, role:Role? = .talents) -> UIView {
        let view = UINib(nibName: "INSGenericFilterCollectionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! INSGenericFilterCollectionView
        view.delegate = delegate
        view.role = role
        return view
     }

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        setupFunctionalityComponents()
    }
    
    private func setup() {
        hideSpecialtyStack(value: true)
        searchFilterCollectionView.delegate = self
        searchFilterCollectionView.dataSource = self
        searchFilterCollectionView.register(UINib(nibName: "INSFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "filterCell")
    }
    
    private func setupFunctionalityComponents() {
        buttonMale.buttonAction = { (_ sender: INSButton) in
            self.buttonFemale.selectedBtn = false
            self.delegate?.genderSelected("MASCULINO", role: self.role)
        }
        
        buttonFemale.buttonAction = {(_ sender: INSButton) in
            self.buttonMale.selectedBtn = false
            self.delegate?.genderSelected("FEMENINO", role: self.role)
        }
        
        buttonTalents.buttonAction = {(_ sender: INSButton) in
            self.buttonCoordinators.selectedBtn = false
            self.delegate?.profileSelected("TALENTOS")

        }
        
        buttonCoordinators.buttonAction = {(_ sender: INSButton) in
            self.buttonTalents.selectedBtn = false
            self.delegate?.profileSelected("COORDINADORES")
        }
        
        buttonTalentSpecialty.buttonAction = {(_ sender: INSButton) in
            self.delegate?.talentSpecialtyPressed()
        }
    }
    
    var filterOptions: [ItemFilter]? {
        didSet {
            _filterOptions = filterOptions
            if _filterOptions?.count ?? 0 > 0 {
                searchFilterCollectionView.isHidden = false
            } else {
                searchFilterCollectionView.isHidden = true
            }
            searchFilterCollectionView.reloadData()
            searchFilterCollectionView.layoutIfNeeded()
            heightConstraint.constant = searchFilterCollectionView.contentSize.height
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        searchFilterCollectionView.reloadData()
    }
            
    var showGenderFilter: Bool = false {
        didSet {
            stackViewGender.isHidden = !showGenderFilter
        }
    }
    
    var showProfileFilter: Bool = false {
        didSet {
            stackViewProfile.isHidden = !showProfileFilter
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterOptions?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath) as? INSFilterCollectionViewCell {
            let itemFilter = filterOptions?[indexPath.row]
            cell.buttonOption.setTitle(itemFilter?.name, for: .normal)
            cell.buttonOption.tag = indexPath.row
            cell.buttonAction = {(_) in
                self.delegate?.itemSelected(itemFilter!, cell)
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func updateFilterItem(_ item:ItemFilter?, _ cell:INSFilterCollectionViewCell?) {
        guard item != nil else {
            return
        }
        cell?.buttonOption.setTitle(item?.name, for: .normal)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        delegate?.search()
    }
}

extension INSGenericFilterCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: (searchFilterCollectionView.bounds.size.width - 20) / 3, height: 30)
        return size
    }
}

extension INSGenericFilterCollectionView {
    func hideSpecialtyStack(value: Bool) {
        stackViewTalentSpecialty.isHidden = value
        self.buttonTalentSpecialty.selectedBtn = false
        self.buttonTalentSpecialty.setTitle("CATEGORÍA", for: .normal)

    }
    
    func hideGenderStack(value: Bool) {
        stackViewGender.isHidden = value
        self.buttonFemale.selectedBtn = false
        self.buttonMale.selectedBtn = false
    }
}
