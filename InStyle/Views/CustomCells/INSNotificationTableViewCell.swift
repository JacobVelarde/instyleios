//
//  INSNotificationTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 24/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonGoDetail: UIButton!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var buttonEye: UIButton!

    public var buttonAction : ((_ sender: UIButton) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUp()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUp() {
        buttonGoDetail.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
        buttonGoDetail.addTarget(self, action: #selector(touchUpInside), for: UIControl.Event.touchUpInside)
        buttonGoDetail.addTarget(self, action: #selector(touchUpInsideEyeButton), for: UIControl.Event.touchUpInside)
    }
    
    @objc func touchUpInside(sender: UIButton){
        self.buttonAction?(buttonGoDetail)
    }
    
    @objc func touchUpInsideEyeButton(sender: UIButton){
        self.buttonAction?(buttonEye)
    }
}
