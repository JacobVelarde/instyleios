//
//  INSHeaderTableView.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 13/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSHeaderTableView: UIView {
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var labelTitleHeader: UILabel!
    @IBOutlet weak var buttonIcon: UIButton!
    @IBOutlet weak var buttonWidthConstraimt: NSLayoutConstraint!
    public var buttonAction : ((_ sender: UIButton) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonIcon.isHidden = true
        buttonIcon.imageView?.contentMode = .scaleAspectFit
        let attributedText = NSMutableAttributedString(string: labelTitleHeader.text!)
        attributedText.addWordSpacing(1.5)
        labelTitleHeader.attributedText = attributedText
        buttonIcon.addTarget(self, action: #selector(pressAction), for: .touchUpInside)

    }

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "INSHeaderTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

    @objc func pressAction(_ sender: Any) {
        buttonAction?(buttonIcon)
    }
}
