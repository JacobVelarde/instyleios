//
//  INSSelectableCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 25/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSSelectableCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageSelectable: UIImageView!
    @IBOutlet weak var imagePhoto: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    public var tapAction : ((_ sender: UICollectionViewCell) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:))))
    }
    
    @objc func tapGesture(_ sender: UITapGestureRecognizer) {
        self.tapAction?(self)
    }

    var changeImage: Bool? {
        didSet {
            if changeImage == true{
                imageSelectable.image = UIImage(named: "selectedIcon")
            } else {
                imageSelectable.image = nil
            }
        }
    }
}
