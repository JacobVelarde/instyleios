//
//  INSSliderTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López on 1/11/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSSliderTableViewCell: UITableViewCell {
    static let sliderChangeNotificationName = Notification.Name(rawValue: "sliderChangeNotificationName")

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMaxValue: UILabel!
    @IBOutlet weak var labelMaxValueConverted: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var labelMinValue: UILabel!
    @IBOutlet weak var labelMinValueConverted: UILabel!
    @IBOutlet weak var stackViewMinNMaxConvertedValues: UIStackView!
    internal var info: SliderInfo? = nil
    
    enum SliderTemplate: Int {
        case age, height, shirtSize, coatSize, pantsSize, bustSize, waistSize, hipSize, weight, footwearFemale, footwearMale
    }
    
    struct SliderInfo {
        let title: String
        var unitMeasurement: String?
        var unitMeasurementConversion: String?
        var minimumValue: Float
        var maximumValue: Float
        var incrementsBy: String
        var sliderTemplate: SliderTemplate?
        
        init(title: String, unitMeasurement: String? = nil, unitMeasurementConversion: String? = nil, minimumValue: Float, maximumValue: Float, incrementsBy: String = "1", sliderTemplate: SliderTemplate? = nil) {
            self.title = title
            self.unitMeasurement = unitMeasurement
            self.unitMeasurementConversion = unitMeasurementConversion
            self.minimumValue = minimumValue
            self.maximumValue = maximumValue
            self.incrementsBy = incrementsBy
            self.sliderTemplate = sliderTemplate
        }
        
        init(sliderTemplate: SliderTemplate) {
            switch sliderTemplate {
            case .age:
                self.init(title: "EDAD", minimumValue: 18, maximumValue: 35, sliderTemplate: sliderTemplate)
            case .height:
                self.init(title: "ALTURA", unitMeasurement: "m", unitMeasurementConversion: "ft in", minimumValue: 1.60, maximumValue: 2, incrementsBy: "0.01", sliderTemplate: sliderTemplate)
            case .shirtSize:
                self.init(title: "CAMISA", unitMeasurement: "cm", unitMeasurementConversion: "in",  minimumValue: 0, maximumValue: 140, incrementsBy: "0.1", sliderTemplate: sliderTemplate)
            case .coatSize:
                self.init(title: "SACO", unitMeasurement: "cm", unitMeasurementConversion: "in", minimumValue: 0, maximumValue: 140, incrementsBy: "0.1", sliderTemplate: sliderTemplate)
            case .pantsSize:
                self.init(title: "PANTALÓN", unitMeasurement: "cm", unitMeasurementConversion: "in", minimumValue: 0, maximumValue: 140, incrementsBy: "0.1", sliderTemplate: sliderTemplate)
            case .bustSize:
                self.init(title: "BUSTO", unitMeasurement: "cm", unitMeasurementConversion: "in", minimumValue: 0, maximumValue: 140, incrementsBy: "0.1", sliderTemplate: sliderTemplate)
            case .waistSize:
                self.init(title: "CINTURA", unitMeasurement: "cm", unitMeasurementConversion: "in", minimumValue: 0, maximumValue: 140, incrementsBy: "0.1", sliderTemplate: sliderTemplate)
            case .hipSize:
                self.init(title: "CADERA", unitMeasurement: "cm", unitMeasurementConversion: "in", minimumValue: 0, maximumValue: 140, incrementsBy: "0.1", sliderTemplate: sliderTemplate)
            case .weight:
                self.init(title: "PESO", unitMeasurement: "kg", unitMeasurementConversion: "lb", minimumValue: 45, maximumValue: 110, sliderTemplate: sliderTemplate)
            case .footwearFemale:
                self.init(title: "CALZADO", unitMeasurement: "cm", unitMeasurementConversion: "", minimumValue: 22, maximumValue: 27, incrementsBy: "0.5", sliderTemplate: sliderTemplate)
            case .footwearMale:
                self.init(title: "CALZADO", unitMeasurement: "cm", unitMeasurementConversion: "", minimumValue: 25, maximumValue: 31, incrementsBy: "0.5", sliderTemplate: sliderTemplate)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(_ template: SliderInfo, currentValue: Any?) {
        self.info = template
        slider.minimumValue = template.minimumValue
        slider.maximumValue = template.maximumValue
        slider.value = template.minimumValue
        if let integerValue = currentValue as? Int {
            slider.value = Float(integerValue)
        } else if let floatValue = currentValue as? Float {
            slider.value = floatValue
        }
        labelTitle.text = template.title
        updateUI(with: template)
    }
    
    fileprivate func updateUI(with info: SliderInfo) {
        if info.incrementsBy.isDecimalNumber(), let decimals = info.incrementsBy.components(separatedBy: ".").last {
            labelMinValue.text = "\(String(format: "%.\(decimals.count)f", slider.value)) \(info.unitMeasurement ?? "")"
            labelMaxValue.text = "\(String(format: "%.\(decimals.count)f", slider.maximumValue)) \(info.unitMeasurement ?? "")"

            if let convertedUnit = info.unitMeasurementConversion {
                var minValue: Any?
                var maxValue: Any?
                
                if let template = info.sliderTemplate, template == .footwearMale {
                    minValue = INSUtility.convertMaleShoeSize(slider.value, from: "MX", to: "US")
                    maxValue = INSUtility.convertMaleShoeSize(slider.maximumValue, from: "MX", to: "US")
                } else if let template = info.sliderTemplate, template == .footwearFemale {
                    minValue = INSUtility.convertFemaleShoeSize(slider.value, from: "MX", to: "US")
                    maxValue = INSUtility.convertFemaleShoeSize(slider.maximumValue, from: "MX", to: "US")
                } else if let template = info.sliderTemplate, template == .height {
                    minValue = INSUtility.convertHeight(slider.value, fromUnit: info.unitMeasurement ?? "", toUnit: convertedUnit)
                    maxValue = INSUtility.convertHeight(slider.maximumValue, fromUnit: info.unitMeasurement ?? "", toUnit: convertedUnit)
                } else {
                    minValue = INSUtility.convertValue(slider.value, fromUnit: info.unitMeasurement ?? "", toUnit: convertedUnit)
                    maxValue = INSUtility.convertValue(slider.maximumValue, fromUnit: info.unitMeasurement ?? "", toUnit: convertedUnit)
                }

                if let min = minValue as? Float, let max = maxValue as? Float {
                    labelMinValueConverted.text = "\(String(format: "%.\(decimals.count)f", min)) \(convertedUnit)"
                    labelMaxValueConverted.text = "\(String(format: "%.\(decimals.count)f", max)) \(convertedUnit)"
                } else if let min = minValue as? (Int, Int), let max = maxValue as? (Int, Int) {
                    labelMinValueConverted.text = "\(String(min.0))'\(String(min.1))''"
                    labelMaxValueConverted.text = "\(String(max.0))'\(String(max.1))''"
                }
            }
        } else if info.incrementsBy.isInteger() {
            labelMinValue.text = "\(String(Int(slider.value))) \(info.unitMeasurement ?? "")"
            labelMaxValue.text = "\(String(Int(slider.maximumValue))) \(info.unitMeasurement ?? "")"
            
            if let convertedUnit = info.unitMeasurementConversion {
                let minValue = INSUtility.convertValue(Int(slider.value), fromUnit: info.unitMeasurement ?? "", toUnit: convertedUnit)
                let maxValue = INSUtility.convertValue(Int(slider.maximumValue), fromUnit: info.unitMeasurement ?? "", toUnit: convertedUnit)
                
                if let min = minValue as? Float, let max = maxValue as? Float {
                    labelMinValueConverted.text = "\(String(format: "%.2f", min)) \(convertedUnit)"
                    labelMaxValueConverted.text = "\(String(format: "%.2f", max)) \(convertedUnit)"
                }
            }
        }
        
        stackViewMinNMaxConvertedValues.isHidden = info.unitMeasurementConversion != nil ? false : true
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        guard let currentInfo = info else { return }
        if let incrementer = Float(currentInfo.incrementsBy) {
            sender.value = roundf(sender.value / incrementer) * incrementer
            NotificationCenter.default.post(name: INSSliderTableViewCell.sliderChangeNotificationName, object: sender)
        }
        updateUI(with: currentInfo)
    }
}
