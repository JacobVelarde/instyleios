//
//  INSAreaTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 19/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSAreaTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var title: UILabel!
    var selectedCell : ((_ sender: UIImageView) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let gestureR = UIGestureRecognizer(target: self, action: #selector(pressAction))
        self.addGestureRecognizer(gestureR)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var item:AreaItems? {
        didSet {
            title.attributedText = item?.title
            imgView.image = UIImage(named: item?.image ?? "")
        }
    }
    
    @objc func pressAction() {
        selectedCell?(imgView)
    }
}

struct AreaItems {
    var title: NSAttributedString?
    var image: String?
    var segue: String?
    
    public init(_ title: NSAttributedString, image: String, segueId: String) {
        self.title = title
        self.image = image
        self.segue = segueId
    }
}
