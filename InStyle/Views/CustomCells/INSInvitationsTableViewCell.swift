//
//  INSInvitationsTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 13/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSInvitationsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonAccept: UIButton!
    @IBOutlet weak var buttonRemove: UIButton!
    var removeAction:((_ sender:UIButton) -> Void)?
    var acceptAction:((_ sender:UIButton) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func acceptAciton(_ sender: Any) {
        acceptAction?(buttonAccept)
    }
    
    @IBAction func removeAction(_ sender: Any) {
        removeAction?(buttonRemove)
    }
}
