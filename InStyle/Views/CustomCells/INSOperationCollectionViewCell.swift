//
//  INSOperationCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 01/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSOperationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var buttonIcon: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
