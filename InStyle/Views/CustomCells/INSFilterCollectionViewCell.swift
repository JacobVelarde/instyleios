//
//  INSFilterCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 30/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSFilterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var buttonOption: UIButton!
    static let filterCollectionNotificationName = Notification.Name(rawValue: "filterCollectionNotificationName")
    var buttonAction:((_ sender: UIButton) -> Void)?
        
    override func prepareForReuse() {
        buttonOption.isSelected = false
        buttonOption.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonOption.titleLabel?.textColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonOption.layer.borderWidth = 1.0
        buttonOption.layer.borderColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
        buttonOption.layer.cornerRadius = buttonOption.frame.size.height/2
        buttonOption.titleLabel?.adjustsFontSizeToFitWidth = true
        buttonOption.titleLabel?.numberOfLines = 1
        buttonOption.titleLabel?.minimumScaleFactor = 0.5
    }
    
    @IBAction func buttonPressedAction(_ sender: UIButton) {
        if buttonAction != nil {
            buttonAction?(sender)
            return
        }
        sender.backgroundColor = sender.isSelected ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
        sender.titleLabel?.textColor = sender.isSelected ? #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        sender.isSelected = !sender.isSelected
        NotificationCenter.default.post(name: INSFilterCollectionViewCell.filterCollectionNotificationName, object: sender)
    }
}
