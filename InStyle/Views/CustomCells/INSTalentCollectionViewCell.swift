//
//  INSTalentCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 14/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSTalentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    var talent: INSTalent? {
        set {
            imageViewPhoto.load(urlStr: newValue?.url_profile ?? "")
            labelName.text = (newValue?.name ?? "") + " " + (newValue?.surnames ?? "")
        }
        get {
            nil
        }
    }
}
