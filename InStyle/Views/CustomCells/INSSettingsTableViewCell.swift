//
//  INSSettingsTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 13/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var texfieldValue: UITextField!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonTrash: UIButton!
    @IBOutlet weak var buttonChecked: UIButton!
    var edittAction: ((_ sender: UIButton) -> Void)?
    var trashAction: ((_ sender: UIButton) -> Void)?
    var applyChangesAction: ((_ sender: UIButton) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonChecked.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        switch sender {
        case buttonEdit:
            edittAction?(buttonEdit)
            break
        case buttonTrash:
            trashAction?(buttonTrash)
        case buttonChecked:
            applyChangesAction?(buttonChecked)
        default:
            break
        }
    }
    
}
