//
//  INSCustomerHeaderTableView.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 11/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSCustomerHeaderTableView: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonEye: UIButton!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonSelected: UIButton!
    @IBOutlet weak var buttonUpArrow: UIButton!
    
    public var seeAction : ((_ sender: UIButton) -> Void)?
    public var editAction : ((_ sender: UIButton) -> Void)?
    public var upAction : ((_ sender: UIButton) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonSelected.isHidden = true
        buttonUpArrow.isHidden = true
        buttonSelected.imageView?.contentMode = .scaleAspectFit
        buttonEye.imageView?.contentMode = .scaleAspectFit
        buttonEdit.imageView?.contentMode = .scaleAspectFit
        buttonUpArrow.imageView?.contentMode = .scaleAspectFit

        let attributedText = NSMutableAttributedString(string: labelTitle.text!)
        attributedText.addWordSpacing(1.5)
        labelTitle.attributedText = attributedText
    }

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "INSCustomerHeaderTableView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    @IBAction func pressAction(_ sender: UIButton) {
        if sender == buttonEye {
            seeAction?(buttonEye)
        } else if sender == buttonEdit {
            editAction?(buttonEdit)
        } else if sender == buttonSelected {
            
        } else if sender == buttonUpArrow {
            upAction?(buttonUpArrow)
        }
    }
    
    var hideEditingButton:Bool? {
        didSet {
            buttonSelected.isHidden = hideEditingButton ?? false
            if hideEditingButton ?? false {
                buttonEdit.isHidden = false
                buttonEye.isHidden = false
                buttonUpArrow.isHidden = true
            } else {
                buttonEdit.isHidden = true
                buttonEye.isHidden = true
                buttonUpArrow.isHidden = true
            }
        }
    }

}
