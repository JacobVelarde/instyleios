//
//  INSMenuItemCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 17/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSMenuItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var buttonIcon: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
