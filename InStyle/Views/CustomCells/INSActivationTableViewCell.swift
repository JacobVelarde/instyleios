//
//  INSActivationTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 12/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

protocol SelectionDelegate {
    func rowSelected(activation: INSActivation)
}

class INSActivationTableViewCell: UITableViewCell {

    @IBOutlet weak var labelEventName: UILabel!
    @IBOutlet weak var buttonPoints: UIButton!
    @IBOutlet weak var selectorButton: UIButton!
    public var buttonAction : ((_ sender: UIButton) -> Void)?
    public var selectorAction : ((_ sender: UIButton) -> Void)?
    
    override func prepareForReuse() {
        selectorButton.isSelected = false
    }

    private var _activation: INSActivation?
    var activation: INSActivation? {
        set {
            _activation = newValue
            labelEventName.text = _activation?.eventName
        }
        get {
            return _activation
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setup() {
        buttonPoints.imageView?.contentMode = .scaleAspectFit
        buttonPoints.addTarget(self, action: #selector(pointsTouchUpInside), for: .touchUpInside)
        selectorButton.addTarget(self, action: #selector(selectorTouchUpInside), for: .touchUpInside)
        if #available(iOS 13.0, *) {
            selectorButton.setImage(UIImage(systemName: "circle"), for: .normal)
            selectorButton.setImage(UIImage(systemName: "circle.fill"), for: .selected)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func pointsTouchUpInside(sender: UIButton){
        self.buttonAction?(buttonPoints)
    }
    
    @objc func selectorTouchUpInside(sender: UIButton) {
        selectorButton.isSelected = !selectorButton.isSelected
        self.selectorAction?(selectorButton)
    }
}
