//
//  INSItemWithStarCollectionViewCell.swift.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 22/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSItemWithStarCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var imageViewStart: UIImageView!
    public var starTapAction : ((_ sender: UICollectionViewCell) -> Void)?
    public var photoTapAction : ((_ sender: UICollectionViewCell) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageViewPhoto.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(photoTapGesture)))
        imageViewStart.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(starTapGesture(_:))))
    }
    
    @objc func photoTapGesture(_ sender: UITapGestureRecognizer) {
        self.photoTapAction?(self)
    }
    
    @objc func starTapGesture(_ sender: UITapGestureRecognizer) {
        self.starTapAction?(self)
    }

    var changeImage: Bool? {
        didSet {
            if changeImage == true{
                imageViewStart.image = UIImage(named: "startWarnIcon")
            } else {
                imageViewStart.image = UIImage(named: "startGrayIcon")
            }
        }
    }
}
