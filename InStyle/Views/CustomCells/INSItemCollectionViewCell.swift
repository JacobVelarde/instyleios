//
//  INSItemCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López on 1/20/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var buttonIcon: UIButton!
    @IBOutlet weak var buttonEdit: UIButton!
    
    static let itemCollectionSelectedNotificationName = Notification.Name(rawValue: "itemCollectionSelectedNotificationName")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundBorder()
    }
    
    @IBAction func pressedButtonAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: INSItemCollectionViewCell.itemCollectionSelectedNotificationName, object: sender)
    }
    
    func roundBorder() {
        buttonIcon.imageView?.layer.cornerRadius = buttonIcon.frame.size.width / 2
    }
}
