//
//  CostsTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 30/05/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class CostsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
