//
//  INSPhotoItemCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 09/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSPhotoItemCollectionViewCell: UICollectionViewCell {
    
    static let photoItemSelectedNotificationName = Notification.Name(rawValue: "photoItemnSelectedNotificationName")

    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var photoDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func photoButtonACtion(_ sender: UIButton) {
        NotificationCenter.default.post(name: INSPhotoItemCollectionViewCell.photoItemSelectedNotificationName, object: sender)
    }
    
    func roundBorder() {
        cameraButton.imageView?.layer.cornerRadius = cameraButton.frame.size.width / 2
    }
}
