//
//  INSCollectionTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López on 1/19/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSCollectionTableViewCell: UITableViewCell {
    
    enum CollectionItemType: Int {
        case photoType
        case photoTypeNDescription
        case filterType
    }
    
    @IBOutlet weak var labeltitle: UILabel!
    @IBOutlet weak var collectionViewOptions: UICollectionView!
    @IBOutlet weak var collectionFlowLayout: UICollectionViewFlowLayout!
    fileprivate var filterCollectionItems: [String]?
    fileprivate var currentInfo: Any?
    fileprivate var collectionType: CollectionItemType?
    fileprivate var collectionItems: Int?
    fileprivate var descriptionTextColor: UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionViewOptions.register(UINib(nibName: "INSItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photoCell")
        collectionViewOptions.register(UINib(nibName: "INSPhotoItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "photoItemCell")
        collectionViewOptions.register(UINib(nibName: "INSFilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "filterCell")
        //empty cell
        collectionViewOptions.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionViewOptions.delegate = self
        collectionViewOptions.dataSource = self
    }
    
    fileprivate func updateUI(itemsInRow: Int, cellSeparation: Int, estimatedItemHeight: Int? = nil) {
        let separatorSize = (itemsInRow - 1) * cellSeparation
        let sizeItem = (Int(collectionViewOptions.frame.size.width) - separatorSize) / itemsInRow
        let height = estimatedItemHeight ?? sizeItem
        collectionFlowLayout.itemSize = CGSize(width: sizeItem, height: height)
        collectionViewOptions.reloadData()
        collectionViewOptions.layoutIfNeeded()
    }
    
    func setFilterCollection(for items: [String], itemsXRow: Int = 1, separation: Int = 10, andCurrentInfo info: Any? = nil) {
        self.collectionType = .filterType
        self.filterCollectionItems = items
        self.currentInfo = info
        self.collectionItems = items.count
        updateUI(itemsInRow: itemsXRow, cellSeparation: separation, estimatedItemHeight: 45)
    }
    
    func setPhotoCollection(type: CollectionItemType, totalItems: Int, itemsInRow: Int = 3, separation: Int = 15, currentInfo info: Any? = nil, estimatedItemHeight: Int? = nil, textColor: UIColor? = nil) {
        self.collectionType = type
        self.currentInfo = info
        self.collectionItems = totalItems
        self.descriptionTextColor = textColor
        updateUI(itemsInRow: itemsInRow, cellSeparation: separation, estimatedItemHeight: estimatedItemHeight)
    }
}

extension INSCollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionType {
        case .filterType:
            return filterCollectionItems?.count ?? 0
        default:
            return collectionItems ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionType {
        case .photoTypeNDescription:
            guard let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoItemCell", for: indexPath) as? INSPhotoItemCollectionViewCell else {
                return UICollectionViewCell()
            }
            itemCell.cameraButton.tag = indexPath.row
            itemCell.editButton.tag = indexPath.row
            
            if let photosTaken = currentInfo as? [Int: (image: INSWebMultimedia?, description: String)],
                let data = photosTaken[indexPath.row] {
                let requestDispatcher = INSRequestDispatcher()
                if let webImage = data.image {
                    if let imageData = webImage.media {
                        itemCell.cameraButton.setImage(UIImage(data: imageData), for: .normal)
                    } else if let imageUrl = webImage.webUrl, !imageUrl.isEmpty {
                        requestDispatcher.downloadData(url: imageUrl) { (success, data) in
                            if success, let imageData = data as? Data, let image = UIImage(data: imageData) {
                                webImage.media = image.jpegData(compressionQuality: INSConstants.noCompression)
                                itemCell.cameraButton.setImage(image, for: .normal)
                            }
                        }
                    }
                    itemCell.editButton.isHidden = false
                    itemCell.photoDescription.text = data.description
                    itemCell.roundBorder()
                } else {
                    itemCell.cameraButton.setImage(#imageLiteral(resourceName: "cameraIcon"), for: .normal)
                    itemCell.editButton.isHidden = true
                    itemCell.photoDescription.text = data.description
                }
            }
            
            if let color = descriptionTextColor {
                itemCell.photoDescription.textColor = color
            }
            return itemCell
        case .photoType:
            guard let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as? INSItemCollectionViewCell else {
                return UICollectionViewCell()
            }
            itemCell.buttonIcon.tag = indexPath.row
            if let photosTaken = currentInfo as? [Int: INSWebMultimedia], let webImage = photosTaken[indexPath.row] {
                let requestDispatcher = INSRequestDispatcher()
                if let imageData = webImage.media {
                    itemCell.buttonIcon.setImage(UIImage(data: imageData), for: .normal)
                } else if let imageUrl = webImage.webUrl {
                    requestDispatcher.downloadData(url: imageUrl) { (success, data) in
                        if let imageData = data as? Data, let image = UIImage(data: imageData) {
                            webImage.media = image.jpegData(compressionQuality: INSConstants.noCompression)
                            itemCell.buttonIcon.setImage(image, for: .normal)
                        }
                    }
                }
                itemCell.buttonEdit.isHidden = false
                itemCell.roundBorder()
            } else {
                itemCell.buttonIcon.setImage(#imageLiteral(resourceName: "cameraIcon"), for: .normal)
                itemCell.buttonEdit.isHidden = true
            }
            return itemCell
        case .filterType:
            guard let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "filterCell", for: indexPath) as? INSFilterCollectionViewCell else {
                return UICollectionViewCell()
            }
            
            itemCell.buttonOption.tag = indexPath.row
            itemCell.buttonOption.setTitle(filterCollectionItems?[indexPath.row], for: .normal)
            
            if let optionsSelected = currentInfo as? [String],
                let currentOption = filterCollectionItems?[indexPath.row] {
                itemCell.buttonOption.isSelected = optionsSelected.contains(currentOption)
                if itemCell.buttonOption.isSelected {
                    itemCell.buttonOption.backgroundColor = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
                }
            }
            return itemCell
        default:
            break;
        }
        
        return collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
    }
}
