//
//  INSCustomerTalentsTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 11/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSCustomerTalentsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var buttonCheck: UIButton!
    public var selectAction : ((_ sender: UIButton) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonCheck.layer.borderColor = INSConstants.ThemeColor.turquoise.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var talent:INSTalent? {
        didSet {
            labelUsername.text = talent?.completeName
        }
    }
    
    var hideCheckButton:Bool? {
        didSet {
            buttonCheck.isHidden = hideCheckButton ?? false
        }
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        selectAction?(sender)
    }
}
