//
//  INSPickerItemView.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 03/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

//struct INSPickerItem {
//    var title: NSAttributedString?
//    var image: String?
//    var segue: String?
//
//    public init(_ title: NSAttributedString, image: String, segueId: String) {
//        self.title = title
//        self.image = image
//        self.segue = segueId
//    }
//}


class INSPickerItemView: UIView {

    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    private var _item:AreaItems?
    
    var item:AreaItems? {
        set {
            _item = newValue
            labelTitle.attributedText = _item?.title
            imageViewIcon.image = UIImage(named: _item?.image ?? "")
        }
        get {
            return _item
        }
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "INSPickerItemView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
