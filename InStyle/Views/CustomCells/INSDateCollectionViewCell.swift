//
//  INSDateCollectionViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 15/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import JTAppleCalendar

class INSDateCollectionViewCell: JTACDayCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var daySelectionBackgroundView: UIView!
    @IBOutlet weak var activationIndicatorView: UIView!
}
