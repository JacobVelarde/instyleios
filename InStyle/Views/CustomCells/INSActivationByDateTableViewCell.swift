//
//  INSActivationByDateTableViewCell.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 20/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSActivationByDateTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: PaddingLabel!
    @IBOutlet weak var button: UIButton!
    public var labelAction : (( _ sender: UITapGestureRecognizer) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()
    }
    
    internal func setActivation(activation: INSActivation, selectedDate: String?){
        button.setTitle(String(selectedDate?.split(separator: "-")[2] ?? ""), for: .normal)
        labelTitle.text = activation.eventName
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func tapAction(_ sender: UITapGestureRecognizer){
        self.labelAction?(sender)
    }
}
