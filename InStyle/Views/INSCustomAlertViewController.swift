//
//  INSCustomAlertViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 12/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

public enum AlertType: Int {
    case imageBelowMessage = 0
    case imageUpMeessage = 1
}

class INSCustomAlertViewController: UIViewController {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelMessageBelowImage: UILabel!
    var titleAlert : String?
    var messageAlert : String?
    var imageNameAlert : String?
    var _alertType: AlertType?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    internal func initWith(title:String? = nil, message:String? = nil, imageName:String? = nil, alertType: AlertType? = .imageBelowMessage) {
        _alertType = alertType
        titleAlert = title
        messageAlert = message
        imageNameAlert = imageName
    }
    
    fileprivate func setup() {
        switch _alertType {
        case .imageUpMeessage:
            labelTitle.isHidden = true
            labelMessage.isHidden = true
            labelMessageBelowImage.text = messageAlert
            imageViewIcon.image = UIImage(named: imageNameAlert ?? "")
            break
        default:
            labelTitle.text = titleAlert
            labelMessage.text = messageAlert
            imageViewIcon.image = UIImage(named: imageNameAlert ?? "")
            labelMessageBelowImage.isHidden = true
            if titleAlert == nil || titleAlert == "" {
                labelTitle.isHidden = true
            }
            if messageAlert == nil || messageAlert == "" {
                labelMessage.isHidden = true
            }
            if imageNameAlert == nil || imageNameAlert == "" {
                imageViewIcon.isHidden = true
            }
            break
        }
        
    }
    
    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
