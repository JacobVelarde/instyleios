//
//  PaddingLabel.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 21/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit


class PaddingLabel: UILabel {
    var contentInsets = UIEdgeInsets(top: 3, left: 20, bottom: 3, right: 0)

    override func drawText(in rect: CGRect) {
        let insetRect = rect.inset(by: contentInsets)
        super.drawText(in: insetRect)
    }

    override var intrinsicContentSize: CGSize {
        return addInsets(to: super.intrinsicContentSize)
    }

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return addInsets(to: super.sizeThatFits(size))
    }

    private func addInsets(to size: CGSize) -> CGSize {
        let width = size.width + contentInsets.left + contentInsets.right
        let height = size.height + contentInsets.top + contentInsets.bottom
        return CGSize(width: width, height: height)
    }
}
