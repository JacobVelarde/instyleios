//
//  INSTalent.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation
import UIKit

class INSTalent: INSUser {
    struct Selfies {
        var face: INSWebMultimedia?
        var sideFace: INSWebMultimedia?
        var wholeBody: INSWebMultimedia?
    }

    struct Polaroids {
        var face: INSWebMultimedia?
        var front: INSWebMultimedia?
        var back: INSWebMultimedia?
        var leftSideFace: INSWebMultimedia?
        var rightSideFace: INSWebMultimedia?
        var threeQuarters: INSWebMultimedia?
    }

    struct LocalPeopleDocuments {
        var socialSecurity: INSPersonalDocument?
        var INE: INSPersonalDocument?
    }

    struct ForeignPeopleDocuments {
        var socialSecurity: INSPersonalDocument?
        var jobPermission: INSPersonalDocument?
    }

    struct SelfVideos {
        var demoReel: INSWebMultimedia?
        var casting: INSWebMultimedia?
    }

    enum EyeColor: String, CaseIterable {
        case black = "Negro"
        case brown = "Café"
        case green = "Verde"
        case blue = "Azul"
        case gray = "Gris"
    }

    enum HairColor: String, CaseIterable {
        case black = "Negro"
        case brown = "Castaño"
        case blond = "Rubio"
        case redhead = "Pelirrojo"
    }

    enum Specialities: Int, CaseIterable {
        case model, GIO, actor, image, promoter, animator, showDriver, coordinator, dancer, influencer
    }

    enum GenderType: String {
        case female = "MUJER"
        case male = "HOMBRE"
    }

    enum Category: Int {
        case promoter = 1
        case hostessAA = 2
        case model = 3
        case gioAA = 4
    }

    var gender: GenderType?
    var age: Int?
    var height: Float?
    var weight: Float?
    var eyeColor: EyeColor?
    var hairColor: HairColor?
    var footwear: Float?
    var selfies: Selfies?
    var book: [INSWebMultimedia?]?
    var polaroids: Polaroids?
    var videos: SelfVideos?
    var localDocuments: LocalPeopleDocuments?
    var foreignDocuments: ForeignPeopleDocuments?
    var specialityAreas: [Specialities]?
    var category: String?
    var categoryId: Category? {
        didSet {
            switch self.categoryId {
            case .promoter:
                self.category = "TALENT_CATEGORY_PROMOTER".localized
            case .hostessAA:
                self.category = "TALENT_CATEGORY_HOSTESS_AA".localized
            case .model:
                self.category = "TALENT_CATEGORY_MODEL".localized
            case .gioAA:
                self.category = "TALENT_CATEGORY_GIO_AA".localized
            case .none:
                break
            }
        }
    }

    var confirmation: String?
    var costo_hr: String?
    var genderValid: String?
    var nationality: String?
    var foreign: Bool = false
    var instagramProfile: String?
    var isBack: Bool = false

    public override init() {
        super.init()
    }

    public init(filterResponse: [String: Any]) {
        super.init()
        self.idUser = filterResponse["id"] as? String
        self.name = filterResponse["nombres"] as? String
        self.surnames = "\(filterResponse["apaterno"] as? String ?? "") \(filterResponse["amaterno"] as? String ?? "")"
        self.category = filterResponse["categoria"] as? String
        if let _categoryId = filterResponse["categoria_id"] as? String,
            let categoryInt = Int(_categoryId),
            let _category = Category(rawValue: categoryInt) {
            self.categoryId = _category
        }
    }

    public init(jsonResponse: [String: Any]? = [:]) {
        super.init()
        if let id = jsonResponse?["user_id"] as? String {
            self.idUser = id
        } else {
            self.idUser = jsonResponse?["id"] as? String
        }
        self.name = jsonResponse?["nombres"] as? String
        self.surnames = jsonResponse?["apellidos"] as? String
        if let url = jsonResponse?["url_perfil"] as? String {
            self.url_profile = url
        } else {
            self.url_profile = String(format:"%@%@", "https://e.agenciainstyle.com/", jsonResponse?["url_profile"] as? String ?? "")
        }
        self.url_profile = self.url_profile?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.genderValid = jsonResponse?["genero"] as? String
        self.nationality = jsonResponse?["nacionalidad"] as? String

        self.confirmation = jsonResponse?["confirmation"] as? String
        self.costo_hr = jsonResponse?["costo_hr"] as? String
        self.isInTeam = isInDreamTeam(jsonResponse?["en_dreamteam"] as? String)
    }

    func setTalentProfile(from user: INSUser, and jsonResponse: [String:Any]) -> INSTalent {
        self.token = user.token
        self.idUser = user.idUser
        self.profile = .talent
        self.firstTimeForTalent = user.firstTimeForTalent

        if let gender = jsonResponse["genero"] as? String {
            self.gender = gender == "Masculino" ? .male : .female
        }
        if let nationality = jsonResponse["nacionalidad"] as? String {
            self.nationality = nationality
            if let _nationality = self.nationality, _nationality.contains("Mexican") {
                self.foreign = false
            } else {
                self.foreign = true
            }
        }
        if let age = jsonResponse["edad"] as? String {
            self.age = Int(age)
        }
        if let height = jsonResponse["altura_mts"] as? String {
            self.height = Float(height)
        }
        if let weight = jsonResponse["peso_kg"] as? String {
            self.weight = Float(weight)
        }
        if let footwear = jsonResponse["calzado"] as? String {
            self.footwear = (Float(footwear) ?? 0) + 20
        }
        if let eyesColor = jsonResponse["ojos"] as? String {
            for _eyeColor in EyeColor.allCases {
                if _eyeColor.rawValue.lowercased() == eyesColor {
                    self.eyeColor = _eyeColor
                }
            }
        }
        if let specialities = jsonResponse["especialidades"] as? [[String: String]] {
            var _specialities: [Specialities] = []
            for speciality in specialities {
                if let model = speciality["modelo"], model == "1" {
                    _specialities.append(.model)
                }
                else if let gio = speciality["edecan"], gio == "1" {
                    _specialities.append(.GIO)
                }
                else if let actor = speciality["actriz"], actor == "1" {
                    _specialities.append(.actor)
                }
                else if let image = speciality["imagen"], image == "1" {
                    _specialities.append(.image)
                }
                else if let promoter = speciality["promotor"], promoter == "1" {
                    _specialities.append(.promoter)
                }
                else if let animator = speciality["animador"], animator == "1" {
                    _specialities.append(.animator)
                }
                else if let showDriver = speciality["conductor"], showDriver == "1" {
                    _specialities.append(.showDriver)
                }
                else if let coordinator = speciality["coordinador"], coordinator == "1" {
                    _specialities.append(.coordinator)
                }
                else if let dancer = speciality["bailarin"], dancer == "1" {
                    _specialities.append(.dancer)
                }
                else if let influencer = speciality["influencer"], influencer == "1" {
                    _specialities.append(.influencer)
                }
            }
            self.specialityAreas = _specialities
        }
        if let multimedia = jsonResponse["multimedia"] as? [[String: String]] {
            self.book = []
            for media in multimedia {
                if let category = media["categoria"], category.contains("doc_") {
                    let doc = INSPersonalDocument(media)
                    if self.foreign {
                        if self.foreignDocuments == nil {
                            self.foreignDocuments = ForeignPeopleDocuments()
                        }
                        if category == "doc_seguro" {
                            self.foreignDocuments?.socialSecurity = doc
                        }
                        else if category == "doc_permisoine" {
                            self.foreignDocuments?.jobPermission = doc
                        }
                    }
                    else {
                        if self.localDocuments == nil {
                            self.localDocuments = LocalPeopleDocuments()
                        }
                        if category == "doc_seguro" {
                            self.localDocuments?.socialSecurity = doc
                        }
                        else if category == "doc_permisoine" {
                            self.localDocuments?.INE = doc
                        }
                    }
                }
                else {
                    let webImage = INSWebMultimedia(media)
                    if let category = webImage.category {
                        if category.contains("selfie_") {
                            if self.selfies == nil {
                                self.selfies = Selfies()
                            }
                            if category == "selfie_perfil" {
                                self.selfies?.sideFace = webImage
                            }
                            else if category == "selfie_rostro" {
                                self.selfies?.face = webImage
                            }
                            else if category == "selfie_ccompleto" {
                                self.selfies?.wholeBody = webImage
                            }
                        }
                        else if category.contains("book_") {
                            self.book?.append(webImage)
                        }
                        else if category.contains("pola_") {
                            if self.polaroids == nil {
                                self.polaroids = Polaroids()
                            }
                            if category == "pola_rostro" {
                                self.polaroids?.face = webImage
                            }
                            else if category == "pola_frente" {
                                self.polaroids?.front = webImage
                            }
                            else if category == "pola_espalda" {
                                self.polaroids?.back = webImage
                            }
                            else if category == "pola_izquierda" {
                                self.polaroids?.leftSideFace = webImage
                            }
                            else if category == "pola_derecha" {
                                self.polaroids?.rightSideFace = webImage
                            }
                            else if category == "pola_34" {
                                self.polaroids?.threeQuarters = webImage
                            }
                        }
                        else if category.contains("video_") {
                            if self.videos == nil {
                                self.videos = SelfVideos()
                            }
                            if category == "video_demo" {
                                self.videos?.demoReel = webImage
                            }
                            else if category == "video_casting" {
                                self.videos?.casting = webImage
                            }
                        }
                    }
                }
            }
        }
        return self
    }

//    override func load(urlStr: String) {
//        guard let url = URL(string: urlStr) else { return }
//        DispatchQueue.global().async { [weak self] in
//            if let data = try? Data(contentsOf: url) {
//                if let image = UIImage(data: data) {
//                    DispatchQueue.main.async {
//                        self?.image = image
//                    }
//                }
//            }
//        }
//    }
}
