//
//  INSBrand.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSBrand {
    var id: String?
    var name: String?
    
    init(filerResponse reponse: [String: Any]) {
        self.id = reponse["id"] as? String
        self.name = reponse["nombre"] as? String
    }
}
