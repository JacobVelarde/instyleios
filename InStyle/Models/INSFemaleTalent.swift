//
//  INSFemaleTalent.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSFemaleTalent: INSTalent {

    var bustSize: Float?
    var waistSize: Float?
    var hipSize: Float?
    var defaultSpecialitiesRawValues: [String] {
        get {
            return INSTalent.Specialities.allCases.map {
                return specialityValueFor($0)
            }
        }
    }
    
    override init(jsonResponse: [String: Any]? = [:]) {
        super.init()
        self.gender = .female
    }
    
    public func setFemaleTalent(from talent: INSTalent, jsonResponse: [String: Any] = [:]) {
        self.token = talent.token
        self.idUser = talent.idUser
        self.name = talent.name
        self.surnames = talent.surnames
        self.age = talent.age
        self.height = talent.height
        self.weight = talent.weight
        self.footwear = talent.footwear
        self.eyeColor = talent.eyeColor
        self.profile = .talent
        self.firstTimeForTalent = talent.firstTimeForTalent
        self.gender = .female
        self.book = talent.book
        self.selfies = talent.selfies
        self.polaroids = talent.polaroids
        self.videos = talent.videos
        self.localDocuments = talent.localDocuments
        self.foreignDocuments = talent.foreignDocuments
        self.specialityAreas = talent.specialityAreas
        if let _bustSize = jsonResponse["m_pecho"] as? String {
            self.bustSize = Float(_bustSize)
        }
        if let _waistSize = jsonResponse["m_cintura"] as? String {
            self.waistSize = Float(_waistSize)
        }
        if let _hipSize = jsonResponse["m_cadera"] as? String {
            self.hipSize = Float(_hipSize)
        }
    }
}

extension INSFemaleTalent {
    
    func specialityValueFor(_ speciality: Specialities) -> String {
        switch speciality {
        case .model:
            return "UNISEX_MODEL".localized
        case .GIO:
            return "FEMALE_HOSTESS".localized
        case .actor:
            return "FEMALE_ACTRESS".localized
        case .image:
            return "UNISEX_IMAGE".localized
        case .promoter:
            return "FEMALE_PROMOTER".localized
        case .animator:
            return "FEMALE_ANIMATOR".localized
        case .showDriver:
            return "FEMALE_SHOWDRIVER".localized
        case .coordinator:
            return "FEMALE_COORDINATOR".localized
        case .dancer:
            return "FEMALE_DANCER".localized
        case .influencer:
            return "UNISEX_INFLUENCER".localized
        }
    }
    
    func specialityFor(_ value: String) -> Specialities? {
        switch value {
        case "UNISEX_MODEL".localized:
            return .model
        case "FEMALE_HOSTESS".localized:
            return .GIO
        case "FEMALE_ACTRESS".localized:
            return .actor
        case "UNISEX_IMAGE".localized:
            return .image
        case "FEMALE_PROMOTER".localized:
            return .promoter
        case "FEMALE_ANIMATOR".localized:
            return .animator
        case "FEMALE_SHOWDRIVER".localized:
            return .showDriver
        case "FEMALE_COORDINATOR".localized:
            return .coordinator
        case "FEMALE_DANCER".localized:
            return .dancer
        case "UNISEX_INFLUENCER".localized:
            return .influencer
        default:
            return nil
        }
    }
}
