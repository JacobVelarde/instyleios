//
//  INSProducer.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSProducer : INSUser{
    
    init(json: [String: Any]? = [:]) {
        super.init()
        self.idUser = json?["id"] as? String
        self.name = json?["nombres"] as? String
        self.surnames = json?["apellidos"] as? String
    }
}
