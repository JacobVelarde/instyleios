//
//  INSPersonalDocument.swift
//  InStyle
//
//  Created by Alejandro López on 05/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSPersonalDocument {
    var document: Data?
    var filePathUrl: URL?
    var webUrl: String?
    
    init(document: Data?, filePathUrl: String?, webUrl: String?) {
        self.document = document
        if let filePath = filePathUrl {
            self.filePathUrl = URL(string: filePath)
        }
        self.webUrl = webUrl
    }
    
    convenience init(_ jsonResponse: [String: String]) {
        guard let url = jsonResponse["url"] else {
            self.init(document: nil, filePathUrl: nil, webUrl: nil)
            return
        }
        self.init(document: nil, filePathUrl: nil, webUrl: url)
    }
}
