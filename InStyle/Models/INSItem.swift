//
//  INSItem.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 19/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSItem: NSObject {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let title = "title"
        static let icon = "icon"
    }
    
    // MARK: Properties
    public var id: String?
    public var title: String?
    public var icon: String?
    
    public required init(json: [String:Any]) {
        id = json["id"] as? String
        title = json["title"] as? String
        icon = json["icon"] as? String
    }
}
