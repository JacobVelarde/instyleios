//
//  INSActivations.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 12/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSActivations: NSObject {
    var completed: String = "0"
    var inProgress: String = "0"
    var next: String = "0"
    var total: String = "0"
    var activationsList: [INSActivation] = []
    
    public init(jsonResponse: [String: Any]?) {
        self.completed = String(format: "%d", jsonResponse?["completadas"] as? Int ?? 0)
        self.inProgress = String(format: "%d", jsonResponse?["encurso"] as? Int ?? 0)
        self.next = String(format: "%d", jsonResponse?["proximas"] as? Int ?? 0)
        self.total = String(format: "%d", jsonResponse?["total"] as? Int ?? 0)
        
        for item in jsonResponse?["listado"] as? [Any] ?? []{
            let activation = INSActivation.init(activation: item as? [String : Any])
            activationsList.append(activation)
        }
    }
}

class INSActivation {
    var id: String?
    var eventName: String?
    var type: String?
    var customer: String?
    var producer: String?
    var brand: String?
    var coordinators: [String] = []
    var totalCoordinators: Int?
    var location: String?
    var eventDate: String?
    var startTime: String?
    var endTime: String?
    var totalTalents: Int?
    var duration: String?
    var talentsTotalCost: Double?
    var coordinatorCost: String?
    var coordinatorsTotalCost: Double?
    var totalEvent: String?
    var totalCostEvent: Double?
    var eventStatus: String?
    var talents: [INSTalent] = []
    
    public init(activation: [String: Any]?) {
        self.id = activation?["id"] as? String
        self.eventName = activation?["nombre_evento"] as? String
    }
    
    public func updateActivation(activation: [String: Any]?) {
        self.type = activation?["tipo_evento"] as? String
        self.customer = activation?["cliente"] as? String
        self.producer = activation?["productor"] as? String
        self.brand = activation?["marca"] as? String
        self.totalCoordinators = activation?["total_coordinadores"] as? Int
        self.location = activation?["ubicacion"] as? String
        self.eventDate = activation?["fecha_evento"] as? String
        self.startTime = activation?["hora_inicio"] as? String
        self.endTime = activation?["hora_final"] as? String
        self.totalTalents = activation?["total_talentos"] as? Int
        self.duration = activation?["duracion"] as? String
        self.talentsTotalCost = activation?["total_costo_talentos"] as? Double
        self.coordinatorCost = activation?["costo_coordinador"] as? String
        self.coordinatorsTotalCost = activation?["total_costo_coordinadores"] as? Double
        self.totalEvent = activation?["total_evento"] as? String
        self.totalCostEvent = activation?["total_costo_evento"] as? Double
        self.eventStatus = activation?["status_evento"] as? String
        
        talents.removeAll()
        for item in activation?["talentos"] as? [Any] ?? []{
            let talent = INSTalent.init(jsonResponse: item as? [String : Any])
            talents.append(talent)
        }
        
        coordinators.removeAll()
        for coordinator in activation?["coordinadores"] as? [Any] ?? [] {
            let dictionary = coordinator as? [String : Any]
            let fullName = (dictionary?["nombres"] as? String ?? "") + " " + (dictionary?["apellidos"] as? String ?? "")
            coordinators.append(fullName)
        }

    }
}
