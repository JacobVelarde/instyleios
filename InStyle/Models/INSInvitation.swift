//
//  INSInvitation.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 14/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSInvitation: NSObject {
    var title: String?
    var descriptionStr: String?
    var date: String?
    
    init(title: String, descriptionStr: String, date:String) {
        self.title = title
        self.descriptionStr = descriptionStr
        self.date = date
    }
}
