//
//  INSMaleTalent.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSMaleTalent: INSTalent {
    
    var shirt: Float?
    var coat: Float?
    var pants: Float?
    var defaultSpecialitiesRawValues: [String] {
        get {
            return INSTalent.Specialities.allCases.map {
                return specialityRawValueFor($0)
            }
        }
    }
    
    override init(jsonResponse: [String: Any]? = [:]) {
        super.init()
        self.gender = .male
    }
    
    public func setMaleTalent(from talent: INSTalent, jsonResponse: [String: Any] = [:]) {
        self.token = talent.token
        self.idUser = talent.idUser
        self.name = talent.name
        self.surnames = talent.surnames
        self.age = talent.age
        self.height = talent.height
        self.weight = talent.weight
        self.footwear = talent.footwear
        self.eyeColor = talent.eyeColor
        self.profile = .talent
        self.firstTimeForTalent = talent.firstTimeForTalent
        self.gender = .male
        self.book = talent.book
        self.selfies = talent.selfies
        self.polaroids = talent.polaroids
        self.videos = talent.videos
        self.localDocuments = talent.localDocuments
        self.foreignDocuments = talent.foreignDocuments
        self.specialityAreas = talent.specialityAreas
        if let _coatSize = jsonResponse["h_saco"] as? String {
            self.coat = Float(_coatSize)
        }
        if let _shirtSize = jsonResponse["h_camisa"] as? String {
            self.shirt = Float(_shirtSize)
        }
        if let _pantsSize = jsonResponse["h_pantalon"] as? String {
            self.pants = Float(_pantsSize)
        }
    }
}

extension INSMaleTalent {
    
    func specialityRawValueFor(_ speciality: Specialities) -> String {
        switch speciality {
        case .model:
            return "UNISEX_MODEL".localized
        case .GIO:
            return "MALE_GIO".localized
        case .actor:
            return "MALE_ACTOR".localized
        case .image:
            return "UNISEX_IMAGE".localized
        case .promoter:
            return "MALE_PROMOTER".localized
        case .animator:
            return "MALE_ANIMATOR".localized
        case .showDriver:
            return "MALE_SHOWDRIVER".localized
        case .coordinator:
            return "MALE_COORDINATOR".localized
        case .dancer:
            return "MALE_DANCER".localized
        case .influencer:
            return "UNISEX_INFLUENCER".localized
        }
    }
    
    func specialityFor(_ value: String) -> Specialities? {
        switch value {
        case "UNISEX_MODEL".localized:
            return .model
        case "MALE_GIO".localized:
            return .GIO
        case "MALE_ACTOR".localized:
            return .actor
        case "UNISEX_IMAGE".localized:
            return .image
        case "MALE_PROMOTER".localized:
            return .promoter
        case "MALE_ANIMATOR".localized:
            return .animator
        case "MALE_SHOWDRIVER".localized:
            return .showDriver
        case "MALE_COORDINATOR".localized:
            return .coordinator
        case "MALE_DANCER".localized:
            return .dancer
        case "UNISEX_INFLUENCER".localized:
            return .influencer
        default:
            return nil
        }
    }
}
