//
//  INSUser.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 06/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSUser {
    enum ProfileType {
        case admin
        case client
        case talent
        case coordinator
        case booker
        case producer
    }
    
    var name: String?
    var surnames: String?
    var email: String?
    var birthDate: String?
    var idUser: String?
    var token: String?
    var profile: ProfileType?
    var firstTimeForTalent: Bool?
    var telephone: String?
    var profileStr: String?

    init() {}
    
    init(withResponse response: [String: Any]) {
        self.token = response["token"] as? String
        self.idUser = response["id_user"] as? String
        if let profile = response["perfil"] as? String {
            switch Int(profile) {
            case 1, 5, 6:
                self.profile = .admin
            case 2, 8:
                self.profile = .client
            case 3:
                self.profile = .talent
            case 4:
                self.profile = .coordinator
            case 7:
                self.profile = .booker
            case 9:
                self.profile = .producer
            default:
                break
            }
            
            if let firstTimeTalent = response["primera_vez"] as? String {
                self.firstTimeForTalent = firstTimeTalent == "0" ? false : true
            }
        }
    }
    var record: String?
    var url_profile: String?
    var image: UIImage?
    var isInTeam:Bool = false

    var completeName: String? {
        get {
            return "\(self.name ?? "") \(self.surnames ?? "")"
        }
    }
    
    func isInDreamTeam(_ isInDream: String?) -> Bool{
        if isInDream == "1" {
            return true
        }
        return false
    }
}
