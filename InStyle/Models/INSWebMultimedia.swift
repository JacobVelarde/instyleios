//
//  INSWebImage.swift
//  InStyle
//
//  Created by Alejandro López on 29/03/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation
import UIKit

enum MediaType {
    case picture
    case video
}

class INSWebMultimedia {
    var filePathUrl: URL?
    var webUrl: String?
    var category: String?
    var media: Data?
    var mediaType: MediaType?
    var takenFromCamera: Bool = false {
        didSet {
            self.mediaType = .picture
            self.compression = self.takenFromCamera ? INSConstants.noCompression : INSConstants.normalCompression
        }
    }
    var compression: CGFloat = INSConstants.normalCompression
    var thumbnail: Data?
    
    public init(_ talentProfileInfo: [String: Any]) {
        if let _url = talentProfileInfo["url"] as? String, let _category = talentProfileInfo["categoria"] {
            self.webUrl = _url
            self.category = _category as? String
        }
    }
    
    public init(filePathUrl: String, thumbnail: Data, mediaType: MediaType) {
        self.filePathUrl = URL(string: filePathUrl)
        self.thumbnail = thumbnail
        self.mediaType = mediaType
    }
    
    public init(webUrl: String, thumbnail: Data, mediaType: MediaType) {
        self.webUrl = webUrl
        self.thumbnail = thumbnail
        self.mediaType = mediaType
    }
    
    public init(_ _image: Data?, takenFromCamera: Bool = false) {
        self.media = _image
        self.takenFromCamera = takenFromCamera
    }
    
    public func imageDataCompressed() -> Data? {
        guard let _image = self.media else {
            return nil
        }
        return UIImage(data: _image)?.jpegData(compressionQuality: self.compression)
    }
}
