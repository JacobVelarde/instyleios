//
//  INSReport.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 09/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSReport {
    var title: String?
    var descriptionStr: String?
    var startDate: String?
    var endDate: String?
    
    init(title: String, descriptionStr: String, startDate:String, endDate:String) {
        self.title = title
        self.descriptionStr = descriptionStr
        self.startDate = startDate
        self.endDate = endDate
    }
}
