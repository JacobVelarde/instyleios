//
//  INSCoordinator.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation
import UIKit

class INSCoordinator: INSUser {
    var gender: String?

    public init(filterResponse response: [String: Any]) {
        super.init()
        self.idUser = response["id"] as? String
        self.name = response["nombres"] as? String
        self.surnames = "\(response["apaterno"] as? String ?? "") \(response["amaterno"] as? String ?? "")"
    }
    
    public init(jsonResponse: [String: Any]? = [:]) {
        super.init()
        if let id = jsonResponse?["user_id"] as? String {
            self.idUser = id
        } else {
            self.idUser = jsonResponse?["id"] as? String
        }
        self.name = jsonResponse?["nombres"] as? String
        self.surnames = jsonResponse?["apellidos"] as? String
        self.email = jsonResponse?["correo"] as? String
        self.telephone = jsonResponse?["telefono"] as? String
        self.gender = jsonResponse?["genero"] as? String

        if let url = jsonResponse?["url_perfil"] as? String {
            self.url_profile = url
        } else {
            self.url_profile = String(format:"%@%@", "https://e.agenciainstyle.com/", jsonResponse?["url_profile"] as? String ?? "")
        }
//        self.url_profile = String(format:"%@%@", "https://e.agenciainstyle.com/", jsonResponse?["url_profile"] as? String ?? "")
        self.url_profile = self.url_profile?.trimmingCharacters(in: .whitespacesAndNewlines)
        self.isInTeam = isInDreamTeam(jsonResponse?["en_dreamteam"] as? String)
    }
}
