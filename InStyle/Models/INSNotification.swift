//
//  INSNotification.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 29/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

struct INSNotification {
    var title: String
    var descriptionStr: String
    
    init(title: String, descriptionStr: String) {
        self.title = title
        self.descriptionStr = descriptionStr
    }

}
