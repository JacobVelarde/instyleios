//
//  INSBooker.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSBooker: INSUser {
    
    public init(filterResponse response: [String: Any]) {
        super.init()
        self.idUser = response["id"] as? String
        self.name = response["nombres"] as? String
        self.surnames = "\(response["apaterno"] as? String ?? "") \(response["amaterno"] as? String ?? "")"
    }
}
