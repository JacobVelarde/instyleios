//
//  INSDynamic.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSDynamic {
    
    enum DynamicType: Int {
        case activation = 1
        case salesWork = 2
        case tasting = 3
        case image = 4
        case sampling = 5
    }
    
    var id: DynamicType?
    var description: String?
    
    public init(filterResponse response: [String: Any]) {
        if let id = response["id"] as? String, let idIntValue = Int(id) {
            self.id = DynamicType(rawValue: idIntValue)
            switch self.id {
            case .activation:
                self.description = "DYNAMIC_ACTIVATION".localized
            case .tasting:
                self.description = "DYNAMIC_TASTING".localized
            case .image:
                self.description = "DYNAMIC_IMAGE".localized
            case .salesWork:
                self.description = "DYNAMIC_SALES_WORK".localized
            case .sampling:
                self.description = "DYNAMIC_SAMPLING".localized
            case .none:
                break
            }
        }
    }
}
