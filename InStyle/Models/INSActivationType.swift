//
//  INSActivationType.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 26/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSActivationType {
    var id: String?
    var description: String?
    var status: String?
    
    init(json: [String: Any]? = [:]) {
        self.id = json?["id"] as? String
        self.description = json?["descripcion"] as? String
        self.status = json?["status"] as? String
    }

}
