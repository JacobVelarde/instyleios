//
//  INSOperator.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 27/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSOperator: INSUser {
    
    public init(filterResponse response: [String: Any]) {
        super.init()
        self.name = response["operador"] as? String
    }
}
