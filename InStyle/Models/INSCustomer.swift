//
//  INSCustomer.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 06/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation

class INSCustomer: INSUser {
    var company: String?
    var companyAddress: String?
    var jobPosition: String?
    var linkedInUrl: String?
    var websiteUrl: String?
    var instagramUrl: String?
    var coordinatorCost: String?
    
    public override init(withResponse response: [String: Any]) {
        super.init()
        self.idUser = response["id"] as? String
        company = response["empresa"] as? String
    }
    
    public override init() {
        super.init()
    }

    init(json: [String: Any]? = [:]) {
        super.init()
        self.idUser = json?["id"] as? String
        self.company = json?["empresa"] as? String
        self.coordinatorCost = json?["costo_coordinador"] as? String
    }
}
