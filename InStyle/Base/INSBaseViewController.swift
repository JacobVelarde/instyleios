//
//  INSBaseViewController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 31/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

protocol INSErrorDelegate : class {
    func callServerError40X(error: INSError?);
    func callServerError500(error: INSError?);
    func callServerError20X(error: INSError?);
}

class INSBaseViewController: UIViewController, INSErrorDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(_:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func tapAction(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func callServerError500(error: INSError?) {
        printErrorInfo(error)
    }
    
    func callServerError40X(error: INSError?) {
        printErrorInfo(error)
    }

    func callServerError20X(error: INSError?) {
        printErrorInfo(error)
    }
    
    fileprivate func printErrorInfo(_ error: INSError?) {
        guard let errorInfo = error, let errorCode = errorInfo.errorCode, let errorMessage = errorInfo.errorMessage else {
            print("There's no error info to show")
            return
        }
        print("ErrorCode \(errorCode) Error mesage \(errorMessage)")

    }
}
