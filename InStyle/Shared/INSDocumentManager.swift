//
//  INSDocumentManager.swift
//  InStyle
//
//  Created by Alejandro López on 05/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation
import UIKit

@objc protocol DocumentManagerDelegate {
    func didSelectedDocument(filePath: URL)
    func didCancelDocumentSelection()
}

class INSDocumentManager: NSObject, UIDocumentPickerDelegate {
    weak var delegate: DocumentManagerDelegate?
    
    open func openDocumentPickerForTypes(_ types: [String]) {
        let documentPicker = UIDocumentPickerViewController(documentTypes: types, in: .open)
        if #available(iOS 11.0, *) {
            documentPicker.allowsMultipleSelection = false
        }
        documentPicker.delegate = self
        (delegate as? UIViewController)?.present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        delegate?.didCancelDocumentSelection()
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        delegate?.didSelectedDocument(filePath: url)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        if let url = urls.first {
            delegate?.didSelectedDocument(filePath: url)
        }
    }
}
