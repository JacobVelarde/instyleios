//
//  INSConstants.swift
//  InStyle
//
//  Created by Alejandro López on 1/11/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSConstants {
    static let ONE_INCHE_TO_CM: Float = 2.54
    static let centimetersInMeter: Float = 100
    static let ONE_KG_TO_LB: Float = 2.20462
    static let noCompression: CGFloat = 1.0
    static let normalCompression: CGFloat = 0.95
    static let videoDemoReelDuration: TimeInterval = 120
    static let videoPresentationDuration: TimeInterval = 60

    enum CellIdentifier: String {
        case slider = "sliderCell"
    }

    struct ThemeColor {
        static let darkTurquoise = #colorLiteral(red: 0.1014571115, green: 0.5890335441, blue: 0.6363800168, alpha: 1)
        static let turquoise = #colorLiteral(red: 0.1254901961, green: 0.5960784314, blue: 0.6509803922, alpha: 1)
        static let lightTurquoise = #colorLiteral(red: 0.1333333333, green: 0.6588235294, blue: 0.7176470588, alpha: 1)

        static let darkGray = #colorLiteral(red: 0.3725490196, green: 0.3764705882, blue: 0.3764705882, alpha: 1)
        static let gray = #colorLiteral(red: 0.585082829, green: 0.5887808204, blue: 0.588850379, alpha: 1)
        static let lightGray = #colorLiteral(red: 0.7876523137, green: 0.7910330892, blue: 0.7910935283, alpha: 1)
        static let textfieldBackground = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 0.9794520548)
        static let textfieldErrorBackground = #colorLiteral(red: 1, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        static let textfieldErrorBorder = UIColor.red
    }

    struct ThemeFontNames {
        static let AvenirMedium = "AvenirNext-Medium"
        static let AvenirDemiBoldItalic = "AvenirNext-DemiBoldItalic"
        static let AvenirDemiBold = "AvenirNext-DemiBold"
        static let AvenirHeavyItalic = "AvenirNext-HeavyItalic"
        static let AvenirRegular = "AvenirNext-Regular"
        static let AvenirItalic = "AvenirNext-Italic"
        static let AvenirMediumitalic = "AvenirNext-MediumItalic"
        static let AvenirUltraLightItalic = "AvenirNext-UltraLightItalic"
        static let AvenirBoldItalic = "AvenirNext-BoldItalic"
        static let AvenirHeavy = "AvenirNext-Heavy"
        static let AvenirBold = "AvenirNext-Bold"
        static let AvenirUltraLight = "AvenirNext-UltraLight"
    }

    struct FormFieldLength {
        static let fullNameMaxLength = 100
        static let emailMaxLength = 100
        static let phoneNumberMaxLength = 10
        static let defaultFieldMinLength = 3
        static let dayNMonthMaxLength = 2
        static let yearMaxLength = 4
    }

    struct FormRegex {
        static let emailRegex = "[A-Z0-9a-z_.+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        static let nameOrSurnameRegex = "^[a-zá-úA-ZÁ-Ú]{4,}(?: [a-zá-úA-ZÁ-Ú]+){0,1}$"
        static let fullNameRegex = "^[a-zá-úA-ZÁ-Ú]{4,}(?: [a-zá-úA-ZÁ-Ú]+){1,2}$"
        static let phoneNumberRegex = "[0-9]{10}"
        static let urlRegex = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$"
    }

    //Operation codes
    enum OperationCodes: String {
        case INS_LOGIN_OPCODE  = "01"
        case INS_RETRIEVE_PASSWORD_OPCODE = "02"
        case INS_REGISTER_CUSTOMER_OPCODE = "03"
        case INS_REGISTER_TALENT_OPCODE = "04"
        case INS_UPDATE_TALENT_DATA_OPCODE = "05"
        case INS_GET_REGION_CODE = "06"
        case INS_GET_ACTIVATIONS_PANEL_CODE = "07"
        case INS_GET_ACTIVATION_DETAIL_CODE = "08"
        case INS_GET_MY_PROFILE_CODE = "09"
        case INS_GET_ACTIVATIONS_BY_DATE_CODE = "10"
        case INS_GET_ADMIN_ACTIVATIONS_CODE = "11"
        case INS_GET_ACTIVATIONS_TYPE_CODE = "12"
        case INS_GET_CUSTOMERS_CODE = "13"
        case INS_GET_PRODUCERS_CODE = "14"
        case INS_GET_BRANDS_CODE = "15"
        case INS_GET_TALENTS_CODE = "16"
        case INS_GET_COORDINATORS_CODE = "17"
        case INS_UPLOAD_PHOTOS_CODE = "18"
        case INS_GET_TALENT_PROFILE_CODE = "19"
        case INS_GET_CALENDAR_ACTIVATION_DAYS_CODE = "20"
        case INS_GET_TALENT_PANEL_CODE = "21"
        case INS_FILTER_TALENTS_CODE = "22"
        case INS_GET_TALENT_DETAIL_CODE = "23"
        case INS_COMPLETE_TALENT_INFO_CODE = "24"
        case INS_ADD_TALENT_CODE = "25"
        case INS_UPDATE_PERFIL_CODE = "26"
        case INS_DREAM_TEAM_GET = "27"
        case INS_ADMIN_GET_CUSTOMERS_CODE = "28"
        case INS_LOGOUT_CODE = "29"
        case INS_ADD_DREAM_TEAM_CODE = "30"
        case INS_DELETE_DREAM_TEAM_CODE = "31"
        case INS_FILTER_COORDINATORS_CODE = "32"
    }

    struct ContentTypes {
        static let imageContent = "image/jpeg"
        static let videoContent = "video/mov"
        static let pdfContent = "application/pdf"
    }
}

public struct Keys {
    static let privateKey = "Private-key"
    static let userId = "User-ID"
    static let tokenAuth = "Token-Auth"
    static let idEvent = "id_evento"
    static let screen = "pantalla"
    static let object = "objeto"
    static let yearMonthDay = "anio_mes_dia"
    static let contentType = "Content-type"
    static let jsonContent = "application/json"
    static let multiPartContent = "multipart/form-data"
    static let user_id = "user_id"
    static let names = "nombres"
    static let surnames = "apellidos"
    static let email = "email"
    static let telephone = "telefono"
    static let birthday = "fecha_nacimiento"
    static let url_profile = "url_profile"
    static let profile = "perfil"
    static let reputation = "reputacion"
    static let messsage = "mensaje"
    static let imei = "imei"
    static let username = "username"
    static let password = "password"
    static let cacheControl = "Cache-Control"
    static let noCache = "no-cache"
}
