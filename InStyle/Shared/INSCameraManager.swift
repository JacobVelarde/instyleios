//
//  INSCameraManager.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 04/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices
import AVFoundation

@objc protocol CameraManagementDelegate {
    func didPhotoTaken(image: UIImage)
    @objc optional func didMovieRecorded(urlVideo: URL, thumbnailImage: UIImage)
}

class INSCameraManager: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    weak var delegate: CameraManagementDelegate?
    
    func launchCamera(for mediaType: CFString, duration: TimeInterval = 0) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else { return }
        
        let mediaUI = UIImagePickerController()
        mediaUI.sourceType = .camera
        mediaUI.allowsEditing = true
        mediaUI.delegate = self

        if mediaType == kUTTypeMovie {
            mediaUI.mediaTypes = [mediaType as String]
            mediaUI.videoQuality = .typeHigh
            mediaUI.videoMaximumDuration = duration
        }
        else if mediaType == kUTTypeImage {
            mediaUI.cameraDevice = .front
        }
        
        (delegate as? UIViewController)?.present(mediaUI, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        if let mediaType = info[.mediaType] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[.mediaURL] as? URL {
            
            // generate thumbnail
            let asset = AVAsset(url: url)
            let assetImgGenerate = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMakeWithSeconds(1.0, preferredTimescale: 600)
            do {
                let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                let thumbnail = UIImage(cgImage: img)
                delegate?.didMovieRecorded?(urlVideo: url, thumbnailImage: thumbnail)
            } catch {
                print(error.localizedDescription)
            }
        } else {
            guard let image = info[.editedImage] as? UIImage else {
                print("No image taken")
                return
            }
            delegate?.didPhotoTaken(image: image)
        }
    }
}
