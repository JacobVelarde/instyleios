//
//  INSImageDownloadingOperation.swift
//  InStyle
//
//  Created by Alejandro López on 04/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation
import UIKit

class INSImageDownloadingOperation: Operation {
    var downloadHandler: ImageDownloadHanlder?
    var imageUrl: String!
    private var indexPath: IndexPath?
    
    override var isAsynchronous: Bool {
        get {
            return true
        }
    }
    
    private var _executing = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    private var _finished = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        return _finished
    }
    
    func executing(_ executing: Bool) {
        _executing = executing
    }
    
    func finish(_ finish: Bool) {
        _finished = finish
    }
    
    required init(url: String, indexPath: IndexPath?) {
        self.imageUrl = url
        self.indexPath = indexPath
    }
    
    override func main() {
        guard isCancelled == false else {
            finish(true)
            return
        }
        self.executing(true)
        // Asynchronous logic (eg: n/w calls) with callback
        self.downloadImageFromUrl()
    }
    
    func downloadImageFromUrl() {
        let newSession = URLSession.shared
        guard let url = URL(string: self.imageUrl) else { return }
        let downloadTask = newSession.downloadTask(with: url) { (location, response, error) in
            if let locationUrl = location, let data = try? Data(contentsOf: locationUrl) {
                let image = UIImage(data: data)
                self.downloadHandler?(image, self.imageUrl, self.indexPath, error)
            }
            self.finish(true)
            self.executing(false)
        }
        downloadTask.resume()
    }
}
