//
//  INSImageDownloadManager.swift
//  InStyle
//
//  Created by Alejandro López on 04/04/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import Foundation
import UIKit

typealias ImageDownloadHanlder = (_ image: UIImage?, _ url: String?, _ indexPath: IndexPath?, _ error: Error?) -> Void

class INSImageDownloadManager {
    private var completionHandler: ImageDownloadHanlder?
    lazy var imageDownloadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "com.agenciainstyle.instyle.imageDownloadQueue"
        queue.qualityOfService = .userInteractive
        return queue
    }()
    let imageCache = NSCache<NSString, UIImage>()
    static let shared = INSImageDownloadManager()
    private init() {}
    
    func downloadImage(_ photo: INSWebMultimedia, indexPath: IndexPath?, handler: @escaping ImageDownloadHanlder) {
        self.completionHandler = handler
        guard let urlPhoto = photo.webUrl else {
            return
        }
        if let cachedImage = imageCache.object(forKey: urlPhoto as NSString) {
            // check fot the cached image for url, if YES then return the cached image
            print("Return cached image for \(urlPhoto)")
            self.completionHandler?(cachedImage, urlPhoto, indexPath, nil)
        }
        else {
            // check if there's a download task that is currently downloading the same image
            if let operations = (imageDownloadQueue.operations as? [INSImageDownloadingOperation])?.filter({
                $0.imageUrl == urlPhoto && $0.isFinished == false && $0.isExecuting == true}), let operation = operations.first {
                print("Increase the priority for \(urlPhoto)")
                operation.queuePriority = .high
            }
            else {
                // create a new task to download the image
                print("Create a new task for \(urlPhoto)")
                let operation = INSImageDownloadingOperation(url: urlPhoto, indexPath: indexPath)
                if indexPath == nil {
                    operation.queuePriority = .veryHigh
                }
                operation.downloadHandler = { (image, url, indexPath, error) in
                    if let newImage = image {
                        self.imageCache.setObject(newImage, forKey: urlPhoto as NSString)
                    }
                    self.completionHandler?(image, url, indexPath, error)
                }
                imageDownloadQueue.addOperation(operation)
            }
        }
    }
}
