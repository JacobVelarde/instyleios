//
//  INSUtility.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 03/02/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import AVFoundation

class INSUtility {
    
    static func convertValue(_ value: Any, fromUnit: String, toUnit: String) -> Any {
        if fromUnit == "cm", toUnit == "in" {
            guard let cm = value as? Float else { return 0.0 }
            return cm/INSConstants.ONE_INCHE_TO_CM
        }
        else if fromUnit == "kg", toUnit == "lb" {
            guard let kg = value as? Int else { return 0.0 }
            return Float(kg) * INSConstants.ONE_KG_TO_LB
        }
        return 0
    }
    
    static func convertHeight(_ value: Float, fromUnit: String, toUnit: String) -> (feet: Int, inch: Int) {
        if fromUnit == "m", toUnit == "ft in" {
            var measure = Measurement(value: Double(value), unit: UnitLength.meters)
            let feet = measure.converted(to: .feet).value
            let reminder = feet.truncatingRemainder(dividingBy: 1)
            measure = Measurement(value: reminder, unit: .feet)
            let inches = measure.converted(to: .inches).value.rounded()
            return (Int(feet), Int(inches))
        }
        return (0, 0)
    }
    
    static func convertFemaleShoeSize(_ value: Float, from: String, to: String) -> Float {
        if from == "MX", to == "US" {
            var USShoeSize: Float = 5.0
            let minCmValue: Float = 22.0
            let maxCmValue: Float = 27.0
            
            for cmValue in stride(from: minCmValue, to: maxCmValue + 0.5, by: 0.5) {
                if value == cmValue {
                    return USShoeSize
                }
                USShoeSize += 0.5
            }
        }
        return 0
    }
    
    static func convertMaleShoeSize(_ value: Float, from: String, to: String) -> Float {
        if from == "MX", to == "US" {
            var USShoeSize: Float = 7.0
            let minCmValue: Float = 25.0
            let maxCmValue: Float = 31.0
            
            for cmValue in stride(from: minCmValue, to: maxCmValue + 0.5, by: 0.5) {
                if value == cmValue {
                    if cmValue == 30.5 {
                        USShoeSize = 12
                    }
                    return USShoeSize
                }
                                
                USShoeSize += 0.5
            }
        }
        return 0
    }
    
    static func getThumbnailImage(from url: URL, completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let asset = AVAsset(url: url)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            do {
                let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60), actualTime: nil)
                completion(UIImage(cgImage: thumbnailImage))
            } catch {
                print("Couldn't generate thumbnail image from \(url.absoluteString), error generated: \(error.localizedDescription)")
                completion(nil)
            }
        }
    }
    
    static func getValueForTopSeparatorConstraintInMainViews() -> CGFloat{
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136: //iPhone 5 or 5S or 5C
                return 70.0
            case 1334, //iPhone 6/6S/7/8
                 1920, 2208: //iPhone 6+/6S+/7+/8+
                return 130.0
            case 2436: //iPhone X/XS/11 Pro
                return 150.0
            case 2688: //iPhone XS Max/11 Pro Max
                return 190.0
            case 1792: //iPhone XR/ 11
                return 170.0
            default:
                print("Unknown")
            }
        }
        return 130.0
    }

    static func getValueForHeightMainTableView() -> CGFloat{
        if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
            case 1136:
                return 300
            case 1334,
                 1920, 2208:
                return 350
            case 2436, 2688, 1792:
                return 400.0
            default:
                print("Unknown")
            }
        }
        return 400.0
    }

}
