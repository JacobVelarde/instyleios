//
//  INSDataController.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 31/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSDataController: NSObject {
    public static let sharedDataController = INSDataController()
    var isLoggedIn:Bool = false
    var user: INSUser = INSUser()
//    var activations: INSActivations?
//    var activationDetail: INSActivation?
    var talentList: [INSTalent] = []
    var coordinatorList: [INSCoordinator] = []

    public func loginResponse(jsonResponse:[String: Any]?) {
        if let response = jsonResponse?[Keys.object] as? [String: Any] {
            user = INSUser(withResponse: response)
        }
    }
    
    public func updateUserProfile(jsonResponse:[String: Any]?) {
        guard jsonResponse?[Keys.object] != nil else {
            return
        }
        if let array = jsonResponse?[Keys.object] as? [Any] {
            if let responseDict = array.first as? [String:Any] {
                user.name = responseDict[Keys.names] as? String
                user.surnames = responseDict[Keys.surnames] as? String
                user.email = responseDict[Keys.email] as? String
                user.url_profile = responseDict[Keys.url_profile] as? String
                user.record = responseDict[Keys.reputation] as? String
                user.birthDate = responseDict[Keys.birthday] as? String
                user.telephone = responseDict[Keys.telephone] as? String
                user.profileStr = responseDict[Keys.profile] as? String
            }
        }
    }
    
    public func initTalentProfile(jsonResponse: [String: Any]?) {
        guard let json = jsonResponse else {
            return
        }
        var talent = INSTalent()
        talent = talent.setTalentProfile(from: self.user, and: json)
        self.user = talent
    }
    
    public func fetchTalents() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_TALENTS_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    self.talentList = self.parseArray(jsonResponse: jsonResponse, opCode:.INS_GET_TALENTS_CODE) as! [INSTalent]
                } else {
                }
            }
        })
    }
    
    public func fetchCoordinators() {
        let requestDispatcher = INSRequestDispatcher()
        requestDispatcher.dispatchRequest(opCode: .INS_GET_COORDINATORS_CODE, requestParameters: nil, completionHandler: { (success, jsonResponse, response, error) in
            var _success = success
            if let code = error?.errorCode, code == INSHTTPCodes.OK201.rawValue {
                _success = true
            }
            DispatchQueue.main.async {
                if _success {
                    self.coordinatorList = self.parseArray(jsonResponse: jsonResponse, opCode:.INS_GET_COORDINATORS_CODE) as! [INSCoordinator]
                } else {
                }
            }
        })
    }
}

extension INSDataController {
    //MARK: Methods to parse json responses
    func parseArray(jsonResponse:[String: Any]?, opCode: INSConstants.OperationCodes) -> [Any]?{
        guard jsonResponse?[Keys.object] != nil else {
            return nil
        }
        var array: [Any] = []
        if let responseDict = jsonResponse?[Keys.object] as? [Any] {
            switch opCode {
            case .INS_GET_TALENTS_CODE:
                for item in responseDict{
                    let talent = INSTalent.init(jsonResponse: item as? [String : Any])
                    array.append(talent)
                }
                break
            case .INS_GET_COORDINATORS_CODE:
                for item in responseDict{
                    let talent = INSCoordinator.init(jsonResponse: item as? [String : Any])
                    array.append(talent)
                }
                break
            default:
                break
            }
        }
        return array
    }
}
