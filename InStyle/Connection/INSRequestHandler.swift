//
//  INSRequestHandler.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 30/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit
import Alamofire

enum HTTPRequestMethod : String {
    case get = "GET"
    case post = "POST"
}

public enum INSHTTPCodes : Int {
    case OK = 200
    case OK201 = 201
    case OK202 = 202
    case NOCONTENT = 204
    case PARTIALCONTENT = 206
    case GO_TO_DASHBOARD = 208
    case KNOWNERROR = 400
    case UNAUTHORIZED = 401
    case NOTFOUND = 404
    case SERVERERROR = 500
}

private let urlBase = "https://rest.agenciainstyle.com/"
private var privateKey = "%sJDlnfXTk#4izhBhQMz*%wrA@tPAoV!^zJQUisiIG5riBAgxc"
typealias ImageDataHandler = (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void

class INSRequestHandler: NSObject {

    var errorObject: INSError?
    let dataController = INSDataController.sharedDataController

    func getDataFrom(opCode: INSConstants.OperationCodes, httpRequestMethod: HTTPRequestMethod, requestParameters: [String : Any]?, queryParameters: [String : Any]? = nil, completionHandler : @escaping ResponseHandler) {
        makeWebServiceCall(opCode: opCode, httpRequestMethod: httpRequestMethod, urlStr: getUrl(opCode: opCode, queryParameters: queryParameters), requestParameters: requestParameters, completionHandler: { (success, jsonResponse, response) in
            if success {
                self.populateController(opCode: opCode, jsonResponse: jsonResponse)
                completionHandler(success, jsonResponse, response, nil)
            }else{
                self.handleError(httpUrlResponse: response, jsonResponse: jsonResponse)
                completionHandler(success, jsonResponse, response, self.errorObject)
            }
        })
    }

    func getDataFromUrl(url: String, completion: @escaping ImageDataHandler) {
        let _url = URL(string: url) ?? URL(string: "")!
        URLSession.shared.dataTask(with: _url) { (data, response, error) in
            completion(data, response, error)
        }.resume()
    }

    func getUrl(opCode: INSConstants.OperationCodes, queryParameters: [String:Any]? = nil) -> String {
        switch opCode {
        case .INS_LOGIN_OPCODE:
            return urlBase + "auth/login"
        case .INS_LOGOUT_CODE:
            return urlBase + "auth/logout"
        case .INS_RETRIEVE_PASSWORD_OPCODE:
            return urlBase + "auth/recuperar"
        case .INS_REGISTER_CUSTOMER_OPCODE:
            return urlBase + "registro/cliente"
        case .INS_REGISTER_TALENT_OPCODE:
            return urlBase + "registro/talento"
        case .INS_UPDATE_TALENT_DATA_OPCODE:
            return urlBase + "talento/actualizarDatos"
        case .INS_GET_REGION_CODE:
            return urlBase + "registro/getCodigos"
        case .INS_GET_ACTIVATIONS_PANEL_CODE:
            return urlBase + "admin/panel" + getAppendPathForUlrQueryParamters(parameters: queryParameters)
        case .INS_GET_ACTIVATION_DETAIL_CODE:
            let id: String = queryParameters?[Keys.idEvent] as! String
            return urlBase + "admin/detalleEvento/" + id
        case .INS_GET_MY_PROFILE_CODE:
            return urlBase + "perfil"
        case .INS_GET_ADMIN_ACTIVATIONS_CODE:
            return urlBase + "admin/activaciones"
        case .INS_GET_ACTIVATIONS_BY_DATE_CODE:
            return urlBase + "admin/calendarioDetalle" + getAppendPathForUlrQueryParamters(parameters: queryParameters)
        case .INS_GET_ACTIVATIONS_TYPE_CODE:
            return urlBase + "activaciones/getTipos"
        case .INS_GET_CUSTOMERS_CODE:
            return urlBase + "activaciones/getClientes"
        case .INS_GET_PRODUCERS_CODE:
            return urlBase + "activaciones/getProductores"
        case .INS_GET_BRANDS_CODE:
            return urlBase + "activaciones/getMarcas"
        case .INS_GET_TALENTS_CODE:
            return urlBase + "admin/getTalentos"
        case .INS_GET_COORDINATORS_CODE:
            return urlBase + "admin/getCoordinadores"
        case .INS_UPLOAD_PHOTOS_CODE:
            return urlBase + "registro/carga"
        case .INS_GET_TALENT_PROFILE_CODE:
            return urlBase + "talento/perfil"
        case .INS_GET_CALENDAR_ACTIVATION_DAYS_CODE:
            return urlBase + "admin/calendario" + getAppendPathForUlrQueryParamters(parameters: queryParameters)
        case .INS_GET_TALENT_PANEL_CODE:
            return urlBase + "admin/paneltalentos" + getAppendPathForUlrQueryParamters(parameters: queryParameters)
        case .INS_FILTER_TALENTS_CODE:
            return urlBase + "admin/filtrartalentos"
        case .INS_GET_TALENT_DETAIL_CODE:
            return urlBase + "admin/detalletalento" + getAppendPathForUlrQueryParamters(parameters: queryParameters)
        case .INS_COMPLETE_TALENT_INFO_CODE:
            return urlBase + "registro/completaperfildocs"
        case .INS_ADD_TALENT_CODE:
            return urlBase + "admin/agregatalento"
        case .INS_UPDATE_PERFIL_CODE:
            return urlBase + "perfil/update"
        case .INS_DREAM_TEAM_GET:
            return urlBase + "dreamteam/get"
        case .INS_ADMIN_GET_CUSTOMERS_CODE:
            return urlBase + "admin/getClientes"
        case .INS_ADD_DREAM_TEAM_CODE:
            return urlBase + "dreamteam/add"
        case .INS_DELETE_DREAM_TEAM_CODE:
            return urlBase + "dreamteam/delete"
        case .INS_FILTER_COORDINATORS_CODE:
            return urlBase + "admin/filtrarcoordinador"
        }
    }

    func getAppendPathForUlrQueryParamters(parameters: [String:Any]?) -> String {
        var appendPath = ""
        if let params = parameters {
            for (key,value) in params {
                appendPath.contains("?") ? (appendPath.append("&")) : (appendPath.append("?"))
                appendPath.append(key)
                appendPath.append("=")
                appendPath.append((value as? String) ?? "")
            }
        }
        return appendPath
    }

    func getHeders(opCode: INSConstants.OperationCodes) -> [String: String] {
        var headers:[String: String] = [Keys.contentType: Keys.jsonContent, Keys.privateKey: privateKey, Keys.cacheControl: Keys.noCache]
        switch opCode {
        case .INS_LOGIN_OPCODE,
             .INS_RETRIEVE_PASSWORD_OPCODE,
             .INS_REGISTER_CUSTOMER_OPCODE,
             .INS_REGISTER_TALENT_OPCODE,
             .INS_GET_REGION_CODE:
            break
        default:
            headers[Keys.userId] = dataController.user.idUser ?? "-1"
            headers[Keys.tokenAuth] = dataController.user.token ?? ""
        }

        if opCode == .INS_UPLOAD_PHOTOS_CODE || opCode == .INS_COMPLETE_TALENT_INFO_CODE {
            headers[Keys.contentType] = Keys.multiPartContent
        }

        return headers
    }

    func makeWebServiceCall(opCode:INSConstants.OperationCodes, httpRequestMethod: HTTPRequestMethod, urlStr: String, requestParameters: [String : Any]?, completionHandler : @escaping ResponseHandlerManager) {
        
        let headers = getHeders(opCode: opCode)
        if headers[Keys.contentType] == Keys.multiPartContent {
            AF.upload(multipartFormData: { (multipartFormData) in
                guard let parameters = requestParameters else { return }
                
                for (key, value) in parameters where ((value as? String) != nil) {
                    multipartFormData.append((value as! String).data(using: .utf8)!, withName: key)
                }
                for (key, value) in parameters where ((value as? Data) != nil) {
                    multipartFormData.append(value as! Data, withName: key, fileName: "\(key).jpg", mimeType: INSConstants.ContentTypes.imageContent)
                }
                for (key, value) in parameters where ((value as? INSWebMultimedia)) != nil {
                    let multimedia = value as! INSWebMultimedia
                    if let mediaType = multimedia.mediaType, mediaType == .picture {
                        if let imageData = multimedia.imageDataCompressed() {
                            multipartFormData.append(imageData, withName: key, fileName: "\(key).jpg", mimeType: INSConstants.ContentTypes.imageContent)
                        }
                    }
                    else if let mediaType = multimedia.mediaType, mediaType == .video {
                        do {
                            let videoData = try Data(contentsOf: multimedia.filePathUrl!)
                            multipartFormData.append(videoData, withName: key, fileName: "\(key).mov", mimeType: INSConstants.ContentTypes.videoContent)
                        } catch {
                            print("Couldn't get video: " + error.localizedDescription)
                        }
                    }
                }
                for (key, value) in parameters where ((value as? INSPersonalDocument)) != nil {
                    let doc = value as! INSPersonalDocument
                    do {
                        let documentData = try Data(contentsOf: doc.filePathUrl!)
                        multipartFormData.append(documentData, withName: "\(key).pdf", mimeType: INSConstants.ContentTypes.pdfContent)
                    } catch {
                        print("Coudln't get document \(key) from \(doc.filePathUrl?.absoluteString ?? ""): \(error.localizedDescription)")
                    }
                }
                print(multipartFormData)
            }, to: urlStr, headers: HTTPHeaders(headers)).responseJSON { (response) in
                debugPrint(response)
                switch response.result {
                case .success:
                    guard let value = response.value as? [String: Any] else {
                        completionHandler(false, ["error": response.error?.errorDescription! as Any], nil)
                        break
                    }
                    completionHandler(true, value, nil)
                case .failure:
                    completionHandler(false, ["error": response.error?.errorDescription! as Any], nil)
                    break
                }
            }
        }
        else {
            logRequest(url: urlStr, requestMethod: httpRequestMethod, requestParameters: requestParameters, requestHeaders: headers)
            let session = URLSession(configuration: .default)
            guard let url: URL = URL(string: urlStr)  else {
                return
            }
            var request = URLRequest(url: url)
            request.httpMethod = httpRequestMethod.rawValue
            
            if let parameters = requestParameters {
                request.httpBody = getHTTPBody(parameters: parameters)
            }
            for (key, value) in headers {
                request.addValue(value, forHTTPHeaderField: key)
            }
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error in
                if let strData = data {
                    if strData.count == 0 {
                        if let urlResponse = response as? HTTPURLResponse {
                            let json = ["codigo":urlResponse.statusCode, "mensaje":urlResponse.description] as [String : Any]
                            completionHandler(false, json, response)
                        }
                    } else {
                        do {
                            let str = String(decoding: strData, as: UTF8.self)
                            print(str)
                            let jsonData = try JSONSerialization.jsonObject(with: strData, options: [])
                            let json = jsonData as! [String : Any]
                            let codigo = json["codigo"] as! Int
                            if codigo == INSHTTPCodes.OK.rawValue || codigo == INSHTTPCodes.OK201.rawValue || codigo == INSHTTPCodes.PARTIALCONTENT.rawValue || codigo == INSHTTPCodes.GO_TO_DASHBOARD.rawValue {
                                completionHandler(true, json, response)
                            } else {
                                completionHandler(false, json, response)
                            }
                        } catch _ as NSError {
                            completionHandler(false, [:], response)
                        }
                    }
                }
                
            })
            task.resume()
        }
    }

    func getHTTPBody(parameters: [String:Any]) -> Data {
        do {
            return try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
        }
        return Data()
    }

    public func handleError(httpUrlResponse : URLResponse?, jsonResponse: [String: Any]?) {
        if let jsonResponseUnwrapped = jsonResponse {
            self.errorObject = self.getErrorObject(response: jsonResponseUnwrapped)
            DispatchQueue.main.async {
                let delegate:INSErrorDelegate? = self.getErrorDelegate()
                switch self.errorObject?.errorCode {
                case INSHTTPCodes.UNAUTHORIZED.rawValue, INSHTTPCodes.NOTFOUND.rawValue, INSHTTPCodes.KNOWNERROR.rawValue:
                    delegate?.callServerError40X(error: self.errorObject)
                    break
                case INSHTTPCodes.NOCONTENT.rawValue, INSHTTPCodes.PARTIALCONTENT.rawValue:
                    delegate?.callServerError20X(error: self.errorObject)
                    break
                default:
                    break
                }
            }
        }
    }

    func getErrorObject(response: [String: Any]?) -> INSError? {
        if (response != nil) {
            let errorObject = INSError()
            errorObject.errorCode = response?["codigo"] as? Int
            errorObject.errorMessage = response?["mensaje"] as? String
            return errorObject
        }
        return nil
    }

    func getErrorDelegate() -> INSErrorDelegate? {
        if (UIApplication.shared.windows.first?.visibleViewcontroller != nil) {
            if (UIApplication.shared.windows.first?.visibleViewcontroller is INSErrorDelegate) {
                return UIApplication.shared.windows.first?.visibleViewcontroller as? INSErrorDelegate
            }
        }
        return nil
    }

    func populateController(opCode: INSConstants.OperationCodes, jsonResponse: [String:Any]) {
        switch opCode {
            case .INS_LOGIN_OPCODE:
                dataController.loginResponse(jsonResponse: jsonResponse)
                print(jsonResponse)
               break
            case .INS_REGISTER_CUSTOMER_OPCODE:
                print(jsonResponse)
               break
            case .INS_GET_MY_PROFILE_CODE:
                dataController.updateUserProfile(jsonResponse: jsonResponse)
             print(jsonResponse)
            break
            default:
               break
        }
    }

    public func logRequest(url:String, requestMethod:HTTPRequestMethod, requestParameters:Any?, requestHeaders: [String: String]) {
        print("Url: \n" + url)
        print("Headers: \n" + (jsonPrint(dictionary: requestHeaders) ?? "Empty headers"))
        print("Request Parameters: \n" + (jsonPrint(dictionary: requestParameters ?? [:]) ?? "Empty parameters"))
    }

    func jsonPrint(dictionary: Any) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
}
