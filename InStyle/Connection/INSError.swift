//
//  INSError.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 31/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

class INSError: NSObject {
    var errorCode: Int?
    var errorMessage: String?
    var httpStatus: Int?
}
