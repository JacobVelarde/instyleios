//
//  INSRequestDispatcher.swift
//  InStyle
//
//  Created by Alejandro López Gómez on 30/01/20.
//  Copyright © 2020 Agencia 360 in Style. All rights reserved.
//

import UIKit

typealias ResponseHandler = (_ success : Bool, _ jsonResponse: [String:Any], _ response : URLResponse?, _ error : INSError? ) -> ()
typealias ResponseHandlerManager = (_ success : Bool, _ jsonResponse: [String:Any], _ response : URLResponse?) -> ()
typealias ResponseDataDownloader = (_ success : Bool, _ data: Any?) -> Void

class INSRequestDispatcher: NSObject {
    private var requestHandler = INSRequestHandler()

    public func dispatchRequest(opCode: INSConstants.OperationCodes, requestParameters: [String:Any]?, queryParameters: [String:Any]? = nil, completionHandler: @escaping ResponseHandler) {
        switch opCode {
        case .INS_LOGIN_OPCODE,
             .INS_LOGOUT_CODE,
             .INS_RETRIEVE_PASSWORD_OPCODE,
             .INS_REGISTER_CUSTOMER_OPCODE,
             .INS_REGISTER_TALENT_OPCODE,
             .INS_UPDATE_TALENT_DATA_OPCODE,
             .INS_GET_ADMIN_ACTIVATIONS_CODE,
             .INS_UPLOAD_PHOTOS_CODE,
             .INS_GET_TALENT_PROFILE_CODE,
             .INS_FILTER_TALENTS_CODE,
             .INS_COMPLETE_TALENT_INFO_CODE,
             .INS_ADD_TALENT_CODE,
             .INS_UPDATE_PERFIL_CODE,
             .INS_ADD_DREAM_TEAM_CODE,
             .INS_DELETE_DREAM_TEAM_CODE,
             .INS_FILTER_COORDINATORS_CODE:
            requestHandler.getDataFrom(opCode: opCode, httpRequestMethod: .post, requestParameters: requestParameters, completionHandler: { (success, jsonResponse, response, error) in
                completionHandler(success, jsonResponse, response, error)
            })
        case .INS_GET_REGION_CODE,
             .INS_GET_MY_PROFILE_CODE,
             .INS_GET_ACTIVATIONS_PANEL_CODE,
             .INS_GET_ACTIVATION_DETAIL_CODE,
             .INS_GET_ACTIVATIONS_BY_DATE_CODE,
             .INS_GET_ACTIVATIONS_TYPE_CODE,
             .INS_GET_CUSTOMERS_CODE,
             .INS_GET_TALENTS_CODE,
             .INS_GET_COORDINATORS_CODE,
             .INS_GET_CALENDAR_ACTIVATION_DAYS_CODE,
             .INS_GET_TALENT_PANEL_CODE,
             .INS_GET_TALENT_DETAIL_CODE,
             .INS_ADMIN_GET_CUSTOMERS_CODE,
             .INS_DREAM_TEAM_GET:
            requestHandler.getDataFrom(opCode: opCode, httpRequestMethod: .get, requestParameters: nil, queryParameters: queryParameters, completionHandler: { (success, jsonResponse, response, error) in
                completionHandler(success, jsonResponse, response, error)
            })
        case .INS_GET_PRODUCERS_CODE,
             .INS_GET_BRANDS_CODE:
            requestHandler.getDataFrom(opCode: opCode, httpRequestMethod: .post, requestParameters: requestParameters, queryParameters: queryParameters, completionHandler: { (success, jsonResponse, response, error) in
            completionHandler(success, jsonResponse, response, error)
        })
        }
    }

    public func downloadData(url: String?, completion: @escaping ResponseDataDownloader) {
        guard url != nil && url != "" else {
            completion(false, nil)
            return
        }
        requestHandler.getDataFromUrl(url: url!) { (data, response, error)  in
            guard let data = data, error == nil else {
                completion(false, nil)
                return
            }
            DispatchQueue.main.async() { () -> Void in
                completion(true, data)
            }
        }
    }
}
